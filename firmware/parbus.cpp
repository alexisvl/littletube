// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "parbus.hpp"

// Supporting modules
#include "hw.hpp"
#include "avl_parsenums.hpp"
#include <util/delay.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#ifdef BUILD_CONTROL
#include <ucos_ii.h>
#endif

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#ifdef BUILD_CONTROL

static char _vp_build[9] = {0};

error_t parbus_transmitter::write(parbus_action ac, uint8_t ad)
{
	// To avoid unnecessary handshaking delay, this interface relies
	// on us de-strobing quickly enough after ack that it does not
	// see a spurious write.
	wdt_reset();
	cli();

	PINGRP_ADn = ad;

	// nSTB is active low, so we are setting the strobe too
	PINGRP_ACn_nSTB_nACK = (uint8_t) ac << PIN_AC0_bp;

	error_t err = error_t::OK;

	for (uint16_t c = 1000;; c--)
	{
		if (!*PIN_nACK)
			break;

		if (!*PIN_VSYNC)
		{
			err = error_t::INTR;
			break;
		}

		if (!*PIN_nACK)
			break;

		if (c == 0)
		{
			err = error_t::TIMEDOUT;
			break;
		}
	}

	// clear strobe
	PIN_nSTB = 1;

	sei();

	return err;
}

result_t<uint8_t> parbus_transmitter::read()
{
	// To avoid unnecessary handshaking delay, this interface relies
	// on us de-strobing quickly enough after ack that it does not
	// see a spurious write.
	wdt_reset();
	cli();

	PINGRP_ADn.vport().DIR = 0;

	// nSTB is active low, so we are setting the strobe too
	PINGRP_ACn_nSTB_nACK = (uint8_t) parbus_action::READ << PIN_AC0_bp;

	error_t err = avl::error_t::OK;
	for (uint16_t c = 0;; c++)
	{
		if (!*PIN_nACK)
			break;

		if (!*PIN_VSYNC)
		{
			err = avl::error_t::INTR;
			break;
		}

		if (!*PIN_nACK)
			break;

		if (c == 65535)
		{
			err = avl::error_t::TIMEDOUT;
			break;
		}
	}

	uint8_t data = *PINGRP_ADn;

	// clear strobe
	PIN_nSTB = 1;
	__builtin_avr_nop();
	__builtin_avr_nop();
	PINGRP_ADn.vport().DIR = 0xFF;
	sei();

	if (err == error_t::OK)
		return data;
	else
		return err;
}

error_t parbus_transmitter::write_addr(uint16_t addr)
{
	error_t e;
	e = write(parbus_action::WRITE_ADDR_LOW, addr & 0xFF);
	if (e != avl::error_t::OK)
		return e;
	e = write(parbus_action::WRITE_ADDR_HIGH, addr >> 8);
	return e;
}

void parbus_vp_to_constant_vsync(void)
{
	// Wait for vsync!
	parbus.write_addr(parbus_cfg_reg::CFG_MAIN);
	uint8_t cfg = parbus.read().ok_or(0); // TODO: handle error
	cfg &= ~0x01;
	parbus.write_addr(parbus_cfg_reg::CFG_MAIN);
	parbus.write(parbus_action::WRITE_8, cfg);
}

void parbus_vp_to_run(void)
{
	// Wait for vsync!
	parbus.write_addr(parbus_cfg_reg::CFG_MAIN);
	uint8_t cfg = parbus.read().ok_or(0);
	cfg |= 0x01;
	parbus.write_addr(parbus_cfg_reg::CFG_MAIN);
	parbus.write(parbus_action::WRITE_8, cfg);
}

error_t parbus_vp_sleep(void)
{
	PIN_VID_nRESET << avl::port_dir::output_0;
	OSTimeDly(1);
	PINGRP_ACn_nSTB_nACK -= (1 << PIN_nSTB_bp);
	OSTimeDly(1);
	PIN_VID_nRESET << avl::port_dir::output_1;
	OSTimeDly(4);

	for (uint16_t c = 0; *PINGRP_ACn_nSTB_nACK & (1 << PIN_nACK_bp); c++)
	{
		if (c == UINT16_MAX)
			return error_t::TIMEDOUT;
	}
	PINGRP_ACn_nSTB_nACK += (1 << PIN_nSTB_bp);
	for (uint16_t c = 0; !(*PINGRP_ACn_nSTB_nACK & (1 << PIN_nACK_bp)); c++)
	{
		if (c == UINT16_MAX)
			return error_t::TIMEDOUT;
	}

	return error_t::OK;
}

error_t parbus_vp_wake(void)
{
	PIN_VID_nRESET << avl::port_dir::output_0;
	OSTimeDly(1);
	PINGRP_ACn_nSTB_nACK += (1 << PIN_nSTB_bp);
	OSTimeDly(1);
	PIN_VID_nRESET << avl::port_dir::output_1;
	OSTimeDly(4);

	for (uint16_t c = 0; *PINGRP_ACn_nSTB_nACK & (1 << PIN_nACK_bp); c++)
	{
		if (c == 10000)
			return error_t::TIMEDOUT;
	}
	PINGRP_ACn_nSTB_nACK -= (1 << PIN_nSTB_bp);
	for (uint16_t c = 0; !(*PINGRP_ACn_nSTB_nACK & (1 << PIN_nACK_bp)); c++)
	{
		if (c == UINT16_MAX)
			return error_t::TIMEDOUT;
	}
	PINGRP_ACn_nSTB_nACK += (1 << PIN_nSTB_bp);
	return error_t::OK;
}

error_t parbus_vp_startup()
{
	error_t e;

	e = parbus_vp_wake();
	if (e != error_t::OK)
		return e;

	e = parbus.write_addr(CFG_MAGIC);
	if (e != error_t::OK)
		return error_t::INTR;
	result_t<uint8_t> magic_r = parbus.read();

	if (magic_r.is_err())
		return magic_r.err();
	else if (magic_r.ok() != CFG_MAGIC_VAL)
	{
		return error_t::IO;
	}

	for (int i = 0; i < 4; i += 1)
	{
		e = parbus.write_addr(CFG_BUILD_HASH_1 + i);
		if (e != error_t::OK)
			return e;
		result_t<uint8_t> bh_r = parbus.read();
		if (bh_r.is_err())
			return bh_r.err();
		_vp_build[(3 - i) * 2] = to_hex_nibble(bh_r.ok() >> 4);
		_vp_build[(3 - i) * 2 + 1] = to_hex_nibble(bh_r.ok());
	}

	return error_t::OK;
}

char const * parbus_vp_get_build()
{
	return _vp_build;
}

#elif defined(BUILD_VIDEO)
#include "hw.h"

video_mode parbus_check_mode(void)
{
	if (*PINGRP_ACn_nSTB_nACK & (1 << PIN_nSTB_bp))
	{
		PINGRP_ACn_nSTB_nACK -= (1 << PIN_nACK_bp);
		while (*PINGRP_ACn_nSTB_nACK & (1 << PIN_nSTB_bp));
		PINGRP_ACn_nSTB_nACK += (1 << PIN_nACK_bp);
		return video_mode::WAKE;
	}
	else
	{
		PINGRP_ACn_nSTB_nACK -= (1 << PIN_nACK_bp);
		while (!(*PINGRP_ACn_nSTB_nACK & (1 << PIN_nSTB_bp)));
		PINGRP_ACn_nSTB_nACK += (1 << PIN_nACK_bp);
		return video_mode::SLEEP;
	}
}

bool parbus_receiver::get(parbus_action * ac, uint8_t * ad)
{
	// wait for stroke
	while (*PIN_nSTB)
	{
		if (GPR_FLAGS & GPR_FLAG_PEND_EXIT_VSYNC)
			return false;
	}

	uint8_t ad_in = *PINGRP_ADn;
	uint8_t ac_in = (*PINGRP_ACn_nSTB_nACK >> PIN_AC0_bp) & 0b111;

	if (ac_in == (uint8_t) parbus_action::READ)
	{
		*ac = (parbus_action) ac_in;
		return true;
	}

	// ack
	PIN_nACK = 0;
	__builtin_avr_nop();
	__builtin_avr_nop();
	__builtin_avr_nop();

	PIN_nACK = 1;

	*ac = (parbus_action) ac_in;
	*ad = ad_in;

	return true;
}

void parbus_receiver::service_read(uint8_t data)
{
	PINGRP_ADn.vport().DIR = 0xFF;
	PINGRP_ADn.vport().OUT = data;

	// ack
	PIN_nACK = 0;

	// wait for strobe to deassert
	while (!*PIN_nSTB);

	PINGRP_ADn.vport().DIR = 0;
	PIN_nACK = 1;
}

#endif

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
