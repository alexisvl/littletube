# littletube firmware

This source tree is the firmware for Littletube.

Beware, most code here is GPL.

## Building

- Make sure submodules are checked out
- You must have Microchip xc8 installed, by default at
  `/opt/microchip/xc8`, and also the non-Microchip distribution
  of avr-gcc and avr-libc. We do not build using the greedy
  Microchip compiler but do need its spec files.
- Make targets:
    - `all` (default): build all firmware
    - `control`: build controller firmware
    - `video`: build video firmware
    - `clean`: clean up all
- Currently, the firmwares should be flashed with mplab-ipe
  (make target upcoming)

If your xc8 goes somewhere else, you can set environment
variable `MICROCHIP_AVR` to your equivalent of
`/opt/microchip/xc8/v*.*/dfp/xc8/avr`.

## Code structure

- `avlcode/` - Directory containing my general-purpose
  embedded code, including some needed AVR-DX drivers.
- `uC-OS2/` - Micrium µC/OS-II RTOS (note this has been
  released as open-source, Apache license).
- `ucos-avrdx/` - My µC/OS-II AVR-DX port. This is not
  intended for upstreaming and would likely not be
  accepted due to nonstandard additions. In particular,
  I've added functionality for RTOS-aware ISRs written
  in higher-level C/++.
- `ucos-include/` - Config includes for µC/OS-II.
- `control/` - Code specific to the controller.
- `video/` - Code specific to the video processor.
- Common sources go directly in `.`

## System structure - controller

- The controller runs µC/OS-II
- A main task located in `control/main.cpp` drives most
  operations. Control to the main task comes from the
  serial port.
- A monitoring task in `control/monitor.cpp` collects and
  processes data in the background to check for trouble
  (both analog and RTOS).
- A high-priority communications task in `control/vsync.cpp`
  transmits data to the video processor during vsync intervals.

When the video processor is active, RTOS ticks are provided
by the vsync signal (at 60 Hz) to make the context switch
into the communications handler as quick as possible. When
the video processor is switched off, the controller switches
to a local approximately 60 Hz tick provided by the RTC. If
vsync is unexpectedly lost, the watchdog timer which is reset
by the tick handler will reboot the system.

## System structure - video

The video processor spends most of its time emitting video
data via cycle-counted assembly. During vsync, it switches
into a C routine that performs communications with the
controller. When the video output is disabled, the video
processor sits in the vsync routine constantly.
