// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "settings.hpp"

// Supporting modules
#include <avr/eeprom.h>
#include <avl_avrdx.hpp>
#include <avl_avrdx_eeprom.hpp>
#include <util/crc16.h>
#include "hw.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------

static const FDAT settings_block_t _default_settings = {
	.checksum = 0,
	.length = sizeof(settings_block_t),
	.inv_txrx = 0,
	.inv_rtscts = 1,
	.xonxoff = 1,
	.rtscts = 0,
	.twostop = 1,
	.parity = 0,
	._reserved = 0,
	.baud = 115200,
	.vddio2_25mV = static_cast<uint8_t>(3.3 / 0.025),

	.always_setup_mode = true,

	.hvset = static_cast<uint8_t>(VA2_TO_HVSET(800)),
	.filset = static_cast<uint8_t>(V_TO_FILSET(6.3f)),
	.hscale_center = 127,
	.hscale_max = 255,
	.vscale_center = 127,
	.vscale_max = 255,
	.cutoff = static_cast<uint8_t>(V_TO_CUTOFF(-90.f)),
	.inten_hi = 204, // Doesn't need to be adjustable, but depends on hw.
			 // Could be used for ext vga brightness
	.inten_lo = 61, // Doesn't need to be adjustable, but depends on hw
			 // Could be used for ext vga contrast
	.cathgain_res_center = static_cast<uint8_t>(AV_TO_CATHGAIN(10)),
	.cathgain_res_min = static_cast<uint8_t>(AV_TO_CATHGAIN(50)),
	.hpos_center = 127,
	.hpos_max = 255,
	.vpos_center = 127,
	.vpos_max = 255,
};

__attribute__((used))
static EEMEM settings_block_t _eeprom_settings;

settings_block_t settings_block;

// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

/// Compute the checksum for a block, as if blk->checksum were 0.
static uint16_t _compute_checksum(settings_block_t const * blk);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

settings_status settings_init()
{
	eeprom_read_block(
		&settings_block,
		&_eeprom_settings,
		sizeof(settings_block_t)
	);

	if (_compute_checksum(&settings_block) != settings_block.checksum)
	{
		memcpy(
			&settings_block,
			&_default_settings,
			sizeof(settings_block_t)
		);
		return settings_status::CORRUPTED;
	}

	if (settings_block.length < sizeof(settings_block_t))
	{
		uint8_t * blk_data = (uint8_t *)(&settings_block);
		uint8_t const * def_data
			= (uint8_t const *)(&_default_settings);
		memcpy(
			&blk_data[settings_block.length],
			&def_data[settings_block.length],
			sizeof(settings_block_t) - settings_block.length
		);
		return settings_status::EXTENDED;
	}

	return settings_status::LOADED;
}

void settings_commit()
{
	settings_block.checksum = _compute_checksum(&settings_block);
	settings_block.length = sizeof(settings_block_t);
	avrdx_eeprom_update_block(
		&settings_block,
		&_eeprom_settings,
		sizeof(settings_block_t)
	);
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static uint16_t _compute_checksum(settings_block_t const * blk)
{
	size_t len = blk->length;
	if (len > sizeof(settings_block_t))
	{
		len = sizeof(settings_block_t);
	}

	uint8_t const * blk_data = (uint8_t const *) blk;

	uint16_t cksum = 0;
	cksum = _crc16_update(cksum, 0); // ->checksum (low)
	cksum = _crc16_update(cksum, 0); // ->checksum (high)

	for (size_t i = 2; i < len; i += 1)
	{
		cksum = _crc16_update(cksum, blk_data[i]);
	}

	return cksum;
}
