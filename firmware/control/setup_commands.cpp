// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "setup_commands.hpp"

// Supporting modules
#include <util/delay.h>
#include "hw.hpp"
#include "settings.hpp"
#include "modes.hpp"
#include "analog.hpp"
#include <avl_parsenums.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>

#pragma GCC optimize ("-Os")

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------

struct cmd_t {
	char const * name;
	void (*fun)(int argc, char ** argv);
	char const * desc;
};

// --- COMMAND DECLARATIONS ----------------------------------------------------

static void _cmd_commit(int argc, char ** argv);
static void _cmd_crash(int argc, char ** argv);
static void _cmd_help(int argc, char ** argv);
static void _cmd_i2c(int argc, char ** argv);
static void _cmd_info(int argc, char ** argv);
static void _cmd_mem(int argc, char ** argv);
static void _cmd_reboot(int argc, char ** argv);
static void _cmd_setting(int argc, char ** argv);
static void _cmd_up(int argc, char ** argv);
static void _cmd_vid(int argc, char ** argv);
static void _cmd_gpio(int argc, char ** argv);
static void _cmd_pat(int argc, char ** argv);

// --- PRIVATE CONSTANTS -------------------------------------------------------

// Silly X-macro bullshit, but I can't figure out a way to get FSTR() to work
// in a struct literal
#define XCOMMANDS \
X(commit,	_cmd_commit,	"commit settings") \
X(crash,	_cmd_crash,	"demo crash") \
X(help,		_cmd_help,	"display help")    \
X(i2c,		_cmd_i2c,	"raw I2C txrx") \
X(info,		_cmd_info,	"device/system info") \
X(mem,		_cmd_mem,	"peek or poke") \
X(reboot,	_cmd_reboot,	"reboot") \
X(setting,	_cmd_setting,	"r/w a setting") \
X(up,		_cmd_up,	"bring up a supply") \
X(vid,		_cmd_vid,	"video processor commands") \
X(gpio,		_cmd_gpio,	"read/set gpio") \
X(pat,		_cmd_pat,	"write test pattern") \

#define X(name,fun,desc) \
	static FDAT const char _cmdname_ ## name [] = # name; \
	static FDAT const char _cmddesc_ ## name [] = desc;
XCOMMANDS
#undef X

#define X(name,fun,desc) \
	{ _cmdname_ ## name, fun, _cmddesc_ ## name },
static FDAT const cmd_t _commands[] = {
	XCOMMANDS
};
#undef X


#define N_COMMANDS (sizeof(_commands)/sizeof(_commands[0]))
#define MAX_ARGV 4

// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

/// Print the OK string
static void _ok();

/// Parse a command into argc and argv
static void _parse(char * cmd, int * argc, char ** argv, int max_argv);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void run_setup_command(char * cmd)
{
	int argc = 0;
	char *argv[MAX_ARGV];

	_parse(cmd, &argc, &argv[0], MAX_ARGV);

	if (argv[0] == NULL)
	{
		return;
	}

	for (size_t i = 0; i < N_COMMANDS; i += 1)
	{
		if (!strcmp(argv[0], _commands[i].name))
		{
			_commands[i].fun(argc, argv);
			return;
		}
	}

	comport.printf(FSTR("*ERR no such command\r\n"));
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _cmd_commit(int argc, char ** argv)
{
	(void) argc;
	(void) argv;
	settings_commit();
	_ok();
}

static void _cmd_crash(int argc, char ** argv)
{
	if (argc == 2)
	{
		uint8_t code = atoi(argv[1]);
		ltm::crash((ltm::crash_code) code);
	}
	else
	{
		comport.printf(FSTR("*ERR expected argument\r\n"));
	}
}

static void _cmd_help(int argc, char ** argv)
{
	(void) argc;
	(void) argv;
	comport.printf(FSTR("littletube setup mode commands:\r\n\r\n"));

	for (size_t i = 0; i < N_COMMANDS; i += 1)
	{
		comport.printf(
			FSTR("%-12s - %s\r\n"),
			_commands[i].name, _commands[i].desc
		);
	}

	_ok();
}

static void _cmd_i2c(int argc, char ** argv)
{
	if (argc < 3)
	{
		comport.printf(FSTR("i2c ADDR TX... [RXN]\r\n"));
		comport.printf(FSTR("limit 8 bytes each, TX in hex\r\n"));
		_ok();
		return;
	}

	uint8_t buffer[8] = {0};
	i2c_packet packet = {
		.addr = (uint8_t) parse_int(argv[1], 0).ok_or(0),
		.n_out = 0,
		.n_in = 0,
		.timeout = OS_TICKS_PER_SEC,
		.buffer = buffer
	};

	if (argc == 4)
		packet.n_in = parse_int(argv[3], 0).ok_or(0);

	bool second_nibble = false;
	for (size_t i = 0; argv[2][i]; i += 1)
	{
		buffer[packet.n_out] <<= 4;
		buffer[packet.n_out] |= parse_hex_nibble(argv[2][i]).ok_or(0);

		if (second_nibble)
			packet.n_out += 1;
		second_nibble = !second_nibble;

		if (packet.n_out == sizeof(buffer))
			break;
	}

	error_t e = I2C.txrx(&packet);

	if (e == error_t::OK)
	{
		for (size_t i = 0; i < packet.n_in; i += 1)
			comport.printf(FSTR("%02X "), packet.buffer[i]);
		if (packet.n_in)
			comport.printf(FSTR("\r\n"));
		_ok();
	}
	else
	{
		comport.printf(FSTR("*ERR %s\r\n"), error_name(e));
	}
}

static void _cmd_info(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

	uint8_t const ucos_major = OS_VERSION / 10000;
	uint8_t const ucos_minor = (OS_VERSION % 10000) / 100;
	uint8_t const ucos_patch = OS_VERSION % 100;

	comport.printf(
		FSTR("Build date:    " BUILD_DATE "\r\n")
	);
	comport.printf(
		FSTR("Build hash:    " BUILD_HASH "\r\n")
	);
	comport.printf(
		FSTR("VP build hash: %s\r\n"),
		parbus_vp_get_build()
	);

	comport.printf(
		FSTR("uC/OS-II ver:  %u.%02u.%02u\r\n"),
		ucos_major,
		ucos_minor,
		ucos_patch
	);

	comport.printf(
		FSTR("AVR device id: %02X %02X %02X\r\n"),
		SIGROW.DEVICEID0,
		SIGROW.DEVICEID1,
		SIGROW.DEVICEID2
	);

	comport.printf(
		FSTR("AVR revision : %c%u\r\n"),
		((SYSCFG.REVID >> 4) & 0xF) + 'A',
		SYSCFG.REVID & 0xF
	);

	comport.printf(FSTR("AVR serial no:"));
	for (int i = 0; i < 16; i += 1)
	{
		comport.printf(FSTR(" %02X"), (&SIGROW.SERNUM0)[i]);
	}
	comport.printf(FSTR("\r\n"));

	_ok();
}

static void _cmd_mem(int argc, char ** argv)
{
	uint16_t addr = 0;
	uint8_t volatile * p = NULL;
	uint8_t value = 0;

	switch (argc)
	{
	default:
	case 1:
		comport.printf(FSTR("mem ADDR(hex) [VALUE]\r\n"));
		_ok();
		break;
	case 2:
		addr = parse_int(argv[1], 16).ok_or(0);
		p = (uint8_t volatile *) addr;
		comport.printf(FSTR("0x%04X = 0x%02X\r\n"), addr, *p);
		_ok();
		break;
	case 3:
		addr = parse_int(argv[1], 16).ok_or(0);
		value = parse_int(argv[2], 0).ok_or(0);
		p = (uint8_t volatile *) addr;
		if (addr)
		{
			*p = value;
			_ok();
		}
		else
		{
			comport.printf(FSTR("*ERR null\r\n"));
		}
		break;
	}
}

static void _cmd_reboot(int argc, char ** argv)
{
	(void) argc;
	(void) argv;
	_ok();
	OSTimeDlyHMSM(0, 0, 2, 0);
	cli();
	hw_pins_safe();
	_PROTECTED_WRITE(RSTCTRL.SWRR, RSTCTRL_SWRST_bm);
	for(;;);
}

#define PRINTSET(name, type, field) \
comport.printf(FSTR("%s = " type "\r\n"), FSTR(name), settings_block.field)
static void _cmd_setting(int argc, char ** argv)
{
	if (argc == 1 || argc > 3)
	{
		comport.printf(FSTR("usage: setting list\r\n"));
		comport.printf(FSTR("       setting NAME - read\r\n"));
		comport.printf(FSTR("       setting NAME VALUE - write\r\n"));
		_ok();
	}
	else if (argc == 2)
	{
		bool list_all = !strcmp(argv[1], FSTR("list"));
		bool match = false;

		if (list_all || !strcmp(argv[1], FSTR("inv_txrx")))
		{
			PRINTSET("inv_txrx", "%u", inv_txrx);
			match = true;
		}
		if (list_all || !strcmp(argv[1], FSTR("inv_rtscts")))
		{
			PRINTSET("inv_rtscts", "%u", inv_rtscts);
			match = true;
		}
		if (list_all || !strcmp(argv[1], FSTR("xonxoff")))
		{
			PRINTSET("xonxoff", "%u", xonxoff);
			match = true;
		}
		if (list_all || !strcmp(argv[1], FSTR("rtscts")))
		{
			PRINTSET("rtscts", "%u", rtscts);
			match = true;
		}
		if (list_all || !strcmp(argv[1], FSTR("twostop")))
		{
			PRINTSET("twostop", "%u", twostop);
			match = true;
		}
		if (list_all || !strcmp(argv[1], FSTR("parity")))
		{
			PRINTSET("parity", "%u", parity);
			match = true;
		}
		if (list_all || !strcmp(argv[1], FSTR("baud")))
		{
			PRINTSET("baud", "%lu", baud);
			match = true;
		}
		if (list_all || !strcmp(argv[1], FSTR("vddio")))
		{
			PRINTSET("vddio", "%u", vddio2_25mV * 25);
			match = true;
		}
		if (!match)
		{
			comport.printf(FSTR("*ERR bad setting\r\n"));
			return;
		}
		_ok();
	}
	else if (argc == 3)
	{
		uint32_t value = 0;
		int scanned = sscanf(argv[2], "%lu", &value);
		if (scanned != 1)
		{
			comport.printf(FSTR("*ERR bad argument\r\n"));
			return;
		}

		bool valid = false;

		if (!strcmp(argv[1], FSTR("inv_txrx")))
		{
			if ((valid = value <= 1))
				settings_block.inv_txrx = value;
		}
		else if (!strcmp(argv[1], FSTR("inv_rtscts")))
		{
			if ((valid = value <= 1))
				settings_block.inv_rtscts = value;
		}
		else if (!strcmp(argv[1], FSTR("xonxoff")))
		{
			if ((valid = value <= 1))
				settings_block.xonxoff = value;
		}
		else if (!strcmp(argv[1], FSTR("rtscts")))
		{
			if ((valid = value <= 1))
				settings_block.rtscts = value;
		}
		else if (!strcmp(argv[1], FSTR("twostop")))
		{
			if ((valid = value <= 1))
				settings_block.twostop = value;
		}
		else if (!strcmp(argv[1], FSTR("parity")))
		{
			if ((valid = value <= 2))
				settings_block.parity = value;
		}
		else if (!strcmp(argv[1], FSTR("baud")))
		{
			if ((valid = (value >= 1500) && (value <= 3000000)))
				settings_block.baud = value;
		}
		else if (!strcmp(argv[1], FSTR("vddio")))
		{
			if ((valid = (value >= 1800) && (value <= 5000)))
				settings_block.vddio2_25mV = value / 25;
		}
		if (!valid)
		{
			comport.printf(FSTR("*ERR bad setting\r\n"));
			return;
		}
		_ok();
	}
}

static void _cmd_up(int argc, char ** argv)
{
	if (argc == 1)
	{
		comport.printf(FSTR("usage: up SUPPLY [vset]\r\n"));
		comport.printf(FSTR("supplies: 50, FIL, VA2\r\n"));
		_ok();
		return;
	}

	if (!strcmp(argv[1], FSTR("50")))
	{
		// TODO factor this out, should be part of system modes
		PIN_50VENABLE << port_dir::output_1;
	}
	else if (!strcmp(argv[1], FSTR("FIL")))
	{
		if (argc != 3)
		{
			comport.printf(FSTR("*ERR vset required (mV)\r\n"));
			return;
		}
		uint16_t mv = (uint16_t) parse_int(argv[2], 0).ok_or(0);

		if (mv < 4200 || mv > 7500)
		{
			comport.printf(FSTR("*ERR vset out of range: 4200-7500\r\n"));
			return;
		}
		lta::set_vfil_100mV(mv/100);
	}
	else if (!strcmp(argv[1], FSTR("VA2")))
	{
		if (argc != 3)
		{
			comport.printf(FSTR("*ERR vset required\r\n"));
			return;
		}
		uint16_t v = (uint16_t) parse_int(argv[2], 0).ok_or(0);

		if (v < 700 || v > 1000)
		{
			comport.printf(FSTR("*ERR vset out of range: 700-1000\r\n"));
			return;
		}
		lta::set_va2(v, HV_KEY);
	}
	else
	{
		comport.printf(FSTR("*ERR bad supply\r\n"));
		return;
	}
}

static void _cmd_vid(int argc, char ** argv)
{
	if (argc == 1)
	{
		comport.printf(FSTR(
			"usage: vid sleep | wake | wr ADDR BYTW | rd ADDR "
			"| benchwr | benchrd\r\n"
		));
		_ok();
		return;
	}

	if (!strcmp(argv[1], FSTR("sleep")))
	{
		parbus_vp_sleep();
		_ok();
	}
	else if (!strcmp(argv[1], FSTR("wake")))
	{
		parbus_vp_wake();
		_ok();
	}
	else if (!strcmp(argv[1], FSTR("wr")))
	{
		if (argc != 4)
		{
			comport.printf(FSTR("*ERR num args\r\n"));
			return;
		}
		auto addr_r = parse_int(argv[2], 0);
		auto data_r = parse_int(argv[3], 0);

		if (addr_r.is_err() || data_r.is_err())
		{
			comport.printf(FSTR("*ERR format\r\n"));
			return;
		}

		if (addr_r.ok() > 0xFFFFu || data_r.ok() > 0xFFu)
		{
			comport.printf(FSTR("*ERR range\r\n"));
			return;
		}

		parbus.write_addr((uint16_t) addr_r.ok());
		parbus.write(parbus_action::WRITE_8, (uint8_t) data_r.ok());
		_ok();
	}
	else if (!strcmp(argv[1], FSTR("rd")))
	{
		if (argc != 3)
		{
			comport.printf(FSTR("*ERR num args\r\n"));
			return;
		}
		auto addr_r = parse_int(argv[2], 0);

		if (addr_r.is_err())
		{
			comport.printf(FSTR("*ERR format\r\n"));
			return;
		}

		if (addr_r.ok() > 0xFFFFu)
		{
			comport.printf(FSTR("*ERR range\r\n"));
			return;
		}

		parbus.write_addr((uint16_t) addr_r.ok());

		result_t<uint8_t> data_r = parbus.read();

		if (data_r.is_err())
		{
			comport.printf(
				FSTR("*ERR %s\r\n"),
				error_describe(data_r.err())
			);
		}
		else
		{
			comport.printf("*OK 0x%02X\r\n", data_r.ok());
		}
	}
	else if (!strcmp(argv[1], FSTR("benchwr")))
	{
		uint32_t t = OSTimeGet();

		// sync to the next tick
		while (t == OSTimeGet());
		t++;

		uint32_t n = 0;
		uint16_t count_reset = 0;
		parbus.write_addr(0);
		while (OSTimeGet() < (t + 60))
		{
			n++;
			count_reset++;
			parbus.write(parbus_action::WRITE_8, 0);
			if (count_reset == 63500)
			{
				parbus.write_addr(0);
				count_reset = 0;
			}
		}

		comport.printf(FSTR("*OK %" PRIu32 " ops in 1 sec\r\n"), n);
	}
	else if (!strcmp(argv[1], FSTR("benchrd")))
	{
		uint32_t t = OSTimeGet();

		// sync to the next tick
		while (t == OSTimeGet());
		t++;

		uint32_t n = 0;
		uint16_t count_reset = 0;
		parbus.write_addr(0);
		while (OSTimeGet() < (t + 60))
		{
			n++;
			count_reset++;
			parbus.read();
			if (count_reset == 63500)
			{
				parbus.write_addr(0);
				count_reset = 0;
			}
		}

		comport.printf(FSTR("*OK %" PRIu32 " ops in 1 sec\r\n"), n);
	}
	else
	{
		comport.printf(FSTR("*ERR bad cmd\r\n"));
		return;
	}
}

static void _cmd_gpio(int argc, char ** argv)
{
	if (argc == 1 || argc > 3)
	{
		comport.printf(FSTR(
			"usage: gpio A1 [1|0|Z]\r\n"
		));
		_ok();
		return;
	}

	PORT_t * gpio = nullptr;
	int pin = -1;

	switch (argv[1][0])
	{
	case 'A': case 'a': gpio = &PORTA; break;
	case 'B': case 'b': gpio = &PORTB; break;
	case 'C': case 'c': gpio = &PORTC; break;
	case 'D': case 'd': gpio = &PORTD; break;
	case 'E': case 'e': gpio = &PORTE; break;
	case 'F': case 'f': gpio = &PORTF; break;
	case 'G': case 'g': gpio = &PORTG; break;
	}

	if (argv[1][1] >= '0' && argv[1][1] <= '9')
	{
		pin = argv[1][1] - '0';
	}


	if (!gpio || pin < 0)
	{
		comport.printf(FSTR("*ERR arg\r\n"));
		return;
	}

	if (argc == 2)
	{
		comport.printf(FSTR("*OK %d\r\n"),
			(gpio->IN & (1 << pin)) ? 1 : 0
		);
	}
	else if (!strcmp(argv[2], FSTR("1")))
	{
		_ok();
		gpio->DIRSET = 1 << pin;
		gpio->OUTSET = 1 << pin;
	}
	else if (!strcmp(argv[2], FSTR("0")))
	{
		_ok();
		gpio->DIRSET = 1 << pin;
		gpio->OUTCLR = 1 << pin;
	}
	else if (!strcmp(argv[2], FSTR("Z")) || !strcmp(argv[2], FSTR("z")))
	{
		_ok();
		gpio->DIRCLR = 1 << pin;
	}
	else
	{
		comport.printf(FSTR("*ERR arg\r\n"));
		return;
	}
}

static void _cmd_pat(int argc, char ** argv)
{
	if (argc != 3)
	{
		comport.printf(FSTR("usage: pat 320|240 cb|vbar|dither\r\n"));
		_ok();
		return;
	}

	uint16_t w, h;

	if (!strcmp(argv[1], FSTR("320")))
	{
		w = 320;
		h = 200;
	}
	else if (!strcmp(argv[1], FSTR("240")))
	{
		w = 240;
		h = 240;
	}
	else
	{
		comport.printf(FSTR("*ERR invalid\r\n"));
		return;
	}

	if (!strcmp(argv[2], FSTR("cb")))
	{
		// 24x24 squares
		parbus.write_addr(0);

		for (uint16_t y = 0; y < h; y += 1)
		{
			parbus.write_addr(y * w);
			if (y % 48 < 24)
				for (uint16_t x = 0; x < w; x += 4)
				{
					if (x % 48 < 24)
						parbus.write(parbus_action::WRITE_8, 0x00);
					else
						parbus.write(parbus_action::WRITE_8, 0xFF);
				}
			else
				for (uint16_t x = 0; x < w; x += 4)
				{
					if (x % 48 < 24)
						parbus.write(parbus_action::WRITE_8, 0xFF);
					else
						parbus.write(parbus_action::WRITE_8, 0x00);
				}
		}
	}
	else if (!strcmp(argv[2], FSTR("dither")))
	{
		for (uint16_t y = 0; y < h; y += 1)
		{
			parbus.write_addr(y * w);
			if (y % 2)
				for (uint16_t x = 0; x < w; x += 4)
					parbus.write(parbus_action::WRITE_8, 0x33);
			else
				for (uint16_t x = 0; x < w; x += 4)
					parbus.write(parbus_action::WRITE_8, 0xCC);
		}
	}
	else if (!strcmp(argv[2], FSTR("vbar")))
	{
		for (uint16_t y = 0; y < h; y += 1)
		{
			parbus.write_addr(y * w);
			for (uint8_t x = 0; x < w/4; x += 1)
			{
				if (x % 12 < 6)
					parbus.write(parbus_action::WRITE_8, 0x00);
				else
					parbus.write(parbus_action::WRITE_8, 0xFF);
			}
		}
	}
}

static void _ok()
{
	comport.printf(FSTR("*OK\r\n"));
}

static void _parse(char * cmd, int * argc, char ** argv, int max_argv)
{
	char * saveptr = NULL;
	char * token = strtok_r(cmd, FSTR(" "), &saveptr);

	if (!token)
	{
		*argc = 0;
		return;
	}

	argv[0] = token;
	*argc = 1;

	while (*argc < max_argv)
	{
		token = strtok_r(NULL, FSTR(" "), &saveptr);

		if (!token)
		{
			break;
		}

		argv[*argc] = token;
		*argc += 1;
	}
}
