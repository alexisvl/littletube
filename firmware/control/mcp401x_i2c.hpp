// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef MCP401X_I2C_HPP
#define MCP401X_I2C_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avl_i2c_i.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Microchip MCP401x I2C digital potentiometers (4017, 4018, 4019).
struct mcp401x_i2c
{
	static constexpr uint8_t ADDR = 0x5E;
	static constexpr uint8_t MAX_VAL = 127;

	/// Construct a mcp401x_i2c instance.
	///
	/// @param i2c - i2c driver
	/// @param addr (optional) - left-aligned address. As there is only one
	///     valid address for this chip, this defaults to mcp401x_i2c::ADDR.
	mcp401x_i2c(avl::i2c_i * i2c, uint8_t addr = ADDR);

	/// Check that the MCP401x responds. There is no device info register,
	/// so this can only check that it acks its address.
	///
	/// There is no need to call this, it is only provided in case it can
	/// be useful. Calling ::get(), or ::set() with a default value, would
	/// also provide the same functionality.
	///
	/// Blocking.
	avl::error_t check();

	/// Set the value of the potentiometer, in wiper steps.
	///
	/// Blocking.
	avl::error_t set(uint8_t value);

	/// Get the value of the potentiometer, in wiper steps.
	///
	/// Blocking.
	avl::result_t<uint8_t> get();

private:
	avl::i2c_i * m_i2c;
	uint8_t m_addr;
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#endif // !defined(MCP401X_I2C_HPP)
