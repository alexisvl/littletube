// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <ucos_ii.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avl_avrdx.hpp>
#include "ascii.h"
#include "settings.hpp"
#include "hw.hpp"
#include "modes.hpp"
#include "setup_commands.hpp"
#include "avrdx_opamp_cal.hpp"
#include "analog.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>
#include <ctype.h>

// This module
#include "main.hpp"

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static void _main_task(void * pd);

/// Set up I2C.
static error_t _setup_i2c();

/// Set up the serial port from settings.
static error_t _setup_serial();

/// Setup mode loop. Accepts human-typed commands, displays a prompt.
static void _run_setup_mode();

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

static uint8_t _main_task_stack[256];

// --- PUBLIC FUNCTIONS --------------------------------------------------------

error_t littletube::start_main_task()
{
	uint8_t e = OSTaskCreateExt(
		_main_task,
		NULL, // pdata
		&_main_task_stack[sizeof(_main_task_stack) - 1], // ptos
		MAIN_TASK_PRIO, // prio
		MAIN_TASK_PRIO, // id
		&_main_task_stack[0], // pbos
		sizeof(_main_task_stack), // stk_size
		NULL, // pext
		OS_TASK_OPT_STK_CHK // opt
	);

	return error_from_ucos(e);
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _main_task(void * pd)
{
	(void) pd;

	uint8_t reset_flags = RSTCTRL.RSTFR;
	RSTCTRL.RSTFR = reset_flags;
	if (reset_flags & RSTCTRL_WDRF_bm) {
		ltm::crash(ltm::crash_code::CRASH_WDT);
	}

	OSStatInit();

	settings_status stat = settings_init();
	error_t i2c_e = _setup_i2c();
	error_t lta_e = lta::start_adc_task();

	if (lta_e != error_t::OK)
		ltm::crash(ltm::crash_code::CRASH_START_LTA);

	error_t ser_e = _setup_serial();

	if (ser_e != error_t::OK)
		ltm::crash(ltm::crash_code::CRASH_OTHER_COMPORT);

	lta::startup();

	if (i2c_e != error_t::OK)
		comport.printf(FSTR("*ERR i2c %s\r\n"), error_name(i2c_e));

	if (stat == settings_status::LOADED)
	{
		comport.printf(FSTR("*INF settings loaded\r\n"));
	}
	else if (stat == settings_status::EXTENDED)
	{
		comport.printf(
			FSTR("*INF settings loaded and updated\r\n")
		);
	}
	else
	{
		comport.printf(
			FSTR("*INF settings corrupted; reinitialized\r\n")
		);
	}

	lta::set_inten_hi(200);
	lta::set_inten_lo(86);

	error_t vp_e = parbus_vp_startup();
	if (vp_e == error_t::OK)
	{
		comport.printf(FSTR("*INF video processor up\r\n"));
	}
	else
	{
		comport.printf(FSTR("*ERR %s starting video processor\r\n"),
			error_name(vp_e));
	}

	if (settings_block.always_setup_mode
			|| ltm::get_mode() == ltm::mode::SETUP)
		_run_setup_mode();
	else
	{
		ltm::crash(FSTR("normal mode not implemented"));
	}

	for (;;);
}

static error_t _setup_serial()
{
	PORTMUX.USARTROUTEA = 0;
	if (comport.init() != error_t::OK)
	{
		ltm::crash(ltm::crash_code::CRASH_COMPORT_INIT);
	}

	if (settings_block.inv_txrx)
	{
		PIN_SER_OUT << port_pinctrl::reset + port_pinctrl::invert::yes;
		PIN_SER_IN  << port_pinctrl::reset + port_pinctrl::invert::yes;
	}
	else
	{
		PIN_SER_OUT << port_pinctrl::reset;
		PIN_SER_IN  << port_pinctrl::reset;
	}

	if (settings_block.inv_rtscts)
	{
		PIN_SER_RTS << port_pinctrl::reset + port_pinctrl::invert::yes;
		PIN_SER_CTS << port_pinctrl::reset + port_pinctrl::invert::yes;
	}
	else
	{
		PIN_SER_RTS << port_pinctrl::reset;
		PIN_SER_CTS << port_pinctrl::reset;
	}

	PIN_SER_OUT << port_dir::output_1;
	PIN_SER_CTS << port_dir::output_1;
	comport.input_xonxoff(settings_block.xonxoff);
	comport.output_xonxoff(settings_block.xonxoff);
	comport.rtscts(settings_block.rtscts);

	comport.usart() <<
		usart_ctrlc::none
		+ usart_ctrlc::cmode::async
		+ usart_ctrlc::chsize::eight;

	if (settings_block.twostop)
	{
		comport.usart() << usart_ctrlc::stopbits::two;
	}

	if (settings_block.parity == 1)
	{
		comport.usart() << usart_ctrlc::parity::odd;
	}
	else if (settings_block.parity != 0)
	{
		comport.usart() << usart_ctrlc::parity::even;
	}

	uint32_t const baud = settings_block.baud;
	result_t<usart_baud_t> b1x = usart_baud::calc_baud(baud, F_CPU);
	result_t<usart_baud_t> b2x = usart_baud::calc_baud_clk2x(baud, F_CPU);

	if (b1x.is_ok())
	{
		comport.usart() << b1x.ok();
		comport.usart() <<
			avl::usart_ctrlb::txen::yes
			+ avl::usart_ctrlb::rxen::yes
			+ avl::usart_ctrlb::rxmode::normal;
	}
	else if (b2x.is_ok())
	{
		comport.usart() << b2x.ok();
		comport.usart() <<
			avl::usart_ctrlb::txen::yes
			+ avl::usart_ctrlb::rxen::yes
			+ avl::usart_ctrlb::rxmode::clk2x;
	}
	else
	{
		ltm::crash(ltm::crash_code::CRASH_BAD_SERIAL);
	}

	comport.usart() << avl::usart_ctrla::none
		+ avl::usart_ctrla::rxcie::yes
		;

	comport_ready = true;

	return error_t::OK;
}

static error_t _setup_i2c()
{
	I2C.init_pins<typeof(PIN_SDA), typeof(PIN_SCL)>(true);

	avrdx_twi::cfg cfg = {
		.fast_mode_plus = false,
		.baudreg = 108, // 100 kHz
	};
	error_t e = I2C.init(cfg);

	return e;
}

static void _run_setup_mode()
{
	char buffer[100];
	uint8_t index = 0;

	comport.printf(FSTR("\r\nsetup> "));

	bool first_key = false;

	for (;;)
	{
		result_t<uint8_t> r = comport.read(OS_TICKS_PER_SEC * 2);

		if (!r.is_ok())
		{
			if (!first_key)
			{
				// Repeat the prompt until the first keypress
				comport.printf(FSTR("\r\nsetup> "));
			}
			continue;
		}

		// Sometimes connecting power sends a break/0
		char c = (char) r.ok();

		if (c)
			first_key = true;

		if (isprint(c))
		{
			if (index < sizeof(buffer) - 1)
			{
				buffer[index++] = c;
				comport.write(r.ok(), 0xFFFF);
			}
		}
		else if ((c == ASCII_BS || c == ASCII_DEL) && index > 0)
		{
			index -= 1;
			comport.printf(FSTR("\b \b"));
		}
		else if ((c == ASCII_CR || c == ASCII_LF) && index > 0)
		{
			buffer[index++] = 0;
			comport.printf(FSTR("\r\n"));
			run_setup_command(buffer);
			comport.printf(FSTR("\r\nsetup> "));
			index = 0;
		}
		else if (c == ASCII_ETX)
		{
			comport.printf(FSTR("^C\r\nsetup> "));
			index = 0;
		}
	}
}
