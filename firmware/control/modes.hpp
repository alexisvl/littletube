// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef MODES_HPP
#define MODES_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avl_error.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace ltm {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

enum class mode {
	/// Low-power mode.
	///
	/// Clock sources: low freq, no ticks (no RTOS)
	/// Video processor: shut down
	/// HV supplies locked out
	///
	/// Execution of this mode is special; it cannot be switched into
	/// directly by a running system. Calling switch_mode(mode::SHUTDOWN)
	/// executes a software reset of the microcontroller.
	///
	/// At boot, the system should call initial_mode() after setting pins
	/// to safe states. This mode will determine the initial mode for the
	/// system and begin executing it; the function will return once entry
	/// into an operational mode (SETUP or READY) occurs.
	SHUTDOWN,

	/// Setup mode.
	///
	/// Clock sources: crystal, RTC ticks
	/// Clock output: yes
	/// Video processor: constant vsync
	/// HV supplies locked out
	/// Console initialized from Setup Mode Console Settings
	/// Console runs in Setup Mode
	SETUP,

	/// Ready, but not running.
	///
	/// Clock sources: crystal, RTC ticks
	/// Clock output: yes
	/// Video processor: constant vsync
	READY,

	/// Running.
	///
	/// Clock sources: crystal, vsync
	/// Clock output: yes
	/// Video processor: running
	///
	/// Does not enable video buffers (including CRT driver)
	RUNNING,

	/// Error.
	///
	/// Clock sources: low freq, no ticks (no RTOS)
	/// Video processor: shut down
	/// HV supplies: locked out
	///
	/// Entry into this mode deactivates the RTOS; switch_mode() will not
	/// return.
	///
	/// If a valid VDDIO2 is present, the console will emit an error
	/// message periodically.
	///
	/// The #ERROR/SHDN pin is pulled low.
	///
	/// A blink code will be emitted from the red LED.
	ERROR,
};

/// Crash codes. These should be defined starting at 2, which is the first one
/// whose function as an actual _code_ is visually obvious.
enum crash_code {
	DO_NOT_USE_0 = 0,
	DO_NOT_USE_1 = 1,

	/// The serial port settings in EEPROM are invalid, so we can't bring
	/// up the com port. You can force a reset using the SETUP_MODE jumper.
	CRASH_BAD_SERIAL = 0x2,

	/// The watchdog timer expired. This typically means the video
	/// processor, which delivers the vsync interrupt, crashed. Power cycle
	/// to try again.
	CRASH_WDT = 0x3,

	/// A pure virtual method was called (internal code error)
	CRASH_PURE_VIRTUAL = 0x4,

	/// An analog.hpp/lta:: function that communicates with the ADC task
	/// was called before initializing the ADC task.
	CRASH_LTA_NOT_INIT = 0x5,

	/// Crystal oscillator failed to start
	CRASH_XTAL_FAIL = 0x6,

	/// Video processor didn't respond at startup
	CRASH_VP_FAIL = 0x7,

	CRASH_COMPORT_INIT = 0b10000,
	CRASH_OTHER_COMPORT = 0b10001,
	CRASH_START_LTA = 0b10010,
	CRASH_OS_3 = 0b10011,
	CRASH_OS_4 = 0b10100,
	CRASH_OS_5 = 0b10101,
	CRASH_OS_6 = 0b10110,
	CRASH_OS_7 = 0b10111,
	CRASH_OS_8 = 0b11000,
	CRASH_OS_9 = 0b11001,
	CRASH_OS_A = 0b11010,
	CRASH_OS_B = 0b11011,
	CRASH_OS_C = 0b11100,
	CRASH_OS_D = 0b11101,
	CRASH_OS_E = 0b11110,
	CRASH_OS_F = 0b11111,
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Get the current mode.
mode get_mode();

/// Switch modes.
///
/// Do not use this to enter mode::ERROR (otherwise, this will trigger an
/// Unspecified Error state). Instead, use crash() to declare which error
/// indication is to be delivered.
avl::error_t switch_mode(mode m);

/// Crash the system. Declare an error, then enter mode::ERROR. This variant is
/// for higher-level errors that do not require a blink code. The Program
/// Counter Blink Code will be used instead: the red LED flashes rapidly, while
/// the green LED blinks out the program counter from which this was called.
///
/// @param e - error message
__attribute__((noreturn, noinline))
void crash(char const * e);

/// Crash the system. Declare an error, then enter mode::ERROR.
///
/// @param code - blink code to emit on the red LED
/// @param e - error message. Can be null
__attribute__((noreturn))
void crash(uint8_t code, char const * e = nullptr);

/// Execute the initial system mode. This function will block until an RTOS
/// mode (SETUP, READY, RUNNING) is reached, then return the mode into which
/// we are entering.
mode initial_mode();

} // ltm

#endif // !defined(MODES_HPP)
