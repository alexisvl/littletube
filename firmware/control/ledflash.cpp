// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "ledflash.hpp"

// Supporting modules
#include "hw.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/wdt.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

/// Delay, while resetting the watchdog periodically
static void wdt_delay_ms(uint16_t n)
{
	while (n > 10)
	{
		wdt_reset();
		_delay_ms(10);
		n -= 10;
	}
}

template<int Time>
static void flash(ledflash_led led)
{
	if (led == ledflash_led::RED)
		PIN_LED_RED << port_dir::output_1;
	else
		PIN_LED_GRN << port_dir::output_1;

	wdt_delay_ms(Time);

	if (led == ledflash_led::RED)
		PIN_LED_RED << port_dir::output_0;
	else
		PIN_LED_GRN << port_dir::output_0;
}

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ledflash(uint16_t code, ledflash_led led)
{
	wdt_reset();
	bool first_bit = false;
	for (int i = 0; i < 16; i += 1)
	{
		if (code & 0x8000u)
		{
			flash<80>(led);
			wdt_delay_ms(80);
			flash<80>(led);
			wdt_delay_ms(80);
			first_bit = true;
			wdt_delay_ms(500);
		}
		else if (first_bit)
		{
			flash<80>(led);
			wdt_delay_ms(240);
			wdt_delay_ms(500);
		}
		code <<= 1;
	}
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
