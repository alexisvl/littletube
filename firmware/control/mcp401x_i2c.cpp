// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "mcp401x_i2c.hpp"

// Supporting modules
#include <ucos_ii.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

mcp401x_i2c::mcp401x_i2c(i2c_i * i2c, uint8_t addr)
	: m_i2c(i2c)
	, m_addr(addr)
{}

error_t mcp401x_i2c::check()
{
	i2c_packet packet = {
		.addr = m_addr,
		.n_out = 0,
		.n_in = 0,
		.timeout = OS_TICKS_PER_SEC,
		.buffer = nullptr,
	};
	return m_i2c->txrx(&packet);
}

error_t mcp401x_i2c::set(uint8_t value)
{
	if (value > mcp401x_i2c::MAX_VAL)
		return error_t::INVAL;

	uint8_t buffer[] = {value};
	i2c_packet packet = {
		.addr = m_addr,
		.n_out = 1,
		.n_in = 0,
		.timeout = OS_TICKS_PER_SEC,
		.buffer = buffer,
	};

	return m_i2c->txrx(&packet);
}

result_t<uint8_t> mcp401x_i2c::get()
{
	uint8_t buffer[1] = {0};
	i2c_packet packet = {
		.addr = m_addr,
		.n_out = 0,
		.n_in = 1,
		.timeout = OS_TICKS_PER_SEC,
		.buffer = buffer,
	};

	error_t e = m_i2c->txrx(&packet);

	if (e != error_t::OK)
		return e;

	return (uint8_t) buffer[0];
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
