// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef LEDFLASH_HPP
#define LEDFLASH_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

enum class ledflash_led: uint8_t {
	RED = 0,
	GREEN = 1,
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Flash a code out an LED. The code is emitted as a sequence of double and
/// single flashes, with a double representing a 1 and a single representing
/// a 0, MSB first. Leading zeros are omitted.
///
/// @param code - code to flash
/// @param led - LED to use
void ledflash(uint16_t code, ledflash_led led);

#endif // !defined(LEDFLASH_HPP)
