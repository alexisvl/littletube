// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avr/io.h>
#include <avl_error.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Settings block. Stored in RAM and EEPROM.
typedef struct __attribute__((packed)) {
	/// Fletcher16 checksum of entire struct, with this field zeroed
	uint16_t checksum;
	/// Length of entire struct in bytes
	uint8_t  length;

	//--- COM PORT SETTINGS ---
	uint8_t  inv_txrx   : 1;
	uint8_t  inv_rtscts : 1;
	uint8_t  xonxoff    : 1;
	uint8_t  rtscts     : 1;
	uint8_t  twostop    : 1;
	uint8_t  parity     : 2;
	uint8_t  _reserved  : 1;
	uint32_t baud;
	uint8_t  vddio2_25mV;

	//--- BASIC SETTINGS -------
	bool always_setup_mode;

	//--- VIDEO SETTINGS -------
	// Most of these range from 0-255 DAC_MIN to DAC_MAX
	uint8_t  hvset;
	uint8_t  filset;
	uint8_t  hscale_center;
	uint8_t  hscale_max;
	uint8_t  vscale_center;
	uint8_t  vscale_max;
	uint8_t  cutoff;
	uint8_t  inten_hi;
	uint8_t  inten_lo;
	uint8_t  cathgain_res_center;
	uint8_t  cathgain_res_min; // minimum resistance value - max gain
	uint8_t  hpos_center;
	uint8_t  hpos_max;
	uint8_t  vpos_center;
	uint8_t  vpos_max;
} settings_block_t;

static_assert(sizeof(settings_block_t) <= EEPROM_SIZE,
	"Settings block must fit in EEPROM");

enum class settings_status {
	/// Settings loaded successfully
	LOADED,
	/// Settings loaded successfully from an older, smaller settings
	/// struct.  New fields have been initialized to defaults.
	EXTENDED,
	/// Settings were corrupted or uninitialized, and have been replaced
	/// with defaults.
	CORRUPTED,
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------

extern settings_block_t settings_block;

// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Initialize settings. settings_block is not valid until after this is called.
///
/// Note that any changes made to settings in the process of loading are not
/// committed, you must call settings_commit() to write them.
settings_status settings_init();

/// Commit settings. The settings struct must not be accessed while this
/// function is running. It will fill in the checksum and then write to EEPROM.
void settings_commit();

#endif // !defined(SETTINGS_HPP)
