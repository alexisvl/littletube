// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "modes.hpp"

// Supporting modules
#include "hw.hpp"
#include "parbus.hpp"
#include "ledflash.hpp"
#include <avr/io.h>
#include <avr/wdt.h>
#include <util/delay.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <alloca.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

/// Error mode handler
__attribute__((noreturn))
static void _error_mode();

/// Shutdown mode handler. Returns once shutdown exit is requested.
static void _shutdown_mode();

/// Safely start and select the external crystal as clock source. This first
/// starts an internal oscillator as a timing reference for timeout, which
/// disables the RTOS tick, so one of the _tick_select_* functions must be
/// called next.
///
/// If the crystal does not start, a crash is triggered.
static void _safely_start_crystal();

/// Select the internal RTC as the source of RTOS ticks.
static void _tick_select_rtc();

/// Select vsync as the source of RTOS ticks. These should come at 60 Hz; if
/// more than 2/60 seconds elapses without a tick occurring, this function
/// fails and returns error_t::TIMEOUT.
///
/// If ticks start successfully but then stop, the watchdog timer will trip.
static avl::error_t _tick_select_vsync();

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

static volatile ltm::mode _mode = ltm::mode::ERROR;
static char const * _error_str = nullptr;
static uint8_t _crash_code = 0;
static uint16_t _crash_addr = 0;

// --- PUBLIC FUNCTIONS --------------------------------------------------------

ltm::mode ltm::get_mode()
{
	return _mode;
}

avl::error_t ltm::switch_mode(ltm::mode m)
{
	avl::error_t e = avl::error_t::OK;

	switch (m)
	{
	case ltm::mode::SHUTDOWN:
		cli();
		hw_pins_safe();
		_PROTECTED_WRITE(RSTCTRL.SWRR, RSTCTRL_SWRST_bm);
		for (;;);
		break;

	case ltm::mode::SETUP:
	case ltm::mode::READY:
		_safely_start_crystal();
		// TODO
		//parbus_vp_to_constant_vsync();
		_tick_select_rtc();
		_mode = m;
		break;

	case ltm::mode::RUNNING:
		_safely_start_crystal();
		parbus_vp_to_run();
		e = _tick_select_vsync();
		if (e != avl::error_t::OK)
			return e;
		_mode = m;
		break;

	case ltm::mode::ERROR:
		cli();
		hw_pins_make_safe();
		_error_mode();
		break;

	default:
		return avl::error_t::INVAL;
	}

	return avl::error_t::OK;
}

void ltm::crash(char const * e)
{
	cli();
	_error_str = e;
	_crash_code = 0;
	_crash_addr = (uint16_t)(uintptr_t) __builtin_return_address(0) * 2;
	ltm::switch_mode(ltm::mode::ERROR);
	__builtin_unreachable();
}

void ltm::crash(uint8_t code, char const * e)
{
	cli();
	if (!e)
	{
		char * s = (char *) alloca(64);
		snprintf(s, 63, FSTR("crash code 0x%X"), code);
		s[63] = 0;
		_error_str = s;
	}
	else
	{
		_error_str = e;
	}
	_crash_code = code;
	_crash_addr = (uint16_t)(uintptr_t) __builtin_return_address(0) * 2;
	ltm::switch_mode(ltm::mode::ERROR);
	__builtin_unreachable();
}

ltm::mode ltm::initial_mode()
{
	(void)_shutdown_mode;
	//_shutdown_mode();

	if (!*PIN_nSETUP_MODE)
		_mode = ltm::mode::SETUP;

	else
		_mode = ltm::mode::READY;
	return _mode;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _error_mode()
{
	// First, get onto a reliable oscillator.
	//
	// Check the VDDIO2 situation - and if MVIO.STATUS claims it's up,
	// actually measure it with the ADC. If it's too low to run the LEDs,
	// drive the console pins low so we don't fry the client, then
	// drive VDDIO2 to 5V.
	//
	// Then, loop. Every time around, if we have console, emit the error
	// string. Whether or not we do, blink out the blink code.

	_PROTECTED_WRITE(
		CLKCTRL.OSCHFCTRLA,
		CLKCTRL_FREQSEL_24M_gc
	);
	_PROTECTED_WRITE(
		CLKCTRL.MCLKCTRLA,
		CLKCTRL_CLKSEL_OSCHF_gc
	);
	_PROTECTED_WRITE(
		CLKCTRL.MCLKCTRLB,
		0
	);
	wdt_reset();
	while (CLKCTRL.MCLKSTATUS & CLKCTRL_SOSC_bm);
	_PROTECTED_WRITE(CLKCTRL.XOSC32KCTRLA, 0);
	_PROTECTED_WRITE(CLKCTRL.XOSCHFCTRLA, 0);

	bool need_5v = !comport_ready;

	if (!(MVIO.STATUS & MVIO_VDDIO2S_bm))
	{
		need_5v = true;
	}
	else if (!need_5v)
	{
		VREF.ADC0REF = VREF_REFSEL_2V048_gc;
		ADC0.CTRLA = 0;
		ADC0.INTCTRL = 0;
		ADC0.CTRLB = ADC_SAMPNUM_NONE_gc;
		ADC0.CTRLC = ADC_PRESC_DIV2_gc;
		ADC0.CTRLD = 0;
		ADC0.CTRLE = 0;
		ADC0.SAMPCTRL = 0;
		ADC0.CTRLA = ADC_RESSEL_10BIT_gc | ADC_ENABLE_bm;
		ADC0.MUXPOS = ADC_MUXPOS_VDDIO2DIV10_gc;
		ADC0.COMMAND = ADC_STCONV_bm;

		while (!(ADC0.INTFLAGS & ADC_RESRDY_bm))
			wdt_reset();

		uint16_t const res = ADC0.RES;

		if (res < (uint16_t)(2.5f * 0.1f / 1.024f * 1024.f + 0.5f))
			need_5v = true;

		ADC0.CTRLA = 0;
	}

	if (need_5v)
	{
		// Disconnect the usart
		USART_COMPORT << avl::usart_ctrlb::none;
		PIN_SER_OUT << avl::port_dir::output_0;
		PIN_SER_IN << avl::port_dir::input;
		PIN_SER_CTS << avl::port_dir::output_0;
		PIN_SER_RTS << avl::port_dir::input;
		PIN_nERR_nSHDN_PASSTHROUGH << avl::port_dir::input;
		PIN_nERR_nSHDN << avl::port_dir::output_0;

		// TODO REFACTOR: make per-opamp drivers so this can be in hw
		OPAMP.OP2CTRLA = 0;
		PIN_VDDIO2_OUT << avl::port_dir::output_1; // drive 5V directly
	}
	else
	{
		// no interrupts
		USART_COMPORT << avl::usart_ctrla::none;
	}

	// If no code is specified, we flash the green LED instead and emit
	// the crash location. Red should be solid.
	if (!_crash_code)
		PIN_LED_RED << avl::port_dir::output_1;

	for (;;)
	{
		wdt_reset();

		if (comport_ready && _error_str)
		{
			wdt_reset();
			USART_COMPORT.send_str(FSTR("*ERR "));
			USART_COMPORT.send_str(_error_str);

			if (_crash_addr)
			{
				char s[64] = {0};
				snprintf(s, sizeof(s) - 1,
					FSTR(", crash addr: 0x%04X"),
					_crash_addr);
				USART_COMPORT.send_str(s);
			}
			USART_COMPORT.send_str(FSTR("\r\n"));
		}

		// flash code
		if (_crash_code)
			ledflash(_crash_code, ledflash_led::RED);
		else
			ledflash(_crash_addr, ledflash_led::GREEN);

		uint16_t delay = 1000;
		while (delay > 10)
		{
			wdt_reset();
			_delay_ms(10);
			delay -= 10;
		}
	}
}

static void _shutdown_mode()
{
	// TODO: disable pullup when not reading it
	while (!*PIN_nERR_nSHDN)
	{
		_PROTECTED_WRITE(CLKCTRL.MCLKCTRLA,
			CLKCTRL_CLKSEL_OSC32K_gc
		);

		_PROTECTED_WRITE(CLKCTRL.XOSC32KCTRLA, 0);
		_PROTECTED_WRITE(CLKCTRL.XOSCHFCTRLA, 0);

		for (int i = 0; i < 10; i += 1)
		{
			wdt_reset();
			__builtin_avr_delay_cycles(32768/100);
		}
	}
}

static void _safely_start_crystal()
{
	// Disable RTC interrupt; select OSC1K and set RTC PER to a timeout.
	RTC.INTCTRL = 0;
	RTC.CLKSEL = RTC_CLKSEL_OSC1K_gc;	// 1024 Hz
	while (RTC.STATUS & RTC_PERBUSY_bm);
	RTC.PER = 512;
	while (RTC.STATUS & RTC_CTRLABUSY_bm);
	RTC.CTRLA = RTC_PRESCALER_DIV1_gc;
	while (RTC.STATUS & RTC_CTRLABUSY_bm);
	RTC.CTRLA = RTC_PRESCALER_DIV1_gc | RTC_RTCEN_bm;
	RTC.INTFLAGS = RTC_OVF_bm;

	// Start the crystal
	_PROTECTED_WRITE(CLKCTRL.XOSCHFCTRLA,
		CLKCTRL_RUNSTDBY_bm | CLKCTRL_CSUTHF_1K_gc |
		CLKCTRL_FRQRANGE_24M_gc | CLKCTRL_ENABLE_bm);

	while (!(CLKCTRL.MCLKSTATUS & CLKCTRL_EXTS_bm))
	{
		wdt_reset();
		if (RTC.INTFLAGS & RTC_OVF_bm)
			ltm::crash(ltm::crash_code::CRASH_XTAL_FAIL);
	}

	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLA,
		CLKCTRL_CLKSEL_EXTCLK_gc | CLKCTRL_CLKOUT_bm
	);
}

static void _tick_select_rtc()
{
	RTC.CLKSEL = RTC_CLKSEL_OSC1K_gc;		// RTC_CLK = 1024 Hz
	while (RTC.STATUS & RTC_PERBUSY_bm);
	RTC.PER = 17; // 60.2 Hz
	RTC.INTCTRL = RTC_OVF_bm;
	while (RTC.STATUS & RTC_CTRLABUSY_bm);
	RTC.CTRLA = RTC_PRESCALER_DIV1_gc;
	while (RTC.STATUS & RTC_CTRLABUSY_bm);
	RTC.CTRLA = RTC_PRESCALER_DIV1_gc | RTC_RTCEN_bm;
	RTC.INTFLAGS = RTC_OVF_bm;
}

static avl::error_t _tick_select_vsync()
{
	uint8_t sreg = SREG;
	cli();

	// Disable RTC interrupt; select OSC1K and set RTC PER to a timeout.
	RTC.INTCTRL = 0;
	RTC.CLKSEL = RTC_CLKSEL_OSC1K_gc;	// 1024 Hz
	while (RTC.STATUS & RTC_PERBUSY_bm);
	RTC.PER = 43;
	while (RTC.STATUS & RTC_CTRLABUSY_bm);
	RTC.CTRLA = RTC_PRESCALER_DIV1_gc;
	while (RTC.STATUS & RTC_CTRLABUSY_bm);
	RTC.CTRLA = RTC_PRESCALER_DIV1_gc | RTC_RTCEN_bm;
	RTC.INTFLAGS = RTC_OVF_bm;

	// Wait for vsync to go low...
	while (*PIN_VSYNC)
	{
		wdt_reset();
		if (RTC.INTFLAGS & RTC_OVF_bm)
			goto failed;
	}

	/// ...and for it to go high again
	while (!*PIN_VSYNC)
	{
		wdt_reset();
		if (RTC.INTFLAGS & RTC_OVF_bm)
			goto failed;
	}

	while (RTC.STATUS & RTC_CTRLABUSY_bm);
	RTC.CTRLA = 0;

	/// Now the interrupt can be enabled
	PIN_VSYNC.get_and_clear_intflag();
	PIN_VSYNC << avl::port_pinctrl::isc::rising;
	SREG = sreg;

	return avl::error_t::OK;

failed:
	_tick_select_rtc();
	SREG = sreg;
	return avl::error_t::TIMEDOUT;
}
