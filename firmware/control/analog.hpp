// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef ANALOG_HPP
#define ANALOG_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avl_error.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// Littletube Analog
namespace lta {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Analog system error codes
enum class analog_error: uint8_t {
	NONE,

	/// Low voltage analog appears to malfunction
	LV_ANALOG,

	/// 50V rail out of regulation (possible short?)
	V50_UNREG,

	/// 12V too low to start
	V12_LOW,

	/// 12V impedance too high, droops on startup
	V12_IMPED,

	/// VA2 out of regulation, too low (possible short?)
	VA2_UNREG_LOW,

	/// VA2 out of regulation, too high (damaged converter?)
	VA2_UNREG_HIGH,

	/// Vfil does not come up, possible short to ground or damaged
	/// regulator
	VFIL_STUCK_LOW,

	/// Ifil too high, possible shorted filament
	IFIL_HIGH,

	/// Ifil too low, possible open filament
	IFIL_LOW,
};

/// Analog system status
struct status {
	enum analog_error error;

	/// Eventually, this should be replaced with computed values in a more
	/// human-accessible form, but for now I'm just dumping.
	uint16_t results[6];
};

enum class init_status: uint8_t {
	NOT_INITIALIZED,
	INITIALIZED,
	INIT_FAILED,
	STARTING,
	RUNNING,
	SHUT_DOWN,
};

// --- PUBLIC CONSTANTS --------------------------------------------------------

/// High voltage key. This is passed into functions that can start the high
/// voltage supply (and checked several times throughout the function) to guard
/// against accidental jumps into them.
///
/// When using this, make sure to store it in volatile variables, as otherwise
/// it is trivial to optimize away.
#define HV_KEY 0xBAD2BDED

// --- PUBLIC VARIABLES --------------------------------------------------------

extern volatile struct status analog_status;

// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Start the analog monitoring task. This task drives the ADC, collecting and
/// processing readings. It also initializes the analog subsystem, including
/// references.
avl::error_t start_adc_task();

/// Check if the analog subsystem has been initialized. Until it has, no other
/// functions in this module (other than start_adc_task() and startup()) may be
/// called.
init_status initialized();

/// Shut down all analog.
///
/// When shut down, all systems that are controlled by this module will be
/// powered down.
///
/// Blocking.
void shutdown();

/// Start analog systems: voltage references, ADC, DACs.
///
/// Blocking.
void startup();

/// Bring up all analog. Powers up DACs and references. Any systems beyond
/// those that were shut down by shutdown() are NOT brought back up.
///
/// Needs to be called at least once, after start_adc_task(). There is no need
/// to wait on any statuses before making the first call.

/// Enable bypass of errors that would be triggered by a CRT not connected
/// (like open filament, Va2 slightly but not dangerously high).
///
/// Errors will still report in the status struct, but operation will not be
/// halted.
void bypass_open_crt(bool en);

/// Perform self-calibration of Va2. DANGER, this requires running the high
/// voltage supply. It should be run with the CRT detached. See the
/// documentation - this is triggered by a human user from setup mode.
///
/// @param key - HV_KEY
///
/// Blocking
avl::error_t va2_selfcal(uint32_t volatile key);

/// Set the 2nd anode voltage. If the self-calibration has not been performed,
/// this uses the default calibration settings.
///
/// The ADC task must be running; this will enable monitoring of output
/// voltages.
///
/// Blocking.
///
/// @param v - HV output in volts, at least 700 to 1000, or 0 to shut down.
/// @param key - HV_KEY, not required for shutdown.
avl::error_t set_va2(uint16_t v, uint32_t volatile key);

/// Set the filament voltage. This value is not subject to calibration, but
/// will be monitored.
///
/// The ADC task must be running; this will enable monitoring of voltage and
/// current.
///
/// Blocking.
///
/// @param v - Filament voltage in hundreds of mV, at least 42 to 75.
avl::error_t set_vfil_100mV(uint8_t v);

/// Set the raw horizontal scale. Calibration should be performed upstream
/// by the graphics manager (likely just as an offset).
///
/// Blocking.
///
/// @param scale - Scale from 0 to 255.
avl::error_t set_hscale(uint8_t scale);

/// Set the raw vertical scale. Calibration should be performed upstream
/// by the graphics manager (likely just as an offset).
///
/// Blocking.
///
/// @param scale - Scale from 0 to 255.
avl::error_t set_vscale(uint8_t scale);

/// Set the horizontal position. Calibration should be performed upstream
/// by the graphics manager (likely just as an offset).
///
/// Blocks if VSYNC OUT queue is full.
///
/// @param pos - Position from 0 to 255
avl::error_t set_hpos(uint8_t pos);

/// Set the vertical position. Calibration should be performed upstream
/// by the graphics manager (likely just as an offset).
///
/// Blocks if VSYNC OUT queue is full.
///
/// @param pos - Position from 0 to 255
avl::error_t set_vpos(uint8_t pos);

/// Set the black level. Nominally 300mV.
///
/// Blocks if VSYNC OUT queue is full.
///
/// @param v - Voltage in tens of mV
avl::error_t set_black_level(uint16_t v);

/// Set the grid cutoff voltage. This voltage may (and should) be set before
/// bringing up Va2, but the analog supply itself will come up simultaneously.
///
/// Blocking.
///
/// @param v - Voltage in negative volts, 0 to 100
avl::error_t set_cutoff(uint8_t v);

/// Set the voltage of the highest intensity, before the video buffer and
/// output termination. For compliance with a 1Vpp video signal, this should be
/// 2V nominally, decreasing to dim the image.
///
/// Blocking.
///
/// @param v - Voltage in tens of mV, nominally 200.
avl::error_t set_inten_hi(uint8_t v);

/// Set the voltage of the lowest intensity, before the video buffer and output
/// termination. With a black level of 0.3V, a recommended nominal level is
/// 860 mV.
///
/// Blocking.
///
/// @param v - Voltage in tens of mV, nominally 86.
avl::error_t set_inten_lo(uint8_t v);

/// Set the IO bank voltage.
///
/// Blocking.
///
/// @param v - Voltage in 25s of mV, from 72 to 200.
avl::error_t set_vddio2(uint16_t v);

/// Set the CRT video gain, from 6.6 to 330.
///
/// Blocking.
///
/// @param gain - Gain in tenths, 66 to 3300.
avl::error_t set_video_gain(uint16_t gain);

} // lta
#endif // !defined(ANALOG_HPP)
