// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef HW_HPP
#define HW_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avr/io.h>
#include <avl_avrdx.hpp>
#include <avl_comport_avrdx.hpp>
#include <avl_avrdx_twi.hpp>
#include "dacx3608.hpp"
#include "mcp401x_i2c.hpp"
#include "parbus.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

extern avl::pin<avl::PORTA_ADDR, 5> PIN_nSTB;
extern avl::pin<avl::PORTA_ADDR, 6> PIN_nACK;
extern avl::pin<avl::PORTB_ADDR, 0> PIN_50VENABLE;
extern avl::pin<avl::PORTB_ADDR, 1> PIN_HVENABLE;
extern avl::pin<avl::PORTB_ADDR, 2> PIN_SDA;
extern avl::pin<avl::PORTB_ADDR, 3> PIN_SCL;
extern avl::pin<avl::PORTB_ADDR, 4> PIN_VID_nRESET;
extern avl::pin<avl::PORTB_ADDR, 5> PIN_LOAD;
extern avl::pin<avl::PORTB_ADDR, 6> PIN_FIL_nREG_EN;
extern avl::pin<avl::PORTB_ADDR, 7> PIN_nERR_nSHDN;
extern avl::pin<avl::PORTC_ADDR, 0> PIN_SER_OUT;
extern avl::pin<avl::PORTC_ADDR, 1> PIN_SER_IN;
extern avl::pin<avl::PORTC_ADDR, 2> PIN_SER_CTS;
extern avl::pin<avl::PORTC_ADDR, 3> PIN_SER_RTS;
extern avl::pin<avl::PORTC_ADDR, 4> PIN_LED_RED;
extern avl::pin<avl::PORTC_ADDR, 5> PIN_LED_GRN;
extern avl::pin<avl::PORTC_ADDR, 6> PIN_nERR_nSHDN_PASSTHROUGH;
extern avl::pin<avl::PORTC_ADDR, 7> PIN_PC7;
extern avl::pin<avl::PORTD_ADDR, 0> PIN_FILVSENSE;
extern avl::pin<avl::PORTD_ADDR, 1> PIN_FILISENSEn;
extern avl::pin<avl::PORTD_ADDR, 2> PIN_VSENSE3;
extern avl::pin<avl::PORTD_ADDR, 3> PIN_VSENSE1;
extern avl::pin<avl::PORTD_ADDR, 4> PIN_FILISENSEp;
extern avl::pin<avl::PORTD_ADDR, 5> PIN_GNDSENSE;
extern avl::pin<avl::PORTD_ADDR, 6> PIN_REFOUT;
extern avl::pin<avl::PORTD_ADDR, 7> PIN_REFIN;
extern avl::pin<avl::PORTE_ADDR, 0> PIN_PE0;
extern avl::pin<avl::PORTE_ADDR, 1> PIN_VDDIO2_SET;
extern avl::pin<avl::PORTE_ADDR, 2> PIN_VDDIO2_OUT;
extern avl::pin<avl::PORTE_ADDR, 3> PIN_PE3;
extern avl::pin<avl::PORTE_ADDR, 4> PIN_PE4;
extern avl::pin<avl::PORTE_ADDR, 5> PIN_PE5;
extern avl::pin<avl::PORTE_ADDR, 6> PIN_nLDAC;
extern avl::pin<avl::PORTE_ADDR, 7> PIN_PE7;
extern avl::pin<avl::PORTF_ADDR, 0> PIN_50VSENSE;
extern avl::pin<avl::PORTF_ADDR, 1> PIN_12VSENSE;
extern avl::pin<avl::PORTF_ADDR, 2> PIN_VSYNC;
extern avl::pin<avl::PORTF_ADDR, 3> PIN_FIL_LOAD_EN;
extern avl::pin<avl::PORTF_ADDR, 4> PIN_nSETUP_MODE;
extern avl::pin<avl::PORTF_ADDR, 5> PIN_nCATHLOCKOUT;

extern avl::pin_group<avl::PORTG_ADDR, 0xFF> PINGRP_ADn;
// This is not really 0xFF - these pins are 0x7C. However, all other pins on
// this port are taken over by peripherals, and using 0xFF makes writes faster.
extern avl::pin_group<avl::PORTA_ADDR, 0xFF> PINGRP_ACn_nSTB_nACK;
#define PIN_AC0_bp 2
#define PIN_AC1_bp 3
#define PIN_AC2_bp 4
#define PIN_nSTB_bp 5
#define PIN_nACK_bp 6

extern avl::usart<avl::USART1_ADDR> USART_COMPORT;
#define ISR_USART_COMPORT_DRE UCOS_MANAGED_ISR(USART1_DRE_vect)
#define ISR_USART_COMPORT_RXC UCOS_MANAGED_ISR(USART1_RXC_vect)

extern avl::comport_avrdx
<
	typeof(USART_COMPORT),
	typeof(PIN_SER_RTS),
	typeof(PIN_SER_CTS),
	80, 160
> comport;
extern bool comport_ready;

extern avl::avrdx_twi I2C;
extern dacx3608 EXT_DAC;
extern mcp401x_i2c VIDEO_GAIN_POT;

extern parbus_transmitter parbus;

// EXT_DAC channels
#define EXT_DAC_CH_HVSET	0
#define EXT_DAC_CH_FILSET	1
#define EXT_DAC_CH_HSCALE	2
#define EXT_DAC_CH_VSCALE	3
#define EXT_DAC_CH_CUTOFF	4
#define EXT_DAC_CH_INTEN_HI	5
#define EXT_DAC_CH_INTEN_LO	6
#define EXT_DAC_CH_VDDIO2_SET	7

// ADC inputs
#define AIN_FILVSENSE	0
#define AIN_FILISENSEn	1
#define AIN_VSENSE3	2
#define AIN_VSENSE1	3
#define AIN_FILISENSEp	4
#define AIN_GNDSENSE	5	// ground near CRT output
#define AIN_VREF	7
#define AIN_50VSENSE	16
#define AIN_12VSENSE	17

#define CLAMP(x, low, high) ((x) < (low) ? (low) : (x) > (high) ? (high) : (x))
#define F_TO_U8(f) ((uint8_t)CLAMP(static_cast<int16_t>((f) + 0.5f), 0, 255))

//#define HVSET_TO_V1(dac) (281.728f - 0.365f*(dac))
#define V1_TO_HVSET(v) CLAMP(-12.0247f*(v)+3294.5f, 0, 1023)
// TODO: confirm this one
//#define HVSET_TO_VA2(dac) (4.f * HVSET_TO_V1(dac))
//#define VA2_TO_HVSET(v) V1_TO_HVSET((v) / 4.f)
#define VA2_TO_HVSET(v) CLAMP(-3.710427f*(v)+3868.f, 0, 1023)

//#define FILSET_TO_V(dac) (7.5875f - 0.0134341f*(dac))
#define V_TO_FILSET(v) CLAMP((5.875f - (v)) * 298.3f + 511.f, 0, 1023)

// 61 -> -92.91V
// 207 -> -1.964V
#define CUTOFF_TO_V(dac) \
	CLAMP(((dac) - 61.0f) / (146.0f * 90.946f) - 92.91f, -100.f, 0.56f)
#define V_TO_CUTOFF(v) \
	CLAMP((((v) + 92.91f) / (90.946f * 146.0f)) + 61, 0, 255)

#define CATHGAIN_TO_AV(dac) (33e3f / ((dac) / 255.f * 5e3f))
#define AV_TO_CATHGAIN(av) ((33e3f / (av)) / 5e3f) * 255.f

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Initialize pins to a sane, safe default state.
void hw_pins_safe();

/// Just change the safety-critical pins to a safe state, not initializing
/// the others
void hw_pins_make_safe();

/// Enable backup power on VDDIO2. This drives VDDIO2 directly at 5V in case
/// the analog variable-voltage system is down. All IOs to the COM port will
/// be disconnected to avoid overvoltage.
///
/// This should be called with the RTOS disabled, or otherwise in a state where
/// the COM port will not be used.
void hw_backup_vddio2();

#endif // !defined(HW_HPP)
