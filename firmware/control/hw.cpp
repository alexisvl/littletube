// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "hw.hpp"

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------

pin<PORTA_ADDR, 5> PIN_nSTB;
pin<PORTA_ADDR, 6> PIN_nACK;
pin<PORTB_ADDR, 0> PIN_50VENABLE;
pin<PORTB_ADDR, 1> PIN_HVENABLE;
pin<PORTB_ADDR, 2> PIN_SDA;
pin<PORTB_ADDR, 3> PIN_SCL;
pin<PORTB_ADDR, 4> PIN_VID_nRESET;
pin<PORTB_ADDR, 5> PIN_LOAD;
pin<PORTB_ADDR, 6> PIN_FIL_nREG_EN;
pin<PORTB_ADDR, 7> PIN_nERR_nSHDN;
pin<PORTC_ADDR, 0> PIN_SER_OUT;
pin<PORTC_ADDR, 1> PIN_SER_IN;
pin<PORTC_ADDR, 2> PIN_SER_CTS;
pin<PORTC_ADDR, 3> PIN_SER_RTS;
pin<PORTC_ADDR, 4> PIN_LED_RED;
pin<PORTC_ADDR, 5> PIN_LED_GRN;
pin<PORTC_ADDR, 6> PIN_nERR_nSHDN_PASSTHROUGH;
pin<PORTC_ADDR, 7> PIN_PC7;
pin<PORTD_ADDR, 0> PIN_FILVSENSE;
pin<PORTD_ADDR, 1> PIN_FILISENSEn;
pin<PORTD_ADDR, 2> PIN_VSENSE3;
pin<PORTD_ADDR, 3> PIN_VSENSE1;
pin<PORTD_ADDR, 4> PIN_FILISENSEp;
pin<PORTD_ADDR, 5> PIN_GNDSENSE;
pin<PORTD_ADDR, 6> PIN_REFOUT;
pin<PORTD_ADDR, 7> PIN_REFIN;
pin<PORTE_ADDR, 0> PIN_PE0;
pin<PORTE_ADDR, 1> PIN_VDDIO2_SET;
pin<PORTE_ADDR, 2> PIN_VDDIO2_OUT;
pin<PORTE_ADDR, 3> PIN_PE3;
pin<PORTE_ADDR, 4> PIN_PE4;
pin<PORTE_ADDR, 5> PIN_PE5;
pin<PORTE_ADDR, 6> PIN_nLDAC;
pin<PORTE_ADDR, 7> PIN_PE7;
pin<PORTF_ADDR, 0> PIN_50VSENSE;
pin<PORTF_ADDR, 1> PIN_12VSENSE;
pin<PORTF_ADDR, 2> PIN_VSYNC;
pin<PORTF_ADDR, 3> PIN_FIL_LOAD_EN;
pin<PORTF_ADDR, 4> PIN_nSETUP_MODE;
pin<PORTF_ADDR, 5> PIN_nCATHLOCKOUT;

pin_group<PORTG_ADDR, 0xFF> PINGRP_ADn;
// This is not really 0xFF - these pins are 0x7C. However, all other pins on
// this port are taken over by peripherals, and using 0xFF makes writes faster.
pin_group<PORTA_ADDR, 0xFF> PINGRP_ACn_nSTB_nACK;

usart<USART1_ADDR> USART_COMPORT;
comport_avrdx
<
	typeof(USART_COMPORT),
	typeof(PIN_SER_RTS),
	typeof(PIN_SER_CTS),
	80, 160
> comport;
bool comport_ready = false;

avl::avrdx_twi I2C(&TWI1);

dacx3608 EXT_DAC(&I2C, dacx3608::ADDR_GND);
mcp401x_i2c VIDEO_GAIN_POT(&I2C);
parbus_transmitter parbus;

// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void hw_pins_safe()
{
	hw_pins_make_safe();
	PIN_LOAD << port_pinctrl::reset << port_dir::output_0;
	PIN_nERR_nSHDN
		<< port_pinctrl::reset + port_pinctrl::pullup::yes
		<< port_dir::input;
	PIN_SER_IN
		<< port_pinctrl::reset + port_pinctrl::pullup::yes
		<< port_dir::input;
	PIN_SER_CTS << port_pinctrl::reset << port_dir::output_1;
	PIN_LED_RED << port_dir::output_0;
	PIN_LED_GRN << port_dir::output_0;
	PIN_nERR_nSHDN_PASSTHROUGH
		<< port_pinctrl::isc::input_disable
		<< port_dir::input;
	PIN_PC7 << port_pinctrl::isc::input_disable;
	PIN_FILVSENSE << port_pinctrl::isc::input_disable;
	PIN_FILISENSEn << port_pinctrl::isc::input_disable;
	PIN_FILISENSEp << port_pinctrl::isc::input_disable;
	PIN_GNDSENSE << port_pinctrl::isc::input_disable;
	PIN_REFOUT << port_pinctrl::isc::input_disable;
	PIN_REFIN << port_pinctrl::isc::input_disable;
	PIN_PE0 << port_pinctrl::isc::input_disable;
	PIN_VDDIO2_SET << port_pinctrl::isc::input_disable;
	PIN_VDDIO2_OUT << port_pinctrl::isc::input_disable;
	PIN_PE3 << port_pinctrl::isc::input_disable;
	PIN_PE4 << port_pinctrl::isc::input_disable;
	PIN_PE5 << port_pinctrl::isc::input_disable;
	PIN_PE7 << port_pinctrl::isc::input_disable;
	PIN_VSYNC
		<< port_pinctrl::reset + port_pinctrl::pullup::yes
		<< port_dir::input;
	PIN_nSETUP_MODE << port_dir::input;
	PINGRP_ADn << port_dir::output_0;
	PINGRP_ACn_nSTB_nACK.port().OUTSET = (0x08 << PIN_AC0_bp);
	PINGRP_ACn_nSTB_nACK.port().DIRSET = (0x0F << PIN_AC0_bp);
	PIN_nACK << port_pinctrl::pullup::yes;
	PIN_VID_nRESET << port_dir::output_1;
}

void hw_pins_make_safe()
{
	PIN_50VENABLE << port_pinctrl::reset << port_dir::output_0;
	PIN_HVENABLE << port_pinctrl::reset << port_dir::output_0;
	PIN_FIL_nREG_EN << port_pinctrl::reset << port_dir::output_1;
	PIN_nERR_nSHDN_PASSTHROUGH
		<< port_pinctrl::isc::input_disable
		<< port_dir::input;
	PIN_50VSENSE << port_pinctrl::isc::input_disable;
	PIN_12VSENSE << port_pinctrl::isc::input_disable;
	PIN_VSENSE3 << port_pinctrl::isc::input_disable;
	PIN_VSENSE1 << port_pinctrl::isc::input_disable;
	PIN_nLDAC << port_dir::output_0;
	PIN_FIL_LOAD_EN << port_dir::output_0;
	PIN_nCATHLOCKOUT << port_pinctrl::reset << port_dir::output_0;
}

void hw_backup_vddio2()
{
	// Disable USART
	// USART pins off
	// OPAMP 2 OFF
	PIN_VDDIO2_OUT << port_dir::output_1;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

ISR_USART_COMPORT_DRE
{
	comport.dre();
}

ISR_USART_COMPORT_RXC
{
	comport.rxc();
}

UCOS_MANAGED_ISR(TWI1_TWIM_vect)
{
	I2C.twim_isr();
}
