// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "dacx3608.hpp"

// Supporting modules
#include <ucos_ii.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------

#define CMD_DEVICE_CONFIG  0x1
#define CMD_STATUS_TRIGGER 0x2
#define CMD_BRDCAST        0x3
#define CMD_DACn_DATA(n)   (0x8 + (n))

// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

dacx3608::dacx3608(i2c_i * i2c, uint8_t addr)
	: m_i2c(i2c)
	, m_addr(addr)
	, m_power(0)
{}

result_t<dacx3608::variant> dacx3608::identify()
{
	result_t<uint16_t> r = _read_reg(CMD_STATUS_TRIGGER);

	if (r.is_err())
		return r.err();

	uint8_t devid = (r.ok() >> 6) & 0x3F;

	if (devid == 0x0C)
		return dacx3608::variant::dac53608;
	else if (devid == 0x14)
		return dacx3608::variant::dac43608;
	else
		return dacx3608::variant::unknown;
}

error_t dacx3608::swrst()
{
	return _write_reg(CMD_STATUS_TRIGGER, 0x000A);
}

error_t dacx3608::set_power(uint8_t channels)
{
	error_t e = _write_reg(
		CMD_DEVICE_CONFIG,
		channels ? ~channels & 0x0FFu : 0x1FFu
	);

	if (e == error_t::OK)
		m_power = channels;

	return e;
}

error_t dacx3608::set_power(uint8_t channel, bool state)
{
	uint8_t new_power = m_power;

	if (state)
		new_power |= (1 << channel);
	else
		new_power &= ~(1 << channel);

	return set_power(new_power);
}

result_t<uint8_t> dacx3608::get_power()
{
	result_t<uint16_t> r = _read_reg(CMD_DEVICE_CONFIG);

	if (r.is_err())
		return r.err();
	else if (r.ok() & 0x100)
		m_power = 0;
	else
		m_power = ~(r.ok() & 0xFF);

	return (uint8_t) m_power;
}

error_t dacx3608::set_value(uint8_t channel, uint16_t value)
{
	if (channel > 7 || value > 1023)
		return error_t::INVAL;

	return _write_reg(
		CMD_DACn_DATA(channel),
		value << 2
	);
}

result_t<uint16_t> dacx3608::get_value(uint8_t channel)
{
	if (channel > 7)
		return error_t::INVAL;

	result_t<uint16_t> r = _read_reg(CMD_DACn_DATA(channel));

	if (r.is_err())
		return r.err();
	else
		return (r.ok() >> 2) & 0x3FF;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

result_t<uint16_t> dacx3608::_read_reg(uint8_t index)
{
	uint8_t buffer[2] = {index, 0};
	i2c_packet packet = {
		.addr = m_addr,
		.n_out = 1,
		.n_in = 2,
		.timeout = OS_TICKS_PER_SEC,
		.buffer = buffer,
	};

	error_t e = m_i2c->txrx(&packet);

	if (e != error_t::OK)
		return e;

	return (((uint16_t) buffer[0]) << 8) | (uint16_t) buffer[1];
}

error_t dacx3608::_write_reg(uint8_t index, uint16_t value)
{
	uint8_t buffer[3] = {
		index,
		(uint8_t)(value >> 8),
		(uint8_t)(value & 0xFF)
	};
	i2c_packet packet = {
		.addr = m_addr,
		.n_out = 3,
		.n_in = 0,
		.timeout = OS_TICKS_PER_SEC,
		.buffer = buffer,
	};
	return m_i2c->txrx(&packet);
}
