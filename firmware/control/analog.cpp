// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "analog.hpp"

// Supporting modules
#include <ucos_ii.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include "dacx3608.hpp"
#include "mcp401x_i2c.hpp"
#include "hw.hpp"
#include "modes.hpp"
#include "settings.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

using namespace lta;
using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------

/// ADC sampling configuration. This struct specifies one measurement to be
/// made by the ADC ISR.
///
/// All measurements are stored as 16-bit results.
typedef struct adc_samp_config {
	/// MUXPOS setting (positive input mux)
	uint8_t muxpos;

	/// MUXNEG setting (negative input mux), or the reserved value 0xFF
	/// to use single-ended mode.
	uint8_t muxneg;

	/// SAMPNUM setting, to accumulate several samples. This is one of
	/// the ADC_SAMPNUM_x_gc flags, not the actual number.
	uint8_t sampnum;

	/// Length of sampling time (real sample time is 4us + SAMPLEN*2us)
	/// To sample signal sources with high impedances, ensure this is set
	/// (assume 1us minimum per 5k ohms - note the datasheet does not spec
	/// the sample capacitance or anything, so we have to make rough
	/// approximations based on default timing and the max impedance they
	/// recommend for it.
	uint8_t samplen;
} adc_samp_config_t;

/// ADC result. Use u16 if muxneg is 0xFF, else d16.
typedef union {
	uint16_t u16;
	int16_t d16;
} adc_result_t;

// --- PRIVATE CONSTANTS -------------------------------------------------------

#define FLAG_RESULTS_READY	0x01
#define FLAG_STARTUP		0x40
#define FLAG_SHUTDOWN		0x80

/// Array of ADC configuration entries. The actual ADC readings are then stored
/// in _adc_results[n] for each _adc_config[n].
static const PROGMEM struct adc_samp_config _adc_config[] = {
	{	.muxpos = AIN_FILVSENSE,
		.muxneg = AIN_GNDSENSE,
		.sampnum = ADC_SAMPNUM_NONE_gc,
		.samplen = 0, // 6.9kOhm
	},
	{	.muxpos = AIN_FILISENSEp,
		.muxneg = AIN_FILISENSEn,
		.sampnum = ADC_SAMPNUM_ACC16_gc, // 16-bit signed
		.samplen = 0, // 4.4kOhm
	},
	{	.muxpos = AIN_VSENSE3,
		.muxneg = AIN_GNDSENSE,
		.sampnum = ADC_SAMPNUM_NONE_gc,
		.samplen = 0, // 10kOhm
	},
	{	.muxpos = AIN_VSENSE1,
		.muxneg = AIN_GNDSENSE,
		.sampnum = ADC_SAMPNUM_NONE_gc,
		.samplen = 0, // 10kOhm
	},
	{	.muxpos = AIN_50VSENSE,
		.muxneg = 0xFF,
		.sampnum = ADC_SAMPNUM_NONE_gc,
		.samplen = 0, // 11.7kOhm
	},
	{	.muxpos = AIN_12VSENSE,
		.muxneg = 0xFF,
		.sampnum = ADC_SAMPNUM_NONE_gc,
		.samplen = 8, // 91kOhm
	},
};

#define ADC_NUM_CONFIGS (sizeof(_adc_config)/sizeof(_adc_config[0]))

// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

/// ADC monitoring task.
static void _adc_task(void * pd);

/// Start sampling ADC channel n
static void _start_sampling(uint8_t n);

/// Timer callback
static void _adc_timer_callback(void * ptmr, void * arg);

// --- PUBLIC VARIABLES --------------------------------------------------------

volatile struct status lta::analog_status;

// --- PRIVATE VARIABLES -------------------------------------------------------

static volatile init_status _initialized = init_status::NOT_INITIALIZED;
static uint8_t _adc_task_stack[256];
static OS_FLAG_GRP *_adc_task_flags;
static OS_TMR *_adc_timer;
static adc_result_t _adc_results[ADC_NUM_CONFIGS];
static uint8_t _adc_n_config = 0;

// --- PUBLIC FUNCTIONS --------------------------------------------------------

avl::error_t lta::start_adc_task()
{
	uint8_t oserr = 0;

	analog_status.error = analog_error::NONE;

	_adc_task_flags = OSFlagCreate(0, &oserr);

	if (oserr)
		return error_from_ucos(oserr);

	oserr = OSTaskCreateExt(
		_adc_task,
		NULL, // pdata
		&_adc_task_stack[sizeof(_adc_task_stack) - 1], // ptos
		ADC_TASK_PRIO, // prio
		ADC_TASK_PRIO, // id
		&_adc_task_stack[0], // pbos
		sizeof(_adc_task_stack), // stk_size
		NULL, // pext
		OS_TASK_OPT_STK_CHK // opt
	);

	if (oserr)
		return error_from_ucos(oserr);

	_adc_timer = OSTmrCreate(
		0,
		//1, // 100ms
		10, // 1000ms
		OS_TMR_OPT_PERIODIC,
		_adc_timer_callback,
		nullptr,
		(INT8U*) FSTR("adctmr"),
		&oserr
	);

	if (oserr)
		return error_from_ucos(oserr);

	_initialized = init_status::SHUT_DOWN;
	return error_t::OK;
}

init_status lta::initialized()
{
	return _initialized;
}

void lta::shutdown()
{
	uint8_t oserr = 0;

	if (_initialized == init_status::NOT_INITIALIZED)
		ltm::crash(ltm::crash_code::CRASH_LTA_NOT_INIT);

	OSFlagPost(_adc_task_flags, FLAG_SHUTDOWN, OS_FLAG_SET, &oserr);
	(void) oserr;

	OSFlagPend(_adc_task_flags, FLAG_SHUTDOWN, OS_FLAG_WAIT_CLR_ALL,
		UINT16_MAX, &oserr);
	(void) oserr;
}

void lta::startup()
{
	uint8_t oserr = 0;

	if (_initialized == init_status::NOT_INITIALIZED)
		ltm::crash(ltm::crash_code::CRASH_LTA_NOT_INIT);

	_initialized = init_status::STARTING;
	comport.printf(FSTR("*DBG requesting initialization\r\n"));

	OSFlagPost(_adc_task_flags, FLAG_STARTUP, OS_FLAG_SET, &oserr);
	(void) oserr;

	comport.printf(FSTR("*DBG blocking until up\r\n"));
	OSFlagPend(_adc_task_flags, FLAG_STARTUP, OS_FLAG_WAIT_CLR_ALL,
		UINT16_MAX, &oserr);
	(void) oserr;
}

void lta::bypass_open_crt(bool en);

avl::error_t lta::va2_selfcal(uint32_t volatile key);

avl::error_t lta::set_va2(uint16_t v, uint32_t volatile key)
{
	if (v == 0)
	{
		PIN_HVENABLE << port_dir::output_0;
		EXT_DAC.set_power(EXT_DAC_CH_HVSET, false);
		return error_t::OK;
	}

	if (key != HV_KEY)
	{
		return error_t::PERM;
	}

	if (v < 700 || v > 1000)
	{
		return error_t::INVAL;
	}

	EXT_DAC.set_value(EXT_DAC_CH_HVSET, VA2_TO_HVSET(v));
	EXT_DAC.set_power(EXT_DAC_CH_HVSET, true);

	if (key != HV_KEY)
	{
		PIN_HVENABLE << port_dir::output_0;
		EXT_DAC.set_power(EXT_DAC_CH_HVSET, false);
		return error_t::PERM;
	}

	PIN_HVENABLE << port_dir::output_1;

	if (key != HV_KEY)
	{
		PIN_HVENABLE << port_dir::output_0;
		EXT_DAC.set_power(EXT_DAC_CH_HVSET, false);
		return error_t::PERM;
	}

	return error_t::OK;
}

avl::error_t lta::set_vfil_100mV(uint8_t v)
{
	if (v == 0)
	{
		PIN_FIL_nREG_EN << port_dir::output_1;
		EXT_DAC.set_power(EXT_DAC_CH_FILSET, false);
		return error_t::OK;
	}

	if (v < 42 || v > 75)
	{
		return error_t::INVAL;
	}

	EXT_DAC.set_value(EXT_DAC_CH_FILSET, V_TO_FILSET(v / 10.f));
	EXT_DAC.set_power(EXT_DAC_CH_FILSET, true);

	PIN_FIL_nREG_EN << port_dir::output_0;

	return error_t::OK;
}

avl::error_t lta::set_hscale(uint8_t scale);

avl::error_t lta::set_vscale(uint8_t scale);

avl::error_t lta::set_hpos(uint8_t pos);

avl::error_t lta::set_vpos(uint8_t pos);

avl::error_t lta::set_black_level(uint16_t v);

avl::error_t lta::set_cutoff(uint8_t v);

avl::error_t lta::set_inten_hi(uint8_t v)
{
	EXT_DAC.set_power(EXT_DAC_CH_INTEN_HI, true);
	return EXT_DAC.set_value(EXT_DAC_CH_INTEN_HI,
		F_TO_U8((v * 0.01f) * 102));
}

avl::error_t lta::set_inten_lo(uint8_t v)
{
	EXT_DAC.set_power(EXT_DAC_CH_INTEN_LO, true);
	return EXT_DAC.set_value(EXT_DAC_CH_INTEN_LO,
		F_TO_U8((v * 0.01f) * 102));
}

avl::error_t lta::set_vddio2(uint16_t v)
{
	// TODO: nice 8-to-10bit translation function
	return EXT_DAC.set_value(EXT_DAC_CH_VDDIO2_SET,
		F_TO_U8((v * 0.025f) * (255/5)) << 2);
}

avl::error_t lta::set_video_gain(uint16_t gain)
{
	uint8_t cathgain = F_TO_U8(AV_TO_CATHGAIN(gain * 0.1f));
	return VIDEO_GAIN_POT.set(cathgain / 2);
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _adc_task(void * pd)
{
	(void) pd;
	for (;;) {
		uint8_t oserr = 0;
		OS_FLAGS flags = OSFlagPend(_adc_task_flags, -1,
			OS_FLAG_WAIT_SET_ANY, UINT16_MAX, &oserr);

		if (oserr == OS_ERR_TIMEOUT)
			continue;

		if ((flags & FLAG_SHUTDOWN) && (flags & FLAG_STARTUP))
		{
			// If startup and shutdown flag simultaneously, do
			// nothing. This is probably not ideal, but I don't
			// want to use a queue here.
			flags &= ~(FLAG_SHUTDOWN | FLAG_STARTUP);
			OSFlagPost(_adc_task_flags,
				FLAG_STARTUP | FLAG_SHUTDOWN,
				OS_FLAG_CLR, &oserr);
		}

		if (flags & FLAG_STARTUP)
		{
			// The AVR-DX voltage reference is crappy, but our +5V
			// is good. Use the DAC to output a 2.5V reference.

			// Set DAC vref to VDDA
			comport.printf(FSTR("*DBG setting vref\r\n"));
			VREF.DAC0REF = VREF_REFSEL_VDD_gc;
			// Set DAC to midpoint
			DAC0.DATA = 0x8000;
			DAC0.CTRLA = DAC_OUTEN_bm | DAC_ENABLE_bm
				| DAC_RUNSTDBY_bm;

			OSTimeDly(1);

			// Sanity check:
			//     Set ADC vref to 4.096V
			//     Read AIN7 (VREFA)
			//     Make sure it's 2.5V +/- 10%
			//     Move ADC vref to VREFA
			VREF.ADC0REF = VREF_REFSEL_4V096_gc;
			ADC0.CTRLA = ADC_LEFTADJ_bm | ADC_ENABLE_bm;
			ADC0.CTRLB = ADC_SAMPNUM_NONE_gc;
			ADC0.CTRLC = ADC_PRESC_DIV48_gc; // 2us clock
			ADC0.CTRLD = 0;
			ADC0.CTRLE = 0;
			ADC0.SAMPCTRL = 100;
			ADC0.MUXPOS = 7;
			ADC0.INTCTRL = 0;
			ADC0.COMMAND = ADC_STCONV_bm;

			while (!ADC0.INTFLAGS & ADC_RESRDY_bm) OSTimeDly(1);

			uint16_t res = ADC0.RES;
			if (res < (uint16_t)(2.5*0.9/4.096*0xFFC0)
				|| res > (uint16_t)(2.5*1.1/4.096*0xFFC0))
			{
				comport.printf(FSTR("*ERR Voltage reference "
					"out of range (%u)\r\n"), res);
				goto shutdown;
			}
			else
			{
				comport.printf(FSTR("*DBG vref check pass\r\n"));
			}

			// Set DAC channels to default values.
			EXT_DAC.set_value(EXT_DAC_CH_HVSET, dacx3608::MAX_VAL);
			EXT_DAC.set_value(EXT_DAC_CH_FILSET, dacx3608::MAX_VAL);
			EXT_DAC.set_value(EXT_DAC_CH_HSCALE, 4*settings_block.hscale_center);
			EXT_DAC.set_value(EXT_DAC_CH_VSCALE, 4*settings_block.vscale_center);
			EXT_DAC.set_value(EXT_DAC_CH_CUTOFF, 0);
			EXT_DAC.set_value(EXT_DAC_CH_INTEN_HI, settings_block.inten_hi);
			EXT_DAC.set_value(EXT_DAC_CH_INTEN_LO, settings_block.inten_lo);
			EXT_DAC.set_power((uint8_t) (1u << EXT_DAC_CH_VDDIO2_SET));
			lta::set_vddio2(settings_block.vddio2_25mV);
			comport.printf(FSTR("*DBG DAC initialized\r\n"));
			VIDEO_GAIN_POT.set(mcp401x_i2c::MAX_VAL);

			// Set up the opamp
			// TODO REFACTOR: this should be defined in hw.hpp, but
			// the structure of the opamp definition in io.h makes
			// this hard
			comport.printf(FSTR("*DBG setting up opamp\r\n"));
			OPAMP.TIMEBASE = (F_CPU - 1) / 1000000uL;
			OPAMP.PWRCTRL = OPAMP_IRSEL_bm;
			OPAMP.CTRLA = OPAMP_ENABLE_bm;
			OPAMP.OP2SETTLE = 200; // 200 us
			OPAMP.OP2INMUX = OPAMP_OP2INMUX_MUXPOS_INP_gc
				| OPAMP_OP2INMUX_MUXNEG_WIP_gc;
			OPAMP.OP2RESMUX = OPAMP_OP2RESMUX_MUXTOP_OUT_gc
				| OPAMP_OP2RESMUX_MUXBOT_GND_gc
				| OPAMP_OP2RESMUX_MUXWIP_WIP3_gc; // 1:1
			OPAMP.OP2CTRLA = OPAMP_RUNSTBY_bm
				| OPAMP_OP2CTRLA_OUTMODE_NORMAL_gc
				| OPAMP_ALWAYSON_bm;
			while (!(OPAMP.OP2STATUS & OPAMP_SETTLED_bm))
				OSTimeDly(1);
			comport.printf(FSTR("*DBG opamp initialized\r\n"));

			// Enable ADC ISR
			ADC0.INTFLAGS = ADC_RESRDY_bm;
			ADC0.INTCTRL = ADC_RESRDY_bm;
			_start_sampling(0);
			OSTmrStart(_adc_timer, &oserr);

			// De-post flag.
			_initialized = init_status::RUNNING;
			OSFlagPost(_adc_task_flags, FLAG_STARTUP,
				OS_FLAG_CLR, &oserr);
		}

		if (flags & FLAG_SHUTDOWN)
		{
shutdown:
			// Disable ADC ISR
			OSTmrStop(_adc_timer, OS_TMR_OPT_NONE, nullptr, &oserr);
			ADC0.INTCTRL = 0;

			_initialized = init_status::SHUT_DOWN;

			// Drive the CRT into full cutoff, then enable cathode
			// lockout/spot killer
			EXT_DAC.set_power(EXT_DAC_CH_CUTOFF, false);
			OSTimeDly(1);
			PIN_nCATHLOCKOUT = 0;
			OSTimeDly(2);

			// Shutting off these supplies is actually a bit
			// tricky, as the DAC control is inverted, and the DACs
			// pull down to ground when powered off. Instead,
			// drive the DAC _HIGH_, which sets the lowest possible
			// output voltage. Both regulator ICs can withstand
			// the applied voltage while powered down.
			EXT_DAC.set_value(EXT_DAC_CH_HVSET, dacx3608::MAX_VAL);
			PIN_HVENABLE = 0;

			// Keep the 50V rail, which powers the cathode driver,
			// up for as long as the CRT might have voltage on it
			// 5RC on VA2 rail, where R is bleeder res
			OSTimeDlyHMSM(0, 0, 0, 435);
			EXT_DAC.set_power(EXT_DAC_CH_HVSET, false);

			PIN_50VENABLE = 0;

			// Same issue with the regulator control as before
			EXT_DAC.set_value(EXT_DAC_CH_FILSET, dacx3608::MAX_VAL);
			PIN_FIL_nREG_EN = 1;
			// Let the filament bleed down the fil reg
			OSTimeDly(2);

			EXT_DAC.set_power(EXT_DAC_CH_FILSET, false);
			PIN_FIL_LOAD_EN = 0;

			// Power down internal analog.
			ADC0.CTRLA = 0;
			DAC0.CTRLA = 0;

			// De-post flag.
			OSFlagPost(_adc_task_flags, FLAG_STARTUP,
				OS_FLAG_CLR, &oserr);
		}

		if (flags & FLAG_RESULTS_READY)
		{
			for (size_t i = 0; i < 6; i += 1)
			{
				analog_status.results[i] = _adc_results[i].u16;
			}

			// De-post flag.
			OSFlagPost(_adc_task_flags, FLAG_RESULTS_READY,
				OS_FLAG_CLR, &oserr);
		}
	}
}

UCOS_MANAGED_ISR(ADC0_RESRDY_vect)
{
	uint8_t n = _adc_n_config;
	uint16_t res = ADC0.RES;
	_adc_results[n++].u16 = res;

	if (n == ADC_NUM_CONFIGS)
	{
		// Done sampling, notify the task
		uint8_t oserr = 0;
		OSFlagPost(_adc_task_flags, FLAG_RESULTS_READY,
			OS_FLAG_SET, &oserr);
		(void) oserr;
		OSIntNoSched = 0;
	}
	else
	{
		// Get the next channel going
		_start_sampling(n);
		OSIntNoSched = 1;
	}
}

static void _start_sampling(uint8_t n)
{
	ADC0.MUXPOS = pgm_read_byte(&_adc_config[n].muxpos);

	uint8_t neg = pgm_read_byte(&_adc_config[n].muxneg);
	if (neg & 0x80)
	{
		ADC0.CTRLA &= ~ADC_CONVMODE_bm;
	}
	else
	{
		ADC0.CTRLA |= ADC_CONVMODE_bm;
		ADC0.MUXNEG = neg;
	}

	ADC0.CTRLB = 0x7 & pgm_read_byte(&_adc_config[n].sampnum);
	ADC0.SAMPCTRL = pgm_read_byte(&_adc_config[n].samplen);

	_adc_n_config = n;
	ADC0.COMMAND = ADC_STCONV_bm;
}

static void _adc_timer_callback(void * ptmr, void * arg)
{
	(void) ptmr;
	(void) arg;
	_start_sampling(0);
}
