// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef DACX3608_HPP
#define DACX3608_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avl_i2c_i.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Texas Instruments DACx3608 eight channel I2C DAC.
///     DAC43608 - 8 bit
///     DAC53608 - 10 bit
///
/// This driver does not currently support the nCLR (clear all) and nLDAC (load
/// all) pins. nCLR should be driven high, and nLDAC should be either driven
/// low for immediate update, or pulsed low manually to update on demand.
struct dacx3608
{
	static constexpr uint8_t ADDR_GND = 0x90;
	static constexpr uint8_t ADDR_VDD = 0x92;
	static constexpr uint8_t ADDR_SDA = 0x94;
	static constexpr uint8_t ADDR_SCL = 0x96;

	static constexpr uint16_t MAX_VAL = 1023;

	enum class variant {
		dac43608,
		dac53608,
		unknown
	};

	/// Construct a dacx3608 instance.
	///
	/// @param i2c - i2c driver
	/// @param addr - left-aligned address. See constants ADDR_GND,
	///     ADDR_VDD, ADDR_SDA, and ADDR_SCL for the addresses selected
	///     by these pin strappings.
	dacx3608(avl::i2c_i * i2c, uint8_t addr);

	/// Identify the dacx3608. Establishes communications and checks the
	/// DEVICE_ID field. It's a good idea to ues this first to make sure
	/// the chip can talk.
	///
	/// If this returns variant::unknown, the device talking may not be
	/// a DACx3608.
	///
	/// Blocking.
	avl::result_t<variant> identify();

	/// Perform a software reset. Not necessary after power-on.
	///
	/// Blocking.
	avl::error_t swrst();

	/// Set power state of all channels. At power-on, all channels are
	/// powered down and must be turned on using this function.
	///
	/// @param channels - bitmask of channels, 0x01 = VA ... 0x80 = VH,
	///     high = on.
	///
	/// Blocking.
	avl::error_t set_power(uint8_t channels);

	/// Set power state of just one channel. This depends on internal
	/// tracking of the power state, so if there is any chance this state
	/// has been modified externally, call get_power() first.
	avl::error_t set_power(uint8_t channel, bool state);

	/// Get power state of all channels.
	///
	/// Updates the internal tracking of the power states
	/// (see set_power(uint8_t, bool)).
	///
	/// Blocking.
	avl::result_t<uint8_t> get_power();

	/// Set the output of a channel.
	///
	/// Always takes 10-bit numbers (ranging from 0 to 1023). For the
	/// DAC43608, the last two bits are truncated.
	///
	/// Blocking.
	///
	/// @param channel - channel number, 0 to 7
	/// @param value - value, 0 to 1023
	avl::error_t set_value(uint8_t channel, uint16_t value);

	/// Get the output of a channel.
	///
	/// Always returns 10-bit numbers (ranging from 0 to 1023). For the
	/// DAC43608, the last two bits are undefined.
	///
	/// Blocking.
	///
	/// @param channel - channel number, 0 to 7
	avl::result_t<uint16_t> get_value(uint8_t channel);

private:
	avl::result_t<uint16_t> _read_reg(uint8_t index);
	avl::error_t _write_reg(uint8_t index, uint16_t value);

	avl::i2c_i * m_i2c;
	uint8_t m_addr;

	uint8_t m_power;
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#endif // !defined(DACX3608_HPP)
