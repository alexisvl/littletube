// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <ucos_ii.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avr/fuse.h>
#include <avl_avrdx.hpp>
#include "hw.hpp"
#include "main.hpp"
#include "modes.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>

FUSES =
{
	/* wdtcfg */ 0,

	(3 << FUSE_LVL_gp) /* BOD level = 2.85V */
	| FUSE_SAMPFREQ_bm /* BOD sample freq = 32 Hz */
	| (1 << FUSE_ACTIVE_gp) /* BOD active */
	| (1 << FUSE_SLEEP_gp) /* BOD active in sleep */
	,

	(0 << FUSE_CLKSEL_gp), /* OSCHF */

	0x00, // reserved
	0x00, // reserved

	(3 << FUSE_CRCSRC_gp) /* No CRC */
	| FUSE_RSTPINCFG1_bm /* Reset pin as reset */
	| FUSE_EESAVE_bm, /* Save EEPROM on flash */

	(1 << FUSE_MVSYSCFG_gp) /* Dual voltage */
	| (2 << FUSE_SUT_gp), /* 2ms startup delay */

	0, /* Code size - entire flash is "boot code" */
	0, /* Boot size - entire flash is "boot code" */
};

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

int main()
{
	hw_pins_safe();

	ltm::mode mode = ltm::initial_mode();
	ltm::switch_mode(mode);

	// TODO: move RTOS ticks into ltm

	// Set up the WDT to catch loss of vsync
	while (WDT.STATUS & WDT_SYNCBUSY_bm);
	_PROTECTED_WRITE(
		WDT.CTRLA,
		WDT_WINDOW_OFF_gc | WDT_PERIOD_32CLK_gc
	);

	// Create a task
	OSInit();
	littletube::start_main_task();

	RTC.INTFLAGS = RTC_OVF_bm;	// No immediate interrupt on sei
	OSStart();
}

extern "C" {
void App_TaskCreateHook(OS_TCB *ptcb) { (void) ptcb; }
void App_TaskDelHook(OS_TCB *ptcb) { (void) ptcb; }
void App_TaskIdleHook() { }
void App_TaskReturnHook(OS_TCB *ptcb) { (void) ptcb; }
void App_TaskStatHook() { }
void App_TaskSwHook() { }
void App_TCBInitHook(OS_TCB *ptcb) { (void) ptcb; }
void App_TimeTickHook() { }
}

UCOS_MANAGED_ISR(RTC_CNT_vect)
{
	//XXX if the timers don't work, bring this back
	//the hook in os_cpu_c.c might do this
	//static uint8_t _timer_count = 0;

	RTC.INTFLAGS = RTC_OVF_bm;

	// Tell the WDT we got a tick. This is most useful when we switch over
	// to tick on VSYNC, in which case the video processor could crash and
	// stop delivering VSYNCs.
	wdt_reset();

	// Signal timers every 6 ticks (100ms)
	/*
	if (_timer_count++ == 6)
	{
		_timer_count = 0;
		OSTmrSignal();
	}
	*/

	OSTimeTick();
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

extern "C" void __cxa_pure_virtual()
{
	ltm::crash(ltm::crash_code::CRASH_PURE_VIRTUAL);
}
