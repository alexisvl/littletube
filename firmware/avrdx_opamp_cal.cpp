// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "avrdx_opamp_cal.hpp"

// Supporting modules
#include <avr/io.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------

struct opamp_unit {
	register8_t CTRLA;
	register8_t STATUS;
	register8_t RESMUX;
	register8_t INMUX;
	register8_t SETTLE;
	register8_t CAL;
	register8_t reserved[2];
};

// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void avrdx_opamp_cal(uint8_t n, cal_report& rept)
{
	if (n > 2)
	{
		rept.failed = true;
		return;
	}

	rept.offset_too_positive = false;
	rept.offset_too_negative = false;
	rept.impossible_to_measure = false;
	rept.failed = false;
	rept.offset = 0;
	rept.offset_post = 0;
	rept.cal_reg = 0x80;

	// DACOUT = PD6
	PORTD.PIN6CTRL = PORT_ISC_INPUT_DISABLE_gc;

	switch (n)
	{
	case 0:
		PORTD.PIN2CTRL = PORT_ISC_INPUT_DISABLE_gc;
		break;
	case 1:
		PORTD.PIN5CTRL = PORT_ISC_INPUT_DISABLE_gc;
		break;
	case 2:
		PORTE.PIN2CTRL = PORT_ISC_INPUT_DISABLE_gc;
		break;
	}

	VREF.ADC0REF = VREF_REFSEL_2V500_gc;
	VREF.DAC0REF = VREF_REFSEL_2V500_gc;

	// Make sure none of the opamp outputs come on unexpectedly
	OPAMP.OP0CTRLA = 0;
	OPAMP.OP1CTRLA = 0;
	OPAMP.OP2CTRLA = 0;

	// Turn on just the opamp we care about, set to buffer the DAC
	OPAMP.TIMEBASE = (F_CPU - 1) / 1000000uL;
	// cal rail-to-rail since it'll be used that way
	OPAMP.PWRCTRL = OPAMP_IRSEL_bm;
	OPAMP.CTRLA = OPAMP_ENABLE_bm;

	opamp_unit * unit = ((opamp_unit *) &OPAMP.OP0CTRLA) + n;
	unit->SETTLE = 200; // 200 us
	unit->CAL = 0x80;
	unit->INMUX
		= OPAMP_OP0INMUX_MUXPOS_DAC_gc | OPAMP_OP0INMUX_MUXNEG_OUT_gc;
	unit->RESMUX = 0;
	unit->CTRLA
		= OPAMP_OP0CTRLA_OUTMODE_NORMAL_gc | OPAMP_ALWAYSON_bm;

	// Emit 1.25V from DAC
	DAC0.CTRLA = DAC_OUTEN_bm | DAC_ENABLE_bm;
	DAC0.DATA = 0x7FC0u;

	// Set up the ADC to sample at 12 bits, accumulate 16 (full 16 bit)
	ADC0.CTRLA = ADC_RESSEL_12BIT_gc | ADC_ENABLE_bm;
	ADC0.CTRLB = ADC_SAMPNUM_ACC16_gc;
	ADC0.CTRLC = ADC_PRESC_DIV8_gc;
	ADC0.CTRLD = ADC_INITDLY_DLY32_gc;
	ADC0.CTRLE = 0;
	ADC0.SAMPCTRL = 10;
	ADC0.INTCTRL = 0;

	while (!(unit->STATUS & OPAMP_SETTLED_bm));

	ADC0.MUXPOS = ADC_MUXPOS_DAC0_gc;
	ADC0.COMMAND = ADC_STCONV_bm;
	while (!(ADC0.INTFLAGS & ADC_RESRDY_bm));

	uint16_t const reference = ADC0.RES;
	uint8_t muxpos_op;

	switch (n)
	{
	case 0:
		muxpos_op = ADC_MUXPOS_AIN2_gc;
		break;
	case 1:
		muxpos_op = ADC_MUXPOS_AIN5_gc;
		break;
	case 2:
		muxpos_op = ADC_MUXPOS_AIN10_gc;
		break;
	}
	ADC0.MUXPOS = muxpos_op;
	ADC0.COMMAND = ADC_STCONV_bm;
	while (!(ADC0.INTFLAGS & ADC_RESRDY_bm));

	uint16_t const precal = ADC0.RES;

	// Calibration step size is 0.5mV. At 2.5Vref, there are 13.1072 ADC
	// steps to a calibration step.
	int32_t const offset32 = (int32_t) precal - (int32_t) reference;

	if (offset32 < INT16_MIN || offset32 > INT16_MAX)
	{
		rept.impossible_to_measure = true;
		rept.failed = true;
	}
	rept.offset = offset32;

	// 3355 ~= 13.1072 * 256
	int32_t const cal = -(rept.offset * 256 + 3355/2) / 3355;

	if (cal < -0x7F)
	{
		rept.offset_too_positive = true;
		rept.failed = true;
	}
	else if (cal > 0x7F)
	{
		rept.offset_too_negative = true;
		rept.failed = true;
	}

	rept.cal_reg = 0x80 + cal;

	if (!rept.failed)
		unit->CAL = rept.cal_reg;

	// Final re-measurement
	ADC0.MUXPOS = ADC_MUXPOS_DAC0_gc;
	ADC0.COMMAND = ADC_STCONV_bm;
	while (!(ADC0.INTFLAGS & ADC_RESRDY_bm));
	uint16_t const reference_post = ADC0.RES;

	ADC0.MUXPOS = muxpos_op;
	ADC0.COMMAND = ADC_STCONV_bm;
	while (!(ADC0.INTFLAGS & ADC_RESRDY_bm));
	uint16_t const postcal = ADC0.RES;

	int32_t const offset_post32
		= (int32_t) postcal - (int32_t) reference_post;

	if (offset_post32 < INT16_MIN || offset_post32 > INT16_MAX)
	{
		rept.impossible_to_measure = true;
	}
	rept.offset_post = offset_post32;

	// Shut everything down
	unit->CTRLA = 0;
	OPAMP.CTRLA = 0;
	ADC0.CTRLA = 0;
	DAC0.CTRLA = 0;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
