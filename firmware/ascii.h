// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef ASCII_H
#define ASCII_H

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

enum ascii {
    ASCII_NUL = 0,
    ASCII_SOH,
    ASCII_STX,
    ASCII_ETX,
    ASCII_EOT,
    ASCII_ENQ,
    ASCII_ACK,
    ASCII_BEL,
    ASCII_BS,
    ASCII_TAB,
    ASCII_LF,
    ASCII_VT,
    ASCII_FF,
    ASCII_CR,
    ASCII_SO,
    ASCII_SI,
    ASCII_DLE,
    ASCII_DC1,
    ASCII_DC2,
    ASCII_DC3,
    ASCII_DC4,
    ASCII_NAK,
    ASCII_SYN,
    ASCII_ETB,
    ASCII_CAN,
    ASCII_EM,
    ASCII_SUB,
    ASCII_ESC,
    ASCII_FS,
    ASCII_GS,
    ASCII_RS,
    ASCII_US,
    ASCII_DEL = 0x7F,
};

#define ASCII_XON ASCII_DC1
#define ASCII_XOFF ASCII_DC3

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#endif // !defined(ASCII_H)
