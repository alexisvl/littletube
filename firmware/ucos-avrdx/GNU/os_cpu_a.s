;********************************************************************************************************
;                                              uC/OS-II
;                                        The Real-Time Kernel
;
;                    Copyright 1992-2020 Silicon Laboratories Inc. www.silabs.com
;
;                                   Modifications for AVR-DX port:
;                                   Copyright 2021 Alexis Lockwood
;
;                                 SPDX-License-Identifier: APACHE-2.0
;
;               This software is subject to an open source license and is distributed by
;                Silicon Laboratories Inc. pursuant to the terms of the Apache License,
;                    Version 2.0 available at www.apache.org/licenses/LICENSE-2.0.
;
;********************************************************************************************************

;********************************************************************************************************
;
;                                      AVR-DX        Specific code
;                                           GNU AVR Compiler
;
; Filename : os_cpu_a.s
; Version  : V2.93.00
; Port by  : Alexis Lockwood - based on ATxmega128 port
;********************************************************************************************************

#include  <os_cpu_i.h>

;********************************************************************************************************
;                                         PUBLIC DECLARATIONS
;********************************************************************************************************

		.global OS_CPU_SR_Save
		.global OS_CPU_SR_Restore
		.global OSStartHighRdy
		.global OSCtxSw
		.global OSIntCtxSw
        .global OSAvrManagedIntCtxSw


;********************************************************************************************************
;                                         EXTERNAL DECLARATIONS
;********************************************************************************************************

		.extern OSTaskSwHook
		.extern OSRunning
		.extern OSTCBHighRdy
		.extern OSTCBCur
		.extern OSPrioHighRdy
        .extern OSIntNoSched

		.text


;********************************************************************************************************
;                               START HIGHEST PRIORITY TASK READY-TO-RUN
;
; Description : This function is called by OSStart() to start the highest priority task that was created
;               by your application before calling OSStart().
;
; Note(s)     : 1) The (data)stack frame is assumed to look as follows:
;                                                +======+
;                  OSTCBHighRdy->OSTCBStkPtr --> |RAMPZ |
;                                                |R31   |
;                                                |R30   |
;                                                |R27   |
;                                                |.     |
;                                                |SREG  |
;                                                |R0    |
;                                                |PCH   |-|--> PC address (2 bytes)
;                                                |PCL   |-|                         (High memory)
;                                                +======+
;                  where the stack pointer points to the task start address.
;
;
;               2) OSStartHighRdy() MUST:
;                      a) Call OSTaskSwHook() then,
;                      b) Set OSRunning to TRUE,
;                      c) Switch to the highest priority task.
;********************************************************************************************************

OSStartHighRdy:
        CALL    OSTaskSwHook                                    ; Invoke user defined context switch hook

        LDI     R16, 1                                          ;
        STS     OSRunning,R16                                   ; OSRunning = 1

        LDS     R26,OSTCBHighRdy                                ; Let X point to TCB of highest priority task
        LDS     R27,OSTCBHighRdy+1                              ; ready to run

        RESTORE_SP                                              ; SP = MEM[X];
        POP_ALL                                                 ; Restore all registers
        RET                                                     ; Start task



;********************************************************************************************************
;                                       TASK LEVEL CONTEXT SWITCH
;
; Description : This function is called when a task makes a higher priority task ready-to-run.
;
; Note(s)     : (1) (A) Upon entry,
;                       OSTCBCur     points to the OS_TCB of the task to suspend
;                       OSTCBHighRdy points to the OS_TCB of the task to resume
;
;                   (B) The stack frame of the task to suspend looks as follows:
;
;                                            SP+0 --> LSB of task code address
;                                              +2     MSB of task code address            (High memory)
;
;                   (C) The saved context of the task to resume looks as follows:
;
;                                                      +======+
;                        OSTCBHighRdy->OSTCBStkPtr --> |RAMPZ |                           (Low memory)
;                                                      |R31   |
;                                                      |R30   |
;                                                      |R27   |
;                                                      |.     |
;                                                      |SREG  |
;                                                      |R0    |
;                                                      |PCH   |-|--> PC address (2 bytes)
;                                                      |PCL   |-|                          (High memory)
;                                                      +======+
;                 (2) The OSCtxSW() MUST:
;                     - Save all register in the current task task.
;                     - Make OSTCBCur->OSTCBStkPtr = SP.
;                     - Call user defined task swith hook.
;                     - OSPrioCur                  = OSPrioHighRdy
;                     - OSTCBCur                   = OSTCBHihgRdy
;                     - SP                         = OSTCBHighRdy->OSTCBStrkPtr
;                     - Pop all the register from the new stack
;********************************************************************************************************

OSCtxSw:
	PUSH_ALL                                                ; Save current task's context

	IN      R28,  SPL                                       ; Y = SP
	IN      R29,  SPH                                       ;

	LDS     R26,OSTCBCur                                    ; X = OSTCBCur->OSTCBStkPtr
	LDS     R27,OSTCBCur+1                                  ;
	ST      X+,R28                                          ; *X = SP
	ST      X+,R29                                          ;

	CALL    OSTaskSwHook                                    ; Call user defined task switch hook

	LDS     R16,OSPrioHighRdy                               ; OSPrioCur = OSPrioHighRdy
	STS     OSPrioCur,R16

	LDS     R26,OSTCBHighRdy                                ; Let X point to TCB of highest priority task
	LDS     R27,OSTCBHighRdy+1                              ; ready to run
	STS     OSTCBCur,R26                                    ; OSTCBCur = OSTCBHighRdy
	STS     OSTCBCur+1,R27

	RESTORE_SP                                              ; SP = MEM[X];
	POP_ALL
	RET



;*********************************************************************************************************
;                                INTERRUPT LEVEL CONTEXT SWITCH
;
; Description : This function is called by OSIntExit() to perform a context switch to a task that has
;               been made ready-to-run by an ISR.
;
; Note(s)     : 1) Upon entry,
;                  OSTCBCur     points to the OS_TCB of the task to suspend
;                  OSTCBHighRdy points to the OS_TCB of the task to resume
;
;               2) The stack frame of the task to suspend looks as follows:
;
;                  OSTCBCur->OSTCBStkPtr ------> RAMPZ                                   (Low memory)
;                                                R31
;                                                R30
;                                                R27
;                                                .
;                                                .
;                                                Flags to load in status register
;                                                R0
;                                                PCH
;                                                PCL                                     (High memory)
;
;               3) The saved context of the task to resume looks as follows:
;
;                  OSTCBHighRdy->OSTCBStkPtr --> RAMPZ                                   (Low memory)
;                                                R31
;                                                R30
;                                                R27
;                                                .
;                                                .
;                                                Flags to load in status register
;                                                R0
;                                                PCH
;                                                PCL                                     (High memory)
;*********************************************************************************************************

OSIntCtxSw:
        LDS     R0, OSIntNoSched ; If we came from a managed ISR, don't use RETI
        ;SBRC    R0, 1
        ;RJMP    OSIntCtxSw_NoRETI
        CALL    OSTaskSwHook
        LDS     R16, OSPrioHighRdy
        STS     OSPrioCur, R16                                  ; switch to the new priority
        LDS     R26, OSTCBHighRdy
        LDS     R27, OSTCBHighRdy+1
        STS     OSTCBCur, R26
        STS     OSTCBCur+1, R27                                 ; and to the new TCB
        LD      R28, X+
        LD      R29, X+
        OUT     SPL, R28                                        ; switch to the new stack
        OUT     SPH, R29
        POP     R0                                              ; and start restoring context
        OUT     RAMPZ, R0
        POP     R31
        POP     R30
        POP     R29
        POP     R28
        POP     R27
        POP     R26
        POP     R25
        POP     R24
        POP     R23
        POP     R22
        POP     R21
        POP     R20
        POP     R19
        POP     R18
        POP     R17
        POP     R16
        POP     R15
        POP     R14
        POP     R13
        POP     R12
        POP     R11
        POP     R10
        POP     R9
        POP     R8
        POP     R7
        POP     R6
        POP     R5
        POP     R4
        POP     R3
        POP     R2
        POP     R1
        POP     R0
        ; If interrupts are going to be enabled, we have to do it immediately before the RETI to
        ; support the nested "Managed ISR" (see os_cpu.h). We have nowhere good to remember whether
        ; that bit would be set now, so use a branch to remmeber it in the program counter.
        SBRS    R0, 7       ; if !(r0 & 0x80)
        RJMP    1f          ;   goto 1
        ; Use the same trick as in OSAvrManagedIntCtxSw to clear a bit with only r0 and SREG available
        CLT                 ; SREG.bit[T] = 0
        BLD     R0, 7       ; r0.bit[7] = SREG.bit[T]
        OUT     SREG, R0
        POP     R0
        SEI
        RETI
1:      OUT     SREG, R0
        POP     R0
        RETI

;*********************************************************************************************************
;                   "SMART" INTERRUPT LEVEL CONTEXT SWITCH
;
; Description : This block (to be jumped to, not called) manages a context switch at the end of a
;               "managed" ISR (one using UCOS_MANAGED_ISR(), see os_cpu.h).
;
; Note(s)     : This mechanism takes advantage of AVR's guarantees regarding the next instruction after
;               re-enabling interrupts being executed before any interrupts, and about disabling
;               intterupts being instaneous. See os_cpu.h. This block is always entered with interrupts
;               disabled.
;
;               SREG has NOT been saved yet, so any parts of this block that have not manually saved it
;               must not modify flags.
;*********************************************************************************************************

OSAvrManagedIntCtxSw:
        PUSH    R0
        IN      R0, SREG
        ; Interrupts have been disabled to prevent nesting before we can get the stack back to normal,
        ; but they were obviously enabled before since we just came from an ISR. Silly trick to set
        ; 0x80 in r0 - we can't use high regs because they haven't been saved yet.
        SET                 ; SREG.bit[T] = 1
        BLD     R0, 7      ; r0.bit[7] = SREG.bit[T]
        PUSH    R0

        LDS     R0, OSIntNoSched    ; on OSIntNoSched, use an abbreviated exit. No need to save/restore
        SBRS    R0, 0
        RJMP    1f

        LDS     R0, OSIntNesting
        DEC     R0
	; TODO SATURATING DEC
        STS     OSIntNesting, R0

        POP     R0
        CLT
        BLD     R0, 7
        OUT     SREG, R0
        POP     R0
        SEI
        RETI

1:      PUSH    R1
        PUSH    R2
        PUSH    R3
        PUSH    R4
        PUSH    R5
        PUSH    R6
        PUSH    R7
        PUSH    R8
        PUSH    R9
        PUSH    R10
        PUSH    R11
        PUSH    R12
        PUSH    R13
        PUSH    R14
        PUSH    R15
        PUSH    R16
        PUSH    R17
        PUSH    R18
        PUSH    R19
        PUSH    R20
        PUSH    R21
        PUSH    R22
        PUSH    R23
        PUSH    R24
        PUSH    R25
        PUSH    R26
        PUSH    R27
        PUSH    R28
        PUSH    R29
        PUSH    R30
        PUSH    R31
        IN      R0, RAMPZ
        PUSH    R0
        IN      R28, SPL                                        ; save the stack pointer in old TCB
        IN      R29, SPH
        LDS     R26, OSTCBCur
        LDS     R27, OSTCBCur+1
        ST      X+, R28
        ST      X+, R29
        CALL    OSIntExit
        ; If we make it back here, there's no context switch necessary. But we just stacked
        ; everything, so we need to put it back how we found it. Just execute the switch anyway.
        JMP     OSIntCtxSw
