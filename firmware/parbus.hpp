// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

/// This poorly named module implements the interface between the video
/// processor and control processor, including the parallel bus.

#ifndef PARBUS_HPP
#define PARBUS_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avl_error.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Types of bus actions.
///
/// Most data is PIXEL addressed, which means 2 bit addresses, not 8 bit.
/// Addresses from FF00 to FFFF are the config addresses; they are
/// byte-addressed.
enum class parbus_action: uint8_t
{
	/// Send the address low byte
	WRITE_ADDR_LOW = 0,
	/// Send the address high byte
	WRITE_ADDR_HIGH = 1,
	/// Write 8 bits and autoincrement by 4 pixel/8 bits
	WRITE_8 = 2,
	/// Write 2 bits and autoincrement by 1 pixel/2 bits
	WRITE_2 = 3,
	/// Write 6 pixels (12 bits) following bit 5...0, where each two
	/// get set to 7...6 if n=1 else set to 0. Autoincrement by 6 pixels
	WRITE_6_PATTERN = 4,
	/// Write up to 64 pixels the same color (run-length coding). Bits
	/// 7...6 are the color, and one plus bits 5...0 are the number of
	/// pixels. Autoincrement by however many were set.
	WRITE_RLE = 5,
	/// Read 8 bits and autoincrement by 8 bits. Requires the address to
	/// be 8 bit aligned.
	READ = 7,
};

/// Config registers
enum parbus_cfg_reg
{
	// 8-bit config register. Bits:
	//     0: RUN. If high, run; if low, constant-vsync.
	//     1: VBUF_EN
	//     2: MODE. 1 = 240x240, 0 = 320x200
	CFG_MAIN = 0xFF00,
	CFG_HPOS_PWM = 0xFF01,
	CFG_VPOS_PWM = 0xFF02,

	CFG_MAGIC = 0xFF10, // Always reads CFG_MAGIC_VAL
	CFG_BUILD_HASH_1 = 0xFF11,
	CFG_BUILD_HASH_2 = 0xFF12,
	CFG_BUILD_HASH_3 = 0xFF13,
	CFG_BUILD_HASH_4 = 0xFF14,
};
#define CFG_MAGIC_VAL 0xA4
#define CFG_OFFSET 0xFF00
#define NUM_CFG (0x14 + 1)

/// Littletube parallel bus transmitter
struct parbus_transmitter
{
	/// Perform a raw bus write action.
	///
	/// @param ac - action
	/// @param ad - address or data
	///
	/// @retval error_t::OK - action was received and will be executed
	/// @retval error_t::INTR - vsync ended and the bus cycle was terminated
	/// @retval error_t::TIMEDOUT - action timed out, VP unresponsive
	avl::error_t write(parbus_action ac, uint8_t ad);

	/// Perform a read to the currently selected address.
	///
	/// @retval avl::error_t::INTR: interrupted by vsync
	/// @retval avl::error_t::TIMEDOUT: timed out, VP unresponsive
	avl::result_t<uint8_t> read();

	/// Write an address.
	avl::error_t write_addr(uint16_t addr);
};

/// Littletube parallel bus receiver
///
/// Any action that is received has been acked, and must be executed. When the
/// VSYNC interval has ended, the caller should stop calling read().
struct parbus_receiver
{
	/// Get a raw bus action.
	///
	/// If the action is a READ, the action is not acknowledged. You must
	/// call service_read() to acknowledge.
	///
	/// @param ac - action
	/// @param ad - address or data
	/// @retval true - got an action
	/// @retval false - interrupted by GPR_FLAG_PEND_EXIT_VSYNC
	bool get(parbus_action * ac, uint8_t * ad);

	/// Service a READ action. This posts the requested data to the bus
	/// and then acknowledges.
	void service_read(uint8_t data);
};

#ifdef BUILD_CONTROL
void parbus_vp_to_constant_vsync();
void parbus_vp_to_run();
avl::error_t parbus_vp_sleep();
avl::error_t parbus_vp_wake();

/// Bring up the video processor on boot and load information from it. It will
/// be left in the awake state.
avl::error_t parbus_vp_startup();

/// Return a pointer to the video processor's build hash. parbus_vp_startup()
/// must have returned success.
char const * parbus_vp_get_build();

#elif defined(BUILD_VIDEO)
enum class video_mode: uint8_t
{
	SLEEP,
	WAKE,
};
video_mode parbus_check_mode(void);
#endif

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#endif // !defined(PARBUS_HPP)
