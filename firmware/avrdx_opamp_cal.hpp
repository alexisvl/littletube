// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVRDX_OPAMP_CAL_HPP
#define AVRDX_OPAMP_CAL_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avl_error.hpp>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Calibration report
struct cal_report {
	bool offset_too_positive;
	bool offset_too_negative;

	/// An extremely unexpected result occurred. Due to the use of external
	/// pins, this may mean e.g. a pin is being driven externally.
	bool impossible_to_measure;

	bool failed;

	/// Actual offset, in increments of 38.15uV (16-bit ADC step at 2.5V)
	int16_t offset;

	/// Post-cal offset, in increments of 38.15uV (16-bit ADC step at 2.5V)
	int16_t offset_post;

	uint8_t cal_reg;
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

// THIS DOESN'T WORK AND I DON'T KNOW WHY. Consider it a prototype
// implementation of the scheme suggested in the datasheet. I don't think
// it's necessary.
//
/// Calibration routine for AVR-Dx opamp. Requires control over OPAMP, ADC,
/// and DAC (so should be run prior to any higher-level analog driver
/// initialization). Also requires the ability to enable the DAC pin and OPAMP
/// output pin and drive arbitrary voltages out them. Requires RTOS running.
///
/// Blocking.
///
/// @param n - opamp number to calibrate
void avrdx_opamp_cal(uint8_t n, cal_report& rept);

#endif // !defined(AVRDX_OPAMP_CAL_HPP)
