// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// VIDEO TIMING GENERATOR
//
// Hardware used:
// 	TCA0
// 	GPR0
// 	GPR1
// 	GPR2

#ifndef TIMING_HPP
#define TIMING_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

/// Last of all sync lines (including blanking)
#define LAST_SYNC_LINE 19

/// Last of true vsync lines (VSYNC_VGA asserted)
#define LAST_VSYNC_VGA_LINE 5

// --- PUBLIC VARIABLES --------------------------------------------------------

/// Number of line currently being scanned, starting at 1. This is not updated
/// during graphics output, hence being able to use an 8-bit integer (note that
/// no external code can run at this time anyway, only timing interrupts).
#define timing_current_line GPR.GPR0

/// Whether scanning an odd frame
#define timing_odd_frame (GPR.GPR2 & 0x01)

// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Start timers and enter video generation mode
void timing_start();

/// Stop timers.
void timing_stop();

#endif // !defined(TIMING_HPP)
