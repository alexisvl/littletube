// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// VIDEO TIMING GENERATOR
//
// TCA0 is used to generate a pair of interrupts: start of sync, start of
// backporch. During the viewable area, the start-of-backporch interrupt
// handler will emit display data (and thus will last the entire visible line
// until the frontporch).
//
// The following types of lines are emitted (with the OVF vector at the
// beginning each time):
//
// START ODD LINES:               ________________________________
// ______________________________/    33.5us black
// 30us blank level              CMP0
//
// START EVEN LINES:
//    ______________________________                             _
// __/ 29.8us black                 \___________________________/2us
//   CMP0                           CMP1 29.4us blank           CMP2
// 2us
//
// NORMAL LINE:
//    _____##################################################_____
// __/ 5us black        VIDEO DATA                           5us black
//   CMP0  CMP1
// 2us
//
// An odd field consists of 1x START ODD LINES, 18x NORMAL LINES all black,
// 240x NORMAL LINES visible (20x black either side for 320x200), 2x NORMAL
// LINES all black (start vsync)
//
// These interrupts also generate the external timing signals, as follows:
//
//     OVF: +HSYNC, +HSYNC_VGA, if (line == 0) { +VSYNC_VGA },
//                 if (line == LAST_SYNC_LINE) { +PEND_EXIT_VSYNC }
//                 if (line == LAST_SYNC_LINE+1) { -VSYNC_VGA }
//     CMP0: -HSYNC, -HSYNC_VGA
//     CMP1: +HSYNC if outside visible data, else start outputting
//     CMP2: -HSYNC
//
// The VSYNC pin is managed by the main loop, as it also signals when access
// is permitted.
//
// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "timing.hpp"

// Supporting modules
#include <avr/io.h>
#include <avr/interrupt.h>
#include "hw.hpp"
#include "hw.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------

#define N_LINES_AFTER_START 243

// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

extern uint8_t * vid_ptr;
extern uint8_t framebuffer[64000/4 + 4];

// --- PUBLIC FUNCTIONS --------------------------------------------------------

void timing_start()
{
	timing_current_line = 0;
	TCA0.SINGLE.CTRLA = 0;
	TCA0.SINGLE.CTRLESET = TCA_SINGLE_CMD_RESET_gc;

	TCA0.SINGLE.CTRLB = 0
		| TCA_SINGLE_WGMODE_NORMAL_gc
		;
	TCA0.SINGLE.CTRLC = 0;
	TCA0.SINGLE.CTRLD = 0;

	TCA0.SINGLE.CMP0 = 48;
	TCA0.SINGLE.PER = 1524;

	TCA0.SINGLE.INTFLAGS = 0xFF;
	TCA0.SINGLE.INTCTRL = TCA_SINGLE_OVF_bm;

	TCA0.SINGLE.CTRLA = 0
		| TCA_SINGLE_CLKSEL_DIV1_gc
		| TCA_SINGLE_ENABLE_bm
		;
}

void timing_stop()
{
	TCA0.SINGLE.INTFLAGS = 0;
	TCA0.SINGLE.CTRLA = 0;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

#pragma GCC optimize "-O3"
ISR(TCA0_OVF_vect)
{
	PIN_HSYNC = 1;
	PIN_HSYNC_VGA = 1;

	TCA0.SINGLE.INTFLAGS = 0xFF;

	uint8_t current_line = timing_current_line;

	// Get in and out FAST, we have 2us.
	if (current_line == 0)
	{
		PIN_VID_VPORT_OUT = 0;
		GPR_FLAGS &= ~GPR_FLAG_DISPLAYING;
		GPR_FLAGS &= ~GPR_FLAG_PEND_EXIT_VSYNC;
		PIN_VSYNC_VGA = 1;
		vid_ptr = &framebuffer[0];
		if (timing_odd_frame)
		{
			// Set up timer for START_ODD
			TCA0.SINGLE.CMP0 = 720;
			TCA0.SINGLE.INTCTRL = TCA_SINGLE_CMP0_bm
				| TCA_SINGLE_OVF_bm;
		}
		else
		{
			// Set up timer for START_EVEN
			TCA0.SINGLE.CMP1 = 715 + 48;
			TCA0.SINGLE.CMP2 = 706 + 715 + 48;
			TCA0.SINGLE.INTCTRL =
				TCA_SINGLE_CMP0_bm | TCA_SINGLE_CMP1_bm
				| TCA_SINGLE_CMP2_bm | TCA_SINGLE_OVF_bm;
		}
		GPR_FLAGS ^= GPR_FLAG_ODD;
		timing_current_line |= 1; // faster :)
		GPR_VISIBLE_LINE = 0;
	}
	else if (current_line == LAST_SYNC_LINE + 1)
	{
		uint8_t vis = GPR_VISIBLE_LINE;
		if (vis >= N_LINES_AFTER_START)
		{
			// Entire frame is finished.
			timing_current_line = 0;
			TCA0.SINGLE.INTCTRL = TCA_SINGLE_CMP0_bm
				| TCA_SINGLE_OVF_bm;
		}
		else if (vis >= 200 && !(GPR_FLAGS & GPR_FLAG_240_240))
		{
			// Generating a black line - final letterboxing
			TCA0.SINGLE.INTCTRL = TCA_SINGLE_CMP0_bm
				| TCA_SINGLE_OVF_bm;
			GPR_VISIBLE_LINE = vis + 1;
		}
		else
		{
			// Generating display data.
			TCA0.SINGLE.CMP1 = 120 + 48;
			GPR_FLAGS |= GPR_FLAG_DISPLAYING;
			TCA0.SINGLE.INTCTRL =
				TCA_SINGLE_CMP0_bm | TCA_SINGLE_CMP1_bm
				| TCA_SINGLE_OVF_bm;
		}
	}
	else
	{
		if (current_line == LAST_SYNC_LINE)
		{
			GPR_FLAGS |= GPR_FLAG_PEND_EXIT_VSYNC;
		}
		if (current_line == LAST_VSYNC_VGA_LINE + 1)
		{
			PIN_VSYNC_VGA = 0;
		}
		// Generating a black line
		timing_current_line = current_line + 1;
		TCA0.SINGLE.CMP0 = 48;
		TCA0.SINGLE.INTCTRL = TCA_SINGLE_CMP0_bm
			| TCA_SINGLE_OVF_bm;
	}
}
#pragma GCC reset_options
