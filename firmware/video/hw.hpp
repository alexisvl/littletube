// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef HW_HPP
#define HW_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avr/io.h>
#include <avl_avrdx.hpp>
#include "parbus.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

// See redundant defs in hw.h!
extern avl::pin<avl::PORTA_ADDR, 0> PIN_CLK;
extern avl::pin<avl::PORTA_ADDR, 1> PIN_HPOS_PWM;
extern avl::pin<avl::PORTA_ADDR, 2> PIN_VPOS_PWM;
extern avl::pin<avl::PORTA_ADDR, 3> PIN_VBUF_EN;
extern avl::pin<avl::PORTA_ADDR, 4> PIN_HSYNC;
extern avl::pin<avl::PORTA_ADDR, 5> PIN_VSYNC;
extern avl::pin<avl::PORTA_ADDR, 6> PIN_HSYNC_VGA;
extern avl::pin<avl::PORTA_ADDR, 7> PIN_VSYNC_VGA;
extern avl::pin<avl::PORTB_ADDR, 0> PIN_VID_0;
extern avl::pin<avl::PORTB_ADDR, 1> PIN_VID_1;
extern avl::pin<avl::PORTB_ADDR, 2> PIN_VBUF_EN_DUPLICATE;
extern avl::pin<avl::PORTB_ADDR, 3> PIN_PB3;
extern avl::pin<avl::PORTB_ADDR, 4> PIN_PB4;
extern avl::pin<avl::PORTB_ADDR, 5> PIN_PB5;
extern avl::pin<avl::PORTD_ADDR, 0> PIN_PD0;
extern avl::pin<avl::PORTD_ADDR, 1> PIN_INTEN_2_IN;
extern avl::pin<avl::PORTD_ADDR, 2> PIN_INTEN_2_OUT;
extern avl::pin<avl::PORTD_ADDR, 3> PIN_PD3;
extern avl::pin<avl::PORTD_ADDR, 4> PIN_INTEN_1_IN;
extern avl::pin<avl::PORTD_ADDR, 5> PIN_INTEN_1_OUT;
extern avl::pin<avl::PORTD_ADDR, 6> PIN_BLACK_OUT;
extern avl::pin<avl::PORTD_ADDR, 7> PIN_PD7;
extern avl::pin<avl::PORTE_ADDR, 0> PIN_PE0;
extern avl::pin<avl::PORTE_ADDR, 1> PIN_PE1;
extern avl::pin<avl::PORTE_ADDR, 2> PIN_PE2;
extern avl::pin<avl::PORTE_ADDR, 3> PIN_PE3;
extern avl::pin<avl::PORTF_ADDR, 3> PIN_nSTB;
extern avl::pin<avl::PORTF_ADDR, 4> PIN_nACK;

extern avl::pin_group<avl::PORTC_ADDR, 0xFF> PINGRP_ADn;
extern avl::pin_group<avl::PORTF_ADDR, 0xFF> PINGRP_ACn_nSTB_nACK;
#define PIN_AC0_bp 0
#define PIN_AC1_bp 1
#define PIN_AC2_bp 2
#define PIN_nSTB_bp 3
#define PIN_nACK_bp 4

extern parbus_receiver parbus;

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#endif // !defined(HW_HPP)
