// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "hw.hpp"

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------

avl::pin<avl::PORTA_ADDR, 0> PIN_CLK;
avl::pin<avl::PORTA_ADDR, 1> PIN_HPOS_PWM;
avl::pin<avl::PORTA_ADDR, 2> PIN_VPOS_PWM;
avl::pin<avl::PORTA_ADDR, 3> PIN_VBUF_EN;
avl::pin<avl::PORTA_ADDR, 4> PIN_HSYNC;
avl::pin<avl::PORTA_ADDR, 5> PIN_VSYNC;
avl::pin<avl::PORTA_ADDR, 6> PIN_HSYNC_VGA;
avl::pin<avl::PORTA_ADDR, 7> PIN_VSYNC_VGA;
avl::pin<avl::PORTB_ADDR, 0> PIN_VID_0;
avl::pin<avl::PORTB_ADDR, 1> PIN_VID_1;
avl::pin<avl::PORTB_ADDR, 2> PIN_VBUF_EN_DUPLICATE;
avl::pin<avl::PORTB_ADDR, 3> PIN_PB3;
avl::pin<avl::PORTB_ADDR, 4> PIN_PB4;
avl::pin<avl::PORTB_ADDR, 5> PIN_PB5;
avl::pin<avl::PORTD_ADDR, 0> PIN_PD0;
avl::pin<avl::PORTD_ADDR, 1> PIN_INTEN_2_IN;
avl::pin<avl::PORTD_ADDR, 2> PIN_INTEN_2_OUT;
avl::pin<avl::PORTD_ADDR, 3> PIN_PD3;
avl::pin<avl::PORTD_ADDR, 4> PIN_INTEN_1_IN;
avl::pin<avl::PORTD_ADDR, 5> PIN_INTEN_1_OUT;
avl::pin<avl::PORTD_ADDR, 6> PIN_BLACK_OUT;
avl::pin<avl::PORTD_ADDR, 7> PIN_PD7;
avl::pin<avl::PORTE_ADDR, 0> PIN_PE0;
avl::pin<avl::PORTE_ADDR, 1> PIN_PE1;
avl::pin<avl::PORTE_ADDR, 2> PIN_PE2;
avl::pin<avl::PORTE_ADDR, 3> PIN_PE3;
avl::pin<avl::PORTF_ADDR, 3> PIN_nSTB;
avl::pin<avl::PORTF_ADDR, 4> PIN_nACK;

avl::pin_group<avl::PORTC_ADDR, 0xFF> PINGRP_ADn;
avl::pin_group<avl::PORTF_ADDR, 0xFF> PINGRP_ACn_nSTB_nACK;

parbus_receiver parbus;

// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------
// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
