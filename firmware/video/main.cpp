// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "parbus.hpp"
#include "timing.hpp"
#include "hw.hpp"
#include "hw.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

FUSES =
{
	/* wdtcfg */ 0,

	(3 << FUSE_LVL_gp) /* BOD level = 2.85V */
	| FUSE_SAMPFREQ_bm /* BOD sample freq = 32 Hz */
	| (1 << FUSE_ACTIVE_gp) /* BOD active */
	| (1 << FUSE_SLEEP_gp) /* BOD active in sleep */
	,

	(0 << FUSE_CLKSEL_gp), /* OSCHF */

	0x00, // reserved
	0x00, // reserved

	(3 << FUSE_CRCSRC_gp) /* No CRC */
	| FUSE_RSTPINCFG1_bm /* Reset pin as reset */
	| FUSE_EESAVE_bm, /* Save EEPROM on flash */

	(0 << FUSE_MVSYSCFG_gp) /* Single voltage */
	| (2 << FUSE_SUT_gp), /* 2ms startup delay */

	0, /* Code size - entire flash is "boot code" */
	0, /* Boot size - entire flash is "boot code" */
};

extern "C"
{
	void output_frame_240_240(uint8_t * framebuffer);
	void output_frame_320_200(uint8_t * framebuffer);
}

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------

/// Native number of pixels in the framebuffer
#define NUM_PIX 64000

uint8_t framebuffer[NUM_PIX/4 + 4];
static uint8_t _configspace[NUM_CFG];

// TODO
static const bool _vsync = true;

// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static void _sleep();
static void _run();
static void _exec_write(uint16_t * paddr, parbus_action ac, uint8_t ad);
static void _exec_cfg(uint8_t cfg_offset, uint8_t ad);
static void _set_pix(uint16_t addr, uint8_t pix);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

using namespace avl;

int main()
{
	// TODO: hw_pins_safe()
	PINGRP_ADn << port_dir::input;
	PINGRP_ACn_nSTB_nACK.port().OUTSET = (0x10 << PIN_AC0_bp);
	PINGRP_ACn_nSTB_nACK.port().DIR = (0x10 << PIN_AC0_bp);
	PIN_VID_0.vport().DIR = 0;
	PIN_VID_0 << port_dir::output_0;
	PIN_VID_1 << port_dir::output_0;
	PIN_HSYNC << port_dir::output_0;
	PIN_HSYNC_VGA << port_dir::output_0;
	PIN_VSYNC_VGA << port_dir::output_0;

	GPR.GPR0 = 0;
	GPR.GPR1 = 0;
	GPR.GPR2 = 0;

	// HACK to make VBUF_EN work: its pin has EVOUTB, so connect event
	// channel 0 to EVOUTB and then output via evsys.
	PORTMUX.EVSYSROUTEA = 0;
	EVSYS.CHANNEL0 = EVSYS_CHANNEL0_PORTA_PIN3_gc;
	EVSYS.USEREVSYSEVOUTB = EVSYS_USER_CHANNEL0_gc;
	PIN_VBUF_EN << port_dir::output_0;
	PIN_VBUF_EN_DUPLICATE << port_dir::output_0;

	video_mode mode = parbus_check_mode();

	_configspace[CFG_MAGIC - CFG_OFFSET] = CFG_MAGIC_VAL;

	uint32_t bh = BUILD_HASH_NUM;
	for (int i = 0; i < 4; i += 1)
	{
		_configspace[CFG_BUILD_HASH_1 - CFG_OFFSET + i]
			= bh & 0xFF;
		bh >>= 8;
	}

	if (mode == video_mode::SLEEP)
		_sleep();
	else
		_run();
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _sleep()
{
	PIN_VSYNC << port_dir::output_0;
	cli();
	for (;;)
	{
		SLPCTRL.CTRLA = SLPCTRL_SMODE_PDOWN_gc | SLPCTRL_SEN_bm;
		asm volatile ("sleep");
	}
}

static void _run()
{
	// External clock
	_PROTECTED_WRITE(
		CLKCTRL.XOSCHFCTRLA,
		CLKCTRL_SELHF_bm | CLKCTRL_ENABLE_bm
	);
	_PROTECTED_WRITE(
		CLKCTRL.MCLKCTRLA,
		CLKCTRL_CLKSEL_EXTCLK_gc
	);
	sei();
	PIN_HSYNC << port_dir::output_1;
	PIN_VSYNC << port_dir::output_1;

	// DAC should output 0.3V for black level
	VREF.DAC0REF = VREF_REFSEL_1V024_gc;
	DAC0.DATA = 0xFFFFu * (0.3 / 1.024);
	DAC0.CTRLA = DAC_OUTEN_bm | DAC_ENABLE_bm;

	OPAMP.TIMEBASE = F_CPU / 1000000;
	OPAMP.OP0INMUX =
		OPAMP_OP0INMUX_MUXPOS_INP_gc
		| OPAMP_OP0INMUX_MUXNEG_OUT_gc;
	OPAMP.OP0CTRLA = OPAMP_OP0CTRLA_OUTMODE_NORMAL_gc | OPAMP_ALWAYSON_bm;
	OPAMP.OP1INMUX =
		OPAMP_OP1INMUX_MUXPOS_INP_gc
		| OPAMP_OP1INMUX_MUXNEG_OUT_gc;
	OPAMP.OP1CTRLA = OPAMP_OP1CTRLA_OUTMODE_NORMAL_gc | OPAMP_ALWAYSON_bm;
	OPAMP.OP2INMUX =
		OPAMP_OP2INMUX_MUXPOS_DAC_gc
		| OPAMP_OP2INMUX_MUXNEG_OUT_gc;
	OPAMP.OP2CTRLA = OPAMP_OP2CTRLA_OUTMODE_NORMAL_gc | OPAMP_ALWAYSON_bm;
	OPAMP.CTRLA = OPAMP_ENABLE_bm;

	union {
		struct {
			uint8_t lo;
			uint8_t hi;
		};
		uint16_t word;
	} addr = {.word = 0};

	for (;;)
	{
		parbus_action ac;
		uint8_t ad;

		if (GPR_FLAGS & GPR_FLAG_PEND_EXIT_VSYNC)
		{
			PIN_VSYNC = 0;
			// TODO: Still getting too much latency jitter.
			// Instead of spinning between interrupts, we should
			// SLEEP between interrupts.
			while (GPR_FLAGS & GPR_FLAG_PEND_EXIT_VSYNC);
			continue;
		}
		else
		{
			PIN_VSYNC = 1;
		}

		if (!parbus.get(&ac, &ad))
			continue;

		if (ac == parbus_action::READ)
		{
			if (addr.word < NUM_PIX)
			{
				parbus.service_read(
					framebuffer[addr.word / 4]
				);
			}
			else if (addr.word >= CFG_OFFSET)
			{
				size_t n = addr.word - CFG_OFFSET;
				if (n < sizeof(_configspace))
					parbus.service_read(
						_configspace[n]
					);
				else
					parbus.service_read(0xFF);
			}
			else
			{
				parbus.service_read(0xFF);
			}
		}
		else if (ac == parbus_action::WRITE_ADDR_LOW)
		{
			addr.lo = ad;
		}
		else if (ac == parbus_action::WRITE_ADDR_HIGH)
		{
			addr.hi = ad;
		}
		else if (addr.word < NUM_PIX)
		{
			_exec_write(&addr.word, ac, ad);
		}
		else if ((addr.word - 0xFF00) < sizeof(_configspace))
		{
			_exec_cfg(addr.word - 0xFF00, ad);
		}
	}
}

static void _exec_write(uint16_t * paddr, parbus_action ac, uint8_t ad)
{
	uint8_t pix = ad >> 6;
	uint16_t addr = *paddr;

	switch (ac)
	{
	case parbus_action::WRITE_8:
		_set_pix(addr++, ad); ad >>= 2;
		_set_pix(addr++, ad); ad >>= 2;
		_set_pix(addr++, ad); ad >>= 2;
	case parbus_action::WRITE_2:
		_set_pix(addr++, ad); ad >>= 2;
		break;
	case parbus_action::WRITE_6_PATTERN:
		if (ad & 0x20) _set_pix(addr, pix); ++addr;
		if (ad & 0x10) _set_pix(addr, pix); ++addr;
		if (ad & 0x08) _set_pix(addr, pix); ++addr;
		if (ad & 0x04) _set_pix(addr, pix); ++addr;
		if (ad & 0x02) _set_pix(addr, pix); ++addr;
		if (ad & 0x01) _set_pix(addr, pix); ++addr;
		break;
	case parbus_action::WRITE_RLE:
		for (int i = 0; i < ((ad & 0x3F) + 1); i += 1)
		{
			_set_pix(addr++, pix);
		}
		break;
	default:
		return;
	}

	*paddr = addr;
}

static void _set_pix(uint16_t addr, uint8_t pix)
{
	uint8_t offs = addr & 0x3;
	uint8_t pix4 = framebuffer[addr / 4];
	pix &= 0x3;

	// TODO: drawing modes
	switch (offs)
	{
	case 0:
		framebuffer[addr / 4] = (pix4 & 0xFC) | pix;
		break;
	case 1:
		framebuffer[addr / 4] = (pix4 & 0xF3) | (pix << 2);
		break;
	case 2:
		framebuffer[addr / 4] = (pix4 & 0xCF) | (pix << 4);
		break;
	case 3:
		framebuffer[addr / 4] = (pix4 & 0x3F) | (pix << 6);
		break;
	}
}

static void _exec_cfg(uint8_t cfg_offset, uint8_t ad)
{
	uint8_t old = _configspace[cfg_offset];

	switch (cfg_offset)
	{
	case CFG_MAIN - CFG_OFFSET:
		if (ad & 0x02)
			PIN_VBUF_EN = 1;
		else
			PIN_VBUF_EN = 0;
		if (ad & 0x04)
			GPR_FLAGS |= GPR_FLAG_240_240;
		else
			GPR_FLAGS &= ~GPR_FLAG_240_240;

		if ((ad & 0x01) != (old & 0x01))
		{
			if (ad & 0x01)
				timing_start();
			else
				timing_stop();
		}
	default:
		break;
	}

	_configspace[cfg_offset] = ad;
}
