// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// Hardware definitions needed by assembler too

#include <avr/io.h>
#ifndef HW_H
#define HW_H

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules

// Standard headers

// --- PUBLIC MACROS -----------------------------------------------------------

#define PIN_HSYNC_VPORT_OUT	VPORTA_OUT
#define PIN_HSYNC_bp		4
#define PIN_VSYNC_VPORT_OUT	VPORTA_OUT
#define PIN_VSYNC_bp		5
#define PIN_HSYNC_VGA_VPORT_OUT	VPORTA_OUT
#define PIN_HSYNC_VGA_bp	6
#define PIN_VSYNC_VGA_VPORT_OUT	VPORTA_OUT
#define PIN_VSYNC_VGA_bp	7
#define PIN_VID_VPORT_OUT	VPORTB_OUT

#define GPR_VISIBLE_LINE	GPR_GPR1
#define GPR_FLAGS		GPR_GPR2
#define GPR_FLAG_ODD_bp		0
#define GPR_FLAG_DISPLAYING_bp	1
#define GPR_FLAG_PEND_EXIT_VSYNC_bp	2
#define GPR_FLAG_240_240_bp	3

#define GPR_FLAG_ODD		(1u << GPR_FLAG_ODD_bp)
#define GPR_FLAG_DISPLAYING	(1u << GPR_FLAG_DISPLAYING_bp)
#define GPR_FLAG_PEND_EXIT_VSYNC (1u << GPR_FLAG_PEND_EXIT_VSYNC_bp)
#define GPR_FLAG_240_240	(1u << GPR_FLAG_240_240_bp)

// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#endif // !defined(HW_H)
