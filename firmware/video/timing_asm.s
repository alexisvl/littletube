#include "hw.h"

.global TCA0_CMP0_vect
.global TCA0_CMP1_vect
.global TCA0_CMP2_vect
.global vid_ptr
.extern framebuffer

#define XLO R26
#define XHI R27
#define RDATA R28

#define LETTERBOX_LINES_320_200_TOP 20

; Output four pixels. Assumes RDATA contains the four pixels, packed with the
; first pixel in the least significant two bits, and X contains the address
; of the next group of four pixels. At the end, X is advanced and the next
; group is loaded.
.macro output_4_pix
	out PIN_VID_VPORT_OUT, RDATA
	lsr RDATA
	lsr RDATA
	out PIN_VID_VPORT_OUT, RDATA
	lsr RDATA
	lsr RDATA
	out PIN_VID_VPORT_OUT, RDATA
	lsr RDATA
	lsr RDATA
	out PIN_VID_VPORT_OUT, RDATA
	ld  RDATA, X+
	; second cycle
.endm

.macro output_4n_pix n
.rept \n
output_4_pix
.endr
.endm

; Output four pixels of the same color without loading any data. Pixel data
; is assumed to be in RDATA.
.macro output_4_same_pix
	out PIN_VID_VPORT_OUT, RDATA
	nop
	nop
	out PIN_VID_VPORT_OUT, RDATA
	nop
	nop
	out PIN_VID_VPORT_OUT, RDATA
	nop
	nop
	out PIN_VID_VPORT_OUT, RDATA
	nop
	nop
.endm

.macro output_4n_same_pix n
.rept \n
output_4_same_pix
.endr
.endm

.macro nops n
.rept \n
nop
.endr
.endm

.data
vid_ptr: .byte 0, 0

.text

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TCA0_CMP0_vect:
	cbi PIN_HSYNC_VPORT_OUT, PIN_HSYNC_bp
	cbi PIN_HSYNC_VGA_VPORT_OUT, PIN_HSYNC_VGA_bp
	push r16
	ldi r16, TCA_SINGLE_CMP0_bm
	sts TCA0_SINGLE_INTFLAGS, r16
	pop r16
	reti

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TCA0_CMP2_vect:
	cbi PIN_HSYNC_VPORT_OUT, PIN_HSYNC_bp
	push r16
	ldi r16, TCA_SINGLE_CMP2_bm
	sts TCA0_SINGLE_INTFLAGS, r16
	pop r16
	reti

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TCA0_CMP1_vect:
	push RDATA
	ldi RDATA, TCA_SINGLE_CMP1_bm
	sts TCA0_SINGLE_INTFLAGS, RDATA
	sbis GPR_FLAGS, GPR_FLAG_DISPLAYING_bp
	rjmp _set_hsync
	; OUTPUT DATA
	sbis GPR_FLAGS, GPR_FLAG_240_240_bp
	rjmp _output_320_200
	rjmp _output_240_240
_set_hsync:
	sbi PIN_HSYNC_VPORT_OUT, PIN_HSYNC_bp
	pop RDATA
	reti

;------------------------------------------------------------------------------
_output_320_200:
	; Preamble is 22 cycles (916.67ns) regardless of path.
	push XLO			; 1	1
	push XHI			; 1	1
	in RDATA, SREG			; 1	1
	push RDATA			; 1	1
	lds XLO, vid_ptr		; 3	3
	lds XHI, vid_ptr + 1		; 3	3
	in RDATA, GPR_VISIBLE_LINE	; 1	1
	cpi RDATA, LETTERBOX_LINES_320_200_TOP	; 1	1
	brge 1f				; 1	2
	jmp _do_black_bar		; 2
1:
	inc RDATA			; 	1
	out GPR_VISIBLE_LINE, RDATA	; 	1
	ld RDATA, X+			; 	2

	output_4n_pix (320/4)

	clr RDATA
	out PIN_VID_VPORT_OUT, RDATA

	ld RDATA, -X
	sts vid_ptr, XLO
	sts vid_ptr + 1, XHI

	pop RDATA
	out SREG, RDATA
	pop XHI
	pop XLO
	pop RDATA
	reti
_do_black_bar:
	inc RDATA			; 1
	out GPR_VISIBLE_LINE, RDATA	; 1
	clr RDATA			; 1
	output_4n_same_pix(320/4)
	pop RDATA
	out SREG, RDATA
	pop XHI
	pop XLO
	pop RDATA
	reti

;------------------------------------------------------------------------------
_output_240_240:
	; TODO rebalance the timings of this preamble
	push XLO			; 1
	push XHI			; 1
	in RDATA, SREG			; 1
	push RDATA			; 1
	lds XLO, vid_ptr		; 3
	lds XHI, vid_ptr + 1		; 3

	in RDATA, GPR_VISIBLE_LINE	; 1
	inc RDATA			; 1
	out GPR_VISIBLE_LINE, RDATA	; 1

	clr RDATA			; 1

	; Preamble up to this point is 8 cycles. Add 14 more to match the
	; 320x200 code
	nops 8

	output_4n_same_pix (39/4)
	out PIN_VID_VPORT_OUT, RDATA
	nop
	nop
	out PIN_VID_VPORT_OUT, RDATA
	nop
	nop
	out PIN_VID_VPORT_OUT, RDATA
	nop
	nop
	out PIN_VID_VPORT_OUT, RDATA
	ld RDATA, X+			; 2
	output_4n_pix (240/4)

	clr RDATA
	out PIN_VID_VPORT_OUT, RDATA

	ld RDATA, -X
	sts vid_ptr, XLO
	sts vid_ptr + 1, XHI
	pop RDATA
	out SREG, RDATA
	pop XHI
	pop XLO
	pop RDATA
	reti

