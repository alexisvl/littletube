Version 4
SHEET 1 1108 680
WIRE 240 -272 224 -272
WIRE 400 -272 320 -272
WIRE 464 -272 400 -272
WIRE 592 -272 528 -272
WIRE 656 -272 592 -272
WIRE 672 -272 656 -272
WIRE 784 -272 752 -272
WIRE 848 -272 784 -272
WIRE 960 -272 928 -272
WIRE 1040 -272 960 -272
WIRE 592 -240 592 -272
WIRE 784 -240 784 -272
WIRE 1040 -240 1040 -272
WIRE 960 -224 960 -272
WIRE 224 -208 224 -272
WIRE 224 -208 208 -208
WIRE 336 -208 224 -208
WIRE 240 -160 208 -160
WIRE 336 -160 336 -208
WIRE 336 -160 320 -160
WIRE 784 -160 784 -176
WIRE 400 -144 400 -272
WIRE 1040 -128 1040 -160
WIRE 592 -96 592 -176
WIRE 256 -64 -368 -64
WIRE 352 -64 336 -64
WIRE -368 -16 -368 -64
WIRE -288 -16 -368 -16
WIRE -272 -16 -288 -16
WIRE -176 -16 -192 -16
WIRE 208 -16 208 -160
WIRE 208 -16 -176 -16
WIRE 256 -16 256 -64
WIRE 336 -16 256 -16
WIRE 400 -16 400 -48
WIRE -368 32 -368 -16
WIRE -176 32 -176 -16
WIRE -160 32 -176 32
WIRE -64 32 -80 32
WIRE 400 32 400 -16
WIRE 400 32 272 32
WIRE 656 96 656 -272
WIRE -176 112 -176 32
WIRE -64 112 -176 112
WIRE 400 112 272 112
WIRE 400 128 400 112
WIRE -368 144 -368 112
WIRE -288 192 -288 -16
WIRE -64 192 -288 192
WIRE 320 192 272 192
WIRE -64 272 -112 272
WIRE 656 272 656 176
WIRE 656 272 272 272
WIRE -112 288 -112 272
WIRE 656 304 656 272
WIRE 656 432 656 384
FLAG 400 128 0
FLAG 384 192 0
FLAG 592 -96 0
FLAG 784 -160 0
FLAG 960 -160 0
FLAG -368 144 0
FLAG -112 288 0
FLAG 656 432 0
FLAG 1040 -128 0
SYMBOL PowerProducts\\MC\\MC34063 176 128 R0
SYMATTR InstName U1
SYMBOL nmos 352 -144 R0
SYMATTR InstName M1
SYMATTR Value IRF510
SYMBOL res -64 16 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 180
SYMBOL res -176 -32 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 0.27
SYMBOL cap 384 176 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C1
SYMATTR Value 470p
SYMBOL ind2 224 -144 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L1
SYMATTR Value 33�
SYMATTR Type ind
SYMBOL ind2 224 -288 M90
WINDOW 0 5 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName L2
SYMATTR Value 33�
SYMATTR Type ind
SYMBOL diode 464 -256 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D1
SYMATTR Value UF4007
SYMBOL cap 576 -240 R0
SYMATTR InstName C2
SYMATTR Value 120�
SYMATTR SpiceLine Rser=0.15
SYMBOL ind 656 -256 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L3
SYMATTR Value 10�
SYMBOL cap 768 -240 R0
SYMATTR InstName C3
SYMATTR Value 100n
SYMBOL res 944 -288 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 0.33
SYMBOL cap 944 -224 R0
SYMATTR InstName C4
SYMATTR Value 100n
SYMBOL voltage -368 16 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value 12
SYMBOL res 640 80 R0
SYMATTR InstName R4
SYMATTR Value 390k
SYMBOL res 640 288 R0
SYMATTR InstName R5
SYMATTR Value 10k
SYMBOL res 1024 -256 R0
SYMATTR InstName R6
SYMATTR Value 500
SYMBOL res 352 -80 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R7
SYMATTR Value 47
SYMBOL diode 400 0 M270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D2
SYMATTR Value UF4007
TEXT 24 -224 Left 2 !K1 L1 L2 .99
TEXT 32 -248 Left 2 !.lib diode.lib
TEXT -80 -288 Left 2 !.tran 1 startup
