<html>
<head>
<title>Littletube &mdash; Small CRT Driver</title>
<link rel="stylesheet" type="text/css" href="littletube.css" />
</head>

<body>

<div style="display:flex">
<div style="padding:1rem;flex:1">
<h1>Littletube &mdash; Small CRT Driver</h1>
<h2>Alexis L</h2>
v0.1, May 2021<p>
See also <a href="littlevid.html">Littlevid</a>, Just The Video Generator.

<p><hr>
<i>Littletube is a serial-controlled CRT driver for small CRTs with no
postaccelerator. It provides power supplies, driver circuitry, video
generator, framebuffer, and a high-level graphics system.</i>

<p>
<b>DANGER! This module produces high voltages and must be treated with
respect. Before ever applying power, read the
<a href="#SAFE">Safety sections</a> carefully.</b>

<h4>Features</h4>
<ul>
<li>Supply voltage: 10 to 14V</li>
<li>CRT anode voltage: 700 to 1000V</li>
<li>Video resolution: 320&times;200 or 240&times;240</li>
<li>480i-compatible output with combined sync for external monitors</li>
<li>Serial input with direct text mode and multiple character sets</li>
<li>Simple high-level graphics commands for quick drawing of UIs</li>
</ul>
</div>

<div>
<img src="logo.svg" width="100%" alt="logo">
<p><table border="0" align="right">
	<tr><td colspan="2" style="width:1px;">
		Littletube and its documentation are
		<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC&nbsp;BY-NC-SA</a>.
		Designed in <a href="https://kicad.org">KiCad</a>.
	</td></tr>
	<tr>
	<td valign="top"><table>
	<tr><th colspan="2">PINOUT (PERIPHERAL)</th></tr>
	<tr><td>1</td><td>+12V in</td></tr>
	<tr><td>2</td><td>+12V in</td></tr>
	<tr><td>3</td><td>TXD (serial IN)</td></tr>
	<tr><td>4</td><td>RXD (serial OUT)</td></tr>
	<tr><td>5</td><td>CTS (flow OUT)</td></tr>
	<tr><td>6</td><td>RTS (flow IN)</td></tr>
	<tr><td>7</td><td>nERR/nSHDN</td></tr>
	<tr><td>8</td><td>GND</td></tr>
	<tr><td>9</td><td>VIDEO OUT</td></tr>
	<tr><td>10</td><td>GND</td></tr>
	<tr><td colspan="2" style="width:1px">Note: serial IOs are named following RS232 DCE convention.</td></tr>
	</table>
	</td>
	<td valign="top"><table>
	<tr><th colspan="2">PINOUT (CRT)</th></tr>
	<tr><td>1</td><td>SECOND ANODE</td></tr>
	<tr><td>2</td><td>&mdash;</td></tr>
	<tr><td>3</td><td>HORIZONTAL +</td></tr>
	<tr><td>4</td><td>HORIZONTAL -</td></tr>
	<tr><td>5</td><td>VERTICAL +</td></tr>
	<tr><td>6</td><td>VERTICAL -</td></tr>
	<tr><td>7</td><td>&mdash;</td></tr>
	<tr><td>8</td><td>&mdash;</td></tr>
	<tr><td>9</td><td>FIRST ANODE</td></tr>
	<tr><td>10</td><td>&mdash;</td></tr>
	<tr><td>11</td><td>FOCUS GRID</td></tr>
	<tr><td>12</td><td>&mdash;</td></tr>
	<tr><td>13</td><td>CATHODE</td></tr>
	<tr><td>14</td><td>FILAMENT +</td></tr>
	<tr><td>15</td><td>FILAMENT -</td></tr>
	<tr><td>16</td><td>CONTROL GRID</td></tr>
	</table></td>
</tr></table>
</div>
</div>

<hr style="clear:both">
<p>
<h3>Table of Contents</h3>
<ol>
<li><a name="tocGBLT"><a href="#GBLT">Getting or building a Littletube</a></a></li>
<li><a name="tocSAFE"><a href="#SAFE">Safety for you</a></a></li>
<li><a name="tocNICE"><a href="#NICE">Safety for the board, or, being nice to it</a></a></li>
<li><a name="tocSUIT"><a href="#SUIT">Suitable CRTs</a></a></li>
<li><a name="tocHARD"><a href="#HARD">Hardware interface and pinouts</a></a></li>
<li><a name="tocSOFT"><a href="#SOFT">Software interface and commands</a></a></li>
<li><a name="tocSETU"><a href="#SETU">Setup Mode</a></a></li>
<li><a name="tocCALI"><a href="#CALI">Calibration and adjustment</a></a></li>
</ol>

<hr><h2><a name="GBLT"><a href="#tocGBLT">Getting or building a Littletube</a></a></h2>

<hr><h2><a name="GBLT"><a href="#tocGBLT">Safety</a></a></h2>
<p>The Littletube contans very high voltages, up to 1000V, in uncontained spaces.
<b>NEVER</b> touch the device while powered. Even in regions outside the shaded
area on the circuit board, up to 50V is present.

<p>I can take no responsibility for any injuries that befall you if you touch
a powered Littletube. If you are unfamiliar with working with high voltages,
not touching the board when powered is the only permitted mode of operation.

<p>For the more experienced or intrepid, the "one hand rule" is strongly
recommnded. Clip one side of your probes to ground before powering on the
circuit, then put one hand behind your back before enabling power and use your
free hand to operate probes. This reduces &mdash; but does not eliminate &mdash;
the chance for current to find a path through your heart.

<hr><h2><a name="NICE"><a href="#tocNICE">Safety for the board, or, being nice to it</a></a></h2>
<p>If the board is stored exposed to your (indoor, hopefully) environment, check
it for dust and debris before powering it on. Brush off any you find with a
gentle brush or compressed air.

<p>If you build a Littletube yourself, it may be prone to self destruction if
you do not apply a coating to the high voltage section. The region of the PCB
that is shaded in silkscreen should be coated top and bottom. I recommend MG
Chemicals 4226 "Super Corona Dope", but this is expensive and a bit of an
unfriendly chemical. Any clear surface finish you can find at the hardware
store is likely to work, though a few components in that section get rather hot,
and if the coating is water-based you must make <i>very, very sure</i> it is
dry before applying power.

<p>It is further recommended to guard the bottom side of the high voltage
section from accidental contact with surfaces below. In prototypes, I am using
fishpaper brushed with the same high voltage coating and stuck onto the back
of the PCB; in a later revision I may add mounts for a less janky plastic sheet.

<hr><h2><a name="SUIT"><a href="#tocSUIT">Suitable CRTs</a></a></h2>
LIttletube can drive CRTs that meet the following requirements:

<ul>
<li>Electrostatic deflection (no deflection yoke).</li>
<li>No postaccelerator (there should not be a little hole to attach an anode cap).</li>
<li>Final anode runs between 700V and 1000V. Note that running a little cold
	usually works, so Littletube can probably drive 1200V CRTs and maybe
	1500V CRTs.</li>
<li>Filament runs between 5 and 7V and at up to 500mA.</li>
<li>Grid cutoff no more negative than -100V.</li>
</ul>

The tubes I had in mind while designing it were the miniature Soviet tubes
like 6LO1I.

<hr><h2><a name="HARD"><a href="#tocHARD">Hardware interface and pinouts</a></a></h2>

<p>Electrical specifications:<p>

<table border="1" style="text-align:center">
<tr><th>Parameter</th><th>Min</th><th>Nom</th><th>Max</th></tr>
<tr><td align=left>Input voltage</td><td>10V</td><td>12V</td><td>14V</td></tr>
<tr><td align=left>Supply current, only CRT drive active</td><td></td><td>TBD</td><td></td></tr>
<tr><td align=left>Supply current, only video buffer active</td><td></td><td>TBD</td><td></td></tr>
<tr><td align=left>Supply current, CRT drive and video buffer active</td><td></td><td>TBD</td><td></td></tr>
<tr><td align=left>Supply current, shutdown</td><td></td><td>TBD</td><td></td></tr>
<tr><td align=left>IO voltage, programmable</td><td>1.8V</td><td>&mdash;</td><td>5.0V</td></tr>
<tr><td align=left>Second anode output (A2), programmable</td><td>650V</td><td>&mdash;</td><td>1000V</td></tr>
<tr><td align=left>First anode output, rel. A2, trimmable</td><td>4%</td><td>&mdash;</td><td>18%</td></tr>
<tr><td align=left>Focus grid output, rel. A2, trimmable</td><td>0%</td><td>&mdash;</td><td>16%</td></tr>
<tr><td align=left>Filament drive, programmable</td><td>4.2V</td><td>&mdash;</td><td>7.5V</td></tr>
<tr><td align=left>Filament current</td><td></td><td></td><td>700mA</td></tr>
<tr><td align=left>Filament-cathode voltage</td><td>0V</td><td></td><td>-50V</td></tr>
<tr><td align=left>Grid cutoff, programmable</td><td>0V</td><td>&mdash;</td><td>-100V</td></tr>
<tr><td align=left>Spot killer cutoff withstand</td><td></td><td></td><td>100V</td></tr>
<tr><td align=left>Cathode V<sub>p-p</sub>, programmable</td><td>7V</td><td></td><td>45V</td></tr>
<tr><td align=left>Deflection V<sub>p-p</sub>, programmable</td><td>20V</td><td></td><td>200V</td></tr>
<tr><td align=left>Video bandwidth</td><td>6 MHz</td><td></td><td></td></tr>
<tr><td align=left>Deflection bandwidth, H</td><td>50 kHz</td><td></td><td></td></tr>
<tr><td align=left>Deflection bandwidth, V</td><td>1 kHz</td><td></td><td></td></tr>
</table>

<p>Notes:
<ol>
<li>Supply current with the CRT driver active is specified with no CRT
	connected. Actual current will depend on your CRT's filament power and
	beam current.
<li>First anode and focus grid adjustments are trimmable by hand using
	potentiometers. See the <a href="#CALI">Calibration and adjustment</a>
	section.
<li>Filament-cathode voltage is the peak voltage applied to the filament with
	respect to the cathode. This is a rating of your tube, and exceeding it
	will cause accelerated degradation of the cathode assembly. Littletube
	operates with a negative bias (the filament is lower than the cathode)
	of up to 50V. Most CRTs prefer this, for example the 6LO1I permits as
	low as -130V but no more positive than 0V. However, you should check
	your CRT datasheet.
</ol>

<dl>
<dt>J1</dt><dd><a href="#conn_main">Main connector</a>: power in, serial in/out, and low voltage video
out. 10-pin header at the low voltage end.</dd>
<dt>J4</dt><dd><a href="#conn_crt">CRT connector</a>. All connections to the tube are here. 16-pin
header with gaps at the high voltage end.</dd>
<dt>JP1</dt><dd>(JUMPER) <a href="#conn_setup">Setup Mode</a>. Boot with a jumper installed here to
enter <a href="#SETU">Setup Mode</a>.</dd>
<dt>J2</dt><dd>(DEVEL) <a href="#conn_prog">Programming/debug header</a>.</dd>
<dt>TP1</dt><dd>(DEVEL) <a href="#conn_par">Parallel bus debug point</a>.</dd>
<dt>J3</dt><dd>(DEVEL) <a href="#conn_vga">VGA out</a>.</dd>
</dl>

<p>
<h3><a name="conn_main">Main connector</a></h3>
<p>Pins are named as if for an RS232 DCE (peripheral/modem, not computer) port.

<blockquote><pre>
PIN     PURPOSE
------------------------------------
J1.1    +12V in
J1.2    +12V in (redundant; both should be connected)
J1.3    TXD (serial IN, this is named from your computer's perspective)
J1.4    RXD (serial OUT, this is named from your computer's perspective)
J1.5    CTS (flow control OUT, active low by default)
J1.6    RTS (flow control IN, active low by default)
J1.7    ERR/SHDN (note: incorrectly labeled "5V" on the prototype PCB, see below)
J1.8    GND
J1.9    VIDEO OUT
J1.10   GND
</pre></blockquote>

<p>By default, the serial port operates at 3.3V. Pin naming is suitable for
direct same-to-same connection to a normal USB UART cable. Note that hardware
flow control is optional; you will want some flow control but the Littletube
also supports software (XON/XOFF) flow control.

<p>See the <a href="#SETU">Setup Mode</a> documentation for information on
changing the serial port parameters. By default, Littletube runs at 115200
baud, no parity, noninverted TX/RX, inverted RTS/CTS, only software flow
control enabled, and 3.3V logic level.

<p>The ERR/SHDN pin is internally pulled up to the selected IO voltage.
However, for robust operation you should also pull it up to the same voltage
yourself, both because the microcontroller internal pullups are weak, and also
to allow it to function in the event of an analog circuit fault that takes out
the programmable IO voltage system.

<p>ERR/SHDN will be pulled to GND by Littletube to indicate the presence of
a fault. In response, your system may use the error query command to request
more information; expect this to time out though as some faults may disable
the serial port.

<p>ERR/SHDN should also be pulled to GND by you to shut down the Littletube.
Note that for lowest power consumtion you should also shut off your local
pullup resistor. Littletube will pulse its pullup resistor periodically for
just long enough to detect when this pin is released again.

<p>VIDEO OUT is a buffered, 75 ohm video signal output with combined sync,
suitable for direct connection to "composite" video inputs (but not including
any color information). The video buffer is not enabled by default but can be
activated manually via the serial interface.

<h3><a name="conn_crt">CRT connector</a></h3>
<p>The CRT connector contains all of the outputs required to drive a CRT.
If you build the board yourself, pins marked --no&nbsp;pin-- below should
have these pins <i>removed</i> from the connector using pliers prior to
soldering, to increase isolation.

<blockquote><pre>
PIN     PURPOSE
------------------------------------
J4.1    Second anode (whichever is higher voltage)
J4.2    --no pin--
J4.3    Horizontal deflection, positive
J4.4    Horizontal deflection, negative
J4.5    Vertical deflection, positive
J4.6    Vertical deflection, negative
J4.7    --no pin--
J4.8    --no pin--
J4.9    First anode
J4.10   --no pin--
J4.11   Focus grid
J4.12   --no pin--
J4.13   Cathode
J4.14   Filament, positive output
J4.15   Filament, negative output
J4.16   Control grid (Russian datasheets call this "modulator")
</pre></blockquote>

<p>To build a cable, the following connectors are recommended. Note,
however, that standard "header connectors" <i>may</i> be used, but the
suggested part number provides latching so the high voltage connections do
not fall out.

<ul>
<li>TE Connectivity 1-1375820-6 connector body</li>
<li>TE Connectivity 1445336-1 crimp pins</li>
</ul>

<p>These parts are available from most electronics distributors. Also, you
should use wiring rated for at least 2kV on pins 1 through 6, at least 600V
on pins 9 and 11, and at least 300V on the rest of the pins. Cheap silicone
wire with these ratings is readily available from sites like eBay and
AliExpress.

<p>If you must use low-voltage wiring for testing, bind together pins
1-6, 9-11, and 13-16 in groups using tape or cable ties, then route them
so that they do not touch each other. Do NOT touch these wires when powered.

<p>The usual thin pins on the CRT itself can be connected to using Amphenol
86566620064LF crimp pins, which are intended for use in D-sub connectors but
also fit the CRT pin geometry. I recommend heat shrinking over them with
non-adhesive heat shrink, both to insulate them from each other, and also to
tension the connectors a bit more.

<h3><a name="conn_setup">Setup Mode jumper</a></h3>
Two-pin jumper. To enter Setup Mode, connect a jumper while the system
is powered down, then boot it up.

<h3><a name="conn_prog">Programming/debug header</a></h3>
<p>Use this header to load new firmware onto the Littletube (note that it
currently does not ship with any bootloader, this header is required). A
programmer supporting Atmel/Microchip UPDI is needed. This can be built using
a USB UART cable - place a 4.7k resistor between TX and RX, then use RX as
the UPDI pin. You can then use
<a href="https://pypi.org/project/pymcuprog/">pymcuprog</a> to program the
microcontrollers.

<p><b>Warning:</b> the +5V pin on this header is an output, meant for
"proper" UPDI programmers that use it as a reference voltage. Do NOT connect
a USB UART 5V pin to this.

<blockquote><pre>
                      PURPOSE    PIN  | PIN   PURPOSE
                      ----------------|----------------
UPDI for framebuffer controller +J2.1 | J2.2  +5V OUT - not IN
       UPDI for main controller  J2.3 | J2.4  GND
</pre></blockquote>

<h3><a name="conn_par">Parallel bus debug point</a></h3>
<p>The parallel bus connection exists for debug purposes; it is simply a large
testpoint for viewing traffic on the parallel bus between the main controller
and the framebuffer controller. If you want to ues this port, populate it with
a Molex 5022441530 flat-flex connector.

<blockquote><pre>
PIN     PURPOSE
------------------------------------
TP1.1   GND
TP1.2   AD0 (address/data bit 0)
TP1.3   AD1 (address/data bit 1)
TP1.4   AD2 (address/data bit 2)
TP1.5   AD3 (address/data bit 3)
TP1.6   AD4 (address/data bit 4)
TP1.7   AD5 (address/data bit 5)
TP1.8   AD6 (address/data bit 6)
TP1.9   AD7 (address/data bit 7)
TP1.10  nACK, used to indicate an action has been executed
TP1.11  nSTB, used to indicate an action is ready on the bus
TP1.12  AC2 (action bit 2)
TP1.13  AC1 (action bit 1)
TP1.14  AC0 (action bit 0)
TP1.15  GND
</pre></blockquote>

<h3><a name="conn_vga">VGA out</a></h3>
<p>The VGA Out connection exists for development purposes, for prototyping the
more lightweight Littlevid using the Littletube board. You should not connect
a VGA cable to it directly, as it lacks critical protection circuitry and is
likely to result in damage to the framebuffer controller.

<p>If you want to use this port, populate it with a TE Connectivity 84952-5
flat-flex connector, and install sturdy 5V TVS diodes at the other end of the
cable to protect it. The +5V pin should not be used (though TVS structures may
connect to it). HSYNC and VSYNC may connect directly to these inputs on a VGA
monitor. To deliver a compliant video signal, the VIDEO pin must be connected
to only one color; if you want white on a color monitor, you can try tying all
three together (some monitors may be able to adapt to this despite the reduced
signal amplitude), or adding a video amplifier to buffer the signal to all
three.

<blockquote><pre>
PIN     PURPOSE
------------------------------------
J3.1    +5V out. NOT CURRENT LIMITED
J3.2    HSYNC
J3.3    VSYNC
J3.4    VIDEO. Present when the video buffer is enabled.
J3.5    GND
</pre></blockquote>

<hr><h2><a name="SOFT"><a href="#tocSOFT">Software interface or commands</a></a></h2>

In normal operating mode, Littletube acts as a serial terminal. Text received
is written to the framebuffer. Commands are specified using ANSI escape
sequences:

<ul>
<li>BEL means ASCII code 7 (hex 07)</li>
<li>BS means ASCII code 8 (hex 08)</li>
<li>HT means ASCII code 9 (hex 09)</li>
<li>LF means ASCII code 10 (hex 0A)</li>
<li>CR means ASCII code 13 (hex 0D)</li>
<li>ESC means ASCII code 27 (hex 1B)</li>
<li>CSI means ESC followed by <tt>[</tt> (ascii hex 1B 5B)</li>
<li>DCS means ESC followed by <tt>P</tt> (ascii hex 1B 50)</li>
<li>OSC means ESC followed by <tt>]</tt> (ascii hex 1B 5D)</li>
<li>SOS means ESC followed by <tt>X</tt> (ascii hex 1B 58)</li>
<li>ST (string terminator) means ESC followed by <tt>\</tt> (ascii hex 1B 5C)</li>
</ul>

<h3>Escape codes</h3>

<table border="1">
<tr><th>Sequence</th><th>Name</th><th>Notes</th></tr>
<tr><td>CSI _n A</td><td>Cursor Up</td><td>_n defaults to 1</td></tr>
<tr><td>CSI _n B</td><td>Cursor Down</td><td>_n defaults to 1</td></tr>
<tr><td>CSI _n C</td><td>Cursor Forward</td><td>_n defaults to 1</td></tr>
<tr><td>CSI _n D</td><td>Cursor Back</td><td>_n defaults to 1</td></tr>
<tr><td>CSI _n ; _m H</td><td>Cursor Position</td><td>Move cursor to row _n, col _m (1-based). Default values are both 1 (top left corner).</td></tr>
<tr><td>CSI _n J</td><td>Erase in Display</td><td>Clear: 0/default: cursor to end; 1: cursor to beginning; 2: entire screen</td></tr>
<tr><td>CSI _n K</td><td>Erase in Line</td><td>Clear: 0/default: cursor to end of line; 1: cursor to beginning of line; 2: entire line</td></tr>
<tr><td>CSI _n m</td><td>Select Graphic Rendition</td><td>See options below</td></tr>
<tr><td>CSI _n Q</td><td>Decimal Character</td><td>Nonstandard. Equivalent to sending _n as a byte value. Used to display the characters from 0 to 31 in the character set, which are normally interpreted as control characters.</td></tr>
<tr><td>OSC _cmd ST</td><td>Setup Mode command</td><td>Runs a command as if in Setup Mode.</td></tr>
<tr><td>DCS _cmd ST</td><td>Device Control command</td><td>Used to set runtime paramters, see below.</td></tr>
<tr><td>SOS _cmd ST</td><td>Graphics Data command.</td><td>Used to draw bitmap graphics into the framebuffer or RAM font, see below.</td></tr>
</table>

<h3>Graphic rendition options</h3>

<table border="1">
<tr><th>Number</th><th>Name</th><th>Notes</th></tr>
<tr><td>0</td><td>Reset</td><td>All attributes off, default Inten = 2</td></tr>
<tr><td>1</td><td>Bold</td><td>Inten = 3</td></tr>
<tr><td>2</td><td>Faint</td><td>Inten = 1</td></tr>
<tr><td>3</td><td>Oblique</td><td>Implemented as a simple bitmap slant</td></tr>
<tr><td>4</td><td>Underline</td><td></td></tr>
<tr><td>7</td><td>Invert</td><td>Swap foreground and background</td></tr>
<tr><td>9</td><td>Strike</td><td>Line drawn through text</td></tr>
<tr><td>10</td><td>Charset: CP437</td><td>Default character set.</td></tr>
<tr><td>11</td><td>Charset: KOI8-R</td><td>Russian character set.</td></tr>
<tr><td>12</td><td>Charset: Windows-1252</td><td>Windows-1252 character set. This includes standalone diacritics, which can be used with BS (backspace) as combining diacritics to render most western European text.</td></tr>
<tr><td>13</td><td>Charset: RAM</td><td>Character set in RAM. This is blank by default; a Graphics Data command can be used to populate it.</td></tr>
<tr><td>21</td><td>Double underline</td><td></td></tr>
<tr><td>22</td><td>Normal intensity</td><td>Cancels 1 and 2</td></tr>
<tr><td>23</td><td>Not oblique</td><td>Cancels 3</td></tr>
<tr><td>24</td><td>Not underlined</td><td>Cancels 4 and 21</td></tr>
<tr><td>27</td><td>Not inverted</td><td>Cancels 7</td></tr>
<tr><td>29</td><td>Not crossed out</td><td>Cancels 9</td></tr>
<tr><td>53</td><td>Overlined</td><td></td></tr>
<tr><td>55</td><td>Not overlined</td><td>Cancels 53</td></tr>
</table>

<h3>Device Control commands</h3>

<p>Device Control commands are strings encapsulatd between DCS and ST. They are
parsed like Setup Mode commands; arguments are separated by a space.

<table border="1">
<tr><th>Command</th><th>Description</th></tr>
<tr><td>on _s</td><td>Turn on output selection _s. This is a string containing
	some combination of <tt>c</tt> to turn on the CRT and <tt>v</tt> to
	turn on the video signal buffer.</td></tr>
<tr><td>off [_s]</td><td>Turn off outputs, either all outputs, or specified outputs.</td></tr>
<tr><td>sleep</td><td>Go to sleep. Equivalent to pulling the SHDN line low. You can wake by sending a BREAK for at least 100ms or by toggling the SHDN line.</td></tr>
<tr><td>inten _x</td><td>Set display intensity, from 0 to 100</td></tr>
<tr><td>hzoom _x</td><td>Set horizontal zoom, from 0 to 100, 50 is default</td></tr>
<tr><td>vzoom _x</td><td>Set vertical zoom, from 0 to 100, 50 is default</td></tr>
<tr><td>hpos _x</td><td>Set horizontal position, from 0 to 100, 50 is default</td></tr>
<tr><td>vpos _x</td><td>Set vertical position, from 0 to 100, 50 is default</td></tr>
<tr><td>aspect _x</td><td>Set aspect ratio and resolution to <tt>sq</tt> (240x240) or <tt>rect</tt> (320x200). After changing, you should redraw the display.</td></tr>
<tr><td>gres</td><td>Return the graphics resolution, as ROWS COLS</td></tr>
<tr><td>cres</td><td>Return the character resolution, as ROWS COLS</td></tr>
</table>

<h3>Graphics Data commands</h3>

<p>Graphics Data commands are strings encapsulated between SOS and ST. They
start with a single character to indicate the command being given, then
a list of space-separated arguments as with Device Control. Graphics data is
provided as a base64-encoded argument.

<p><b>NOTE</b> that the input buffers can only support a total command length
of 120 bytes, including SOS, command byte, all arguments, and ST. Any more than
this cannot be processed and should be split into multiple commands.</p>

<table border="1">
<tr><th>Command char</th><th>Name</th><th>Args</th><th>Description</th></tr>
<tr><td>0</td><td>RF_RESET</td><td></td><td>Reset RAM font</td></tr>
<tr><td>1</td><td>RF_COPY</td><td>_x _y _z</td><td>Copy characters from current font into RAM font. Copies _x consecutive characters starting at index _y in the source font and starting at index _z in the RAM font.</td></tr>
<tr><td>2</td><td>RF_LOAD</td><td>_x _graph</td><td>Load a character into the RAM font at codepoint _x. _graph is base64, each byte is a row, bits in a byte are MSB (left) to LSB (right) columns. Font is 8x14, all 14 rows are required.</td></tr>
<tr><td>3</td><td>FONT_GET</td><td>_x</td><td>Return the contents of character _x in the current font, in the same encoding as for RF_LOAD.</td></tr>
<tr><td>4</td><td>GOTO</td><td>[_row] [_col]</td><td>Set graphics cursor at pixel location _row,_col, zero-indexed to the top left corner.</td></tr>
<tr><td>5</td><td>COFFSET</td><td>[_row] [_col]</td><td>Set the top left position of character location (0, 0) to (_row, _col)</td></tr>
<tr><td>6</td><td>PIX</td><td>[_inten] _trunc _graph</td><td>Load pixel data starting at the graphics cursor. If [_inten] is set, encoding is 1 bit per pixel, draw at _inten if 1, clear if 0, MSB to LSB left to right. If [_inten] is not present, encoding is 2 bits per pixel, draw all pixels. The first _trunc pixels are skipped, allowing pixel counts not a multiple of 4/8 to be drawn.</td></tr>
<tr><td>7</td><td>NEXT_ROW_PIX</td><td>[_inten] _trunc _graph</td><td>Same as PIX, but first, increment the row and reset the column to the last value passed to GOTO. Used to easily draw rectangular regions of image data.</td></tr>
<tr><td>8</td><td>MODE</td><td>_m</td><td>Set the drawing mode. This applies to all operations, including text drawing! 0 (default) = ASSIGN, all pixels are set. 1 = MAX, pixels are set to max of old value and new value. 2 = MIN, pixels are set to min of old value and new value. 3 = XOR, pixels are set to exclusive-or of old and new values.</td></tr>
<tr><td>9</td><td>SCREENSHOT</td><td>_row _col _height _width</td><td>Read back a region of graphics data. The pixels are encoded in 2-bit in the same order as above, with the row stride equal to _width rounded up to the nearest 4. Use 0 0 -1 -1 to screenshot the entire display. For large regions, this is a long operation that will tie up the system for several VSYNC intervals.</td></tr>
<tr><td>A</td><td>BRUSH</td><td>_w _inten [_aa]</td><td>Set brush width, intensity, and antialiasing setting for high-level drawing ops. Default is 1 2 1. Antialiasing nominally uses the Xiaolin Wu algorithm.</td></tr>
<tr><td>B</td><td>FILL</td><td>[_inten]</td><td>Set fill intensity for high-level drawing ops (omit to turn off fill, which is the default)</td></tr>
<tr><td>C</td><td>LINE</td><td>dy dx</td><td>Draw a line from graphics cursor to +(dy, dx) and move the graphics cursor to +(dy, dx).</td></tr>
<tr><td>D</td><td>RECT</td><td>dy dx</td><td>Draw a rectangle from graphics cursor to +(dy, dx) and move the graphics cursor to +(dy, dx).</td></tr>
<tr><td>E</td><td>CIRCLE</td><td>r</td><td>Draw a circle with radius r at the graphics cursor.</td></tr>
</table>

<hr><h2><a name="SETU"><a href="#tocSETU">Setup Mode</a></a></h2>
<hr><h2><a name="CALI"><a href="#tocCALI">Calibration and adjustment</a></a></h2>

<p><b>Danger, high voltage</b>: see the <a href="#SAFE">Safety</a> section before
performing calibration. Some steps require adjustment of on-board
potentiometers. These potentiometers are insulated, but they reside in the
high-voltage section of the board. You should use an insulated screwdriver
(included with the Littletube if you bought it from me) to adjust them.

<h3>Adjustment (per tube)</h3>

<p>Adjustment needs to be performed for every CRT. Software adjustments can
be exported via the serial port and re-imported in one step.

<ol>
<li>Perform Calibration, below, if this is a new board or its EEPROM has
	been erased. You will know if this is necessary &mdash; the firmware
	will not allow CRT operation otherwise.</li>
<li>Attach your CRT to the Littletube.</li>
<li>Enter Setup Mode by attaching the setup mode jumper before powering on.
	Note that in Setup Mode, the serial port parameters are always
	115200 8N2, 3.3V, xon/xoff.</li>
<li>Program your CRT's second anode voltage with the following command,
	where <tt>XXX</tt> is the voltage (without decimal point) between
	650 and 1000:<br>
	<tt>setting va2 XXX</tt></li>
<li>Program your CRT's filament voltage with the following command, where
	<tt>XXX</tt> is the nominal voltage in <i>integer millivolts</i>
	(for example, 6.3V is 6300):<br>
	<tt>setting vfil XXX</tt></li>
<li>Program your CRT's nominal filament current with the following command,
	where <tt>XXX</tt> is the nominal current in <i>integer milliamps</i>
	(for example, 300mA is 300):<br>
	<tt>setting ifil XXX</tt></li>
<li>Issue the HV Unlock command to permit high voltage generation in setup mode:<br>
	<tt>unlock_hv</tt></li>
<li>Enter the Guided Adjustment routine with the following command:<br>
	<tt>crt_adjust</tt>
	<br><br>
	Afer a 30 second delay to permit cancelling, Littletube will display a
	test pattern on the CRT and some instructions on the terminal. Use the
	keyboard to make adjustments:
	<br>
	<pre>
	1     Brightness down.     Dim the black level.
	2     Brightness up.       Increase the black level.
	9     Contrast down.       Dim the brightest parts of the image.
	0     Contrast up.         Brightens the brightest parts of the image.

	W     Image up.            Moves the entire image up.
	A     Image left.          Moves the entire image left.
	S     Image down.          Moves the entire image down.
	D     Image right.         Moves the entire image right.

	up    Vertical zoom in.    Makes the image larger on the vertical axis.
	down  Vertical zoom out.   Makes the image smaller on the vertical axis.
	left  Horizontal zoom out. Makes the image smaller on the horizontal axis.
	right Horizontal zoom in.  Makes the image larger on the horizontal axis.

	Q     Toggle aspect ratio. Switches between sQuare and round display.

	esc   Exit, cancel.        Discards adjustments.
	enter Exit, apply.         Saves adjustments in RAM - you must still use `commit`
	</pre><br>

	<p>First, adjust the brightness (cutoff/grid bias). If there are no black
	regions of the image, adjust it down until these regions are actually
	black. If some regions are already black, adjust it up just until they
	begin to glow, then back down. The video bandwidth, and thus sharpness,
	will be superior if the cutoff is "critical" - low enough, but no lower
	than needed.

	<p>Second, once the alternating black and light regions are clearly
	visible, use the insulated screwdriver and one-hand technique (described
	in <a href="#SAFE">Safety</a>) to adjust the FOCUS (RV1) and ASTIG (RV2)
	potentiometers on the board for the best image quality. If ASTIG also
	adjusts intensity (as it will on some simpler tubes), do not take the
	tube uncomfortably bright - decrease contrast using the `9` key to keep
	it at a safe level.

	<p>Third, adjust the contrast. Note that this is only the <i>maximum</i>
	adjustment. Try to find an intensity level that is brighter than you
	expect to need, but not so bright that there is a risk of damage to the
	CRT (use your best judgment &mdash; it is a display, not a flashlight).

	<p>Fourth, adjust the position and zoom parameters to bracket the
	corners of the display with the "corners" shown in the test pattern
	(if they are not visible, zoom out more). Use the Q key to toggle the
	aspect ratio to the one that gets the best circle in the center; you
	may find you need to re-adjust zoom and position after changing.

	<p>Fifth, press enter to exit the Guided Adjustment Routine.
</li>
<li>If you are satisfied with the adjustments, save them to EEPROM with the following command:<br>
	<tt>commit</tt>
</ol>

<h3>Calibration (per board)</h3>

<ol>
<li>Disconnect any CRT from the board. The CRT connector should be empty.</li>
<li>Place the Littletube securely on a surface, where it can activate its
	high voltage supply without danger.</li>
<li>Enter Setup Mode as specified above.</li>
<li>Issue the HV Unlock command to permit high voltage generation in setup mode:<br>
	<tt>unlock_hv</tt></li>
<li>Issue the Self Calibrate command:<br>
	<tt>do_self_cal</tt></li>
<li>The firmware will warn you that it is about to generate high voltage, and
	give you 30 seconds to disagree by pressing any key.</li>
<li>After calibration is performed, save it by issuing the Commit command:<br>
	<tt>commit</tt></li>
</ol>

<p>
</body>
</html>
